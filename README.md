# UPSYCLE Message Router

As part of [DREAM](https://dream.public.cat) we present a reference software
implementation of
[upsycle-router](https://dream.public.cat/t/d1-2-upsycle-architecture-overview/151).
We provide library and application code for running one or more message routers
and one or more services, as described in the [technical specification of
upsycle-router](https://dream.public.cat/pub/multicast-message-routing-for-p2p-pubsub)

> UPSYCLE is a decentralized pub/sub protocol based on P2P gossip-based
> clustering and dissemination protocols. It is used for the decentralized
> synchronization of mergeable, replicated data structures with causal ordering
> and eventual consistency properties.

Upsycle-router is an integral part of the UPSYCLE architecture. It is the
component which is run on all devices in the system — a phone, a laptop, a core
node in a data center — which handles authentication, encryption, and
translation of public keys into routing information. Services connect to the
nearest message router, and message routers connect to each other. In this way
the message routers form the backbone of the system and all parts of the system
can communicate with each other. Once connected, services can publish updates
to a “topic” (which is identified by a public key), or directly send a message to
another service or router (whose address is also a public key), and the
message router makes the routing, sending and receiving transparent.

It also contains a cache of all recently seen messages and a temporary queue
for messages which should be retried later, and it maintains a registry of
which nodes in the system should be notified about various kinds of updates.

End-to-end encryption ensures that intermediate message routers can only read the
outermost routing header of a message to know how to handle it,
but cannot access the message payload.
It is not part of the pub/sub protocol itself, however applications using it
are encouraged to implement end-to-end encryption as part of a higher layer protocol.

As part of UPSYCLE there is also ongoing research and development into
peer-to-peer protocols including peer discovery, clustering, and dissemination,
which form another major part of the system. Once these are ready they can be
connected to this implementation, for example by creating a service which
understands these protocols and communicates through its local message router.

[DREAM]: https://dream.public.cat
[UPSYCLE]: https://dream.public.cat/pub/dream-pubsub-spec

## Installation

### Requirements

Upsycle-router (<abbr>UR</abbr>) is written in OCaml and requires version `4.11.0` or later.

It is available on Debian 11, Alpine Linux 3.14, and NixOS 21.05 among others.

#### On Debian 11 Bullseye

```shell
apt install \
  git \           # to retrieve repositories
  ocaml-nox \     # the OCaml interpreter
  opam \          # the OCaml Package Manager
  pkg-config \
  libgmp-dev \    # required to build ocaml-conduit-dream
  libffi-dev \    # required to build UR
  openssl         # the openssl binary is needed by UR
```

#### On Alpine Linux 3.14

Alpine Linux is a popular distribution for containers as it makes tiny OS
images. These instructions have been verified on 3.14.2.

```shell
apk update
# --- remove any existing ocaml packages; adapt as necessary.
apk del ocaml ocaml-compiler-libs ocaml-findlib ocaml-ocamldoc ocaml-runtime
apk add opam
apk add build-base m4 pkgconfig
# --- needed by opam
apk add bash bubblewrap coreutils git
# --- needed by conf-gmp
apk add gmp-dev
# --- needed by core
apk add linux-headers
# --- needed by conf-libffi
apk add libffi-dev
# --- needed by upsycle-router
apk add openssl
```

#### On NixOS 21

Since NixOS builds from source, you only have to use the provided _Nix Flakes_ and they will pull all dependencies: you may proceed to the Build section now!

### Build

#### On NixOS

```shell
nix build
```

On legacy version you may use `nix-build` instead.

#### On other OSes

```shell
# On Debian (use OCaml compiler from apt):
opam init -y

# On Alpine (build OCaml compiler using opam):
opam init -y --compiler 4.11.0

# If you already use opam on your system, this step is highly recommended in
# order to create an isolated environment in which to install the dependencies,
# but may take some time depending on the speed of your machine. In that case,
# only skip it if you know what you're doing. Use any string you like for
# <name>, e.g. '4.11.0' or 'upsycle-router'.
opam switch create <name> 4.11.0

src_dir="$HOME"/src/dream # for example
mkdir -p "$src_dir"
cd "$src_dir"

# 1. Install ocaml-seaboar
git clone https://gitlab.com/public.dream/libs/ocaml-seaboar ocaml-seaboar
cd $_
git reset --hard v0.6.8
opam install .
cd ..

# 2. Install ocaml-conduit-dream
git clone https://gitlab.com/public.dream/upsycle/ocaml-conduit-dream ocaml-conduit-dream
cd $_
git reset --hard dream-v4.0.1-10
opam install .
cd ..

# 3. Build this code repository
git clone https://gitlab.com/public.dream/upsycle/upsycle-router upsycle-router
cd $_
git submodule update --init --recursive
opam install --deps-only --with-test .
# set <jobs> to 100 for speed, or 1 if memory-constrained
dune build -p upsycle-router -j <jobs>

# Optional -- this will install the binaries `upsycle-router` and
# `upsycle-service` in your path (most # likely ~/.opam/<switch>/bin)
opam install .
```

## Run

Once you have successfully built the sources, you may run:

### On NixOS

To obtain a developer shell:

```shell
nix develop
```
(or `nix-shell` on legacy NixOSes)

### On all OSes

For a more detailed description of the options, we refer to the example
scenario. In a terminal:
```shell
# Generate fresh ED25519 private keys and SSL certificates. These expire after 1 day in the default configuration.
example/gen-keys-and-certs
# Run message router A.
example/run-message-router a
```

The directory `example/` provides 3 routers: A, B, and C. You may start other routers with your own configuration, see `./bin/message-router -h` and the example configuration files in `example/conf`.

In a new terminal (Ctrl-z won't work unless you disable keyboard input; see
below), launch service A1:
```shell
example/run-service a1
```

This example provides 4 services: A1, A2, A3, and B1, which connect to their respective message routers A & B. You may start other services with your own configuration, see `./bin/service -h` and the example configuration files in `example/conf`.

## Implementation, discussion, refinement and errata

### Repositories

This component depends on three repositories:
- [ocaml-seaboar](https://gitlab.com/public.dream/libs/ocaml-seaboar)
- [ocaml-conduit-dream](https://gitlab.com/public.dream/upsycle/ocaml-conduit-dream)
- [upsycle-router](https://gitlab.com/public.dream/upsycle/upsycle-router) (the current one)

We created `ocaml-seaboar` in order to provide a highly expressive and useful
library for encoding and decoding CBOR with OCaml. CBOR is the canonical binary
encoding format which has been chosen for DREAM. `ocaml-seaboar` is very useful
for accurately encoding/decoding an arbitrary OCaml data structure using
functional programming techniques (in particular monadic parsing provided by the
[Angstrom](https://github.com/inhabitedtype/angstrom) library).

We use CBOR for encoding messages and for serializing the state of the message
router. Serialization is useful because if the message router is forced to
restart, or if it's running in for example a unikernel which is stopped and
restarted by a hypervisor, the restarted process can quickly restore its
last-known state.

`ocaml-conduit-dream` is a fork of
[ocaml-conduit](https://github.com/mirage/ocaml-conduit), which provides a very
useful high-level interface for creating TLS connections and sending and
receiving data through asynchronous channels linked to Lwt “threads”, or
promises. We chose to fork it in order first to fix a bug related to client
authentication (about which more below), and second in order to allow a node to
learn the public key of a connected peer, which the upstream library otherwise
abstracts away. This is a crucial element of UPSYCLE’s design.

### What we provide

We provide library and application code for a reference implementation of the
UPSYCLE message router and one or more services.

The library code can be found in upsycle-router/lib and its API documentation is
[here](https://upsycle-router.alleycat.cc).

We also provide two console applications: one for running the message router and
one for running a (mock) local service. You can find the code for these in
upsycle-router/bin, and an example scenario with three message routers and four
services in example/. The keyboard can be configured in various modes (see the
associated YAML configuration files). When enabled, it can be used to manipulate
or query the state or to send a variety of predefined messages, and in the case
of the message router, to simulate receiving various kinds of messages.

Press ‘h’ (when in keyboard mode) to see the available commands. Set
`keyboard-mode` to `demo` to enhance the set of commands, to `normal` for a
minimal set, or to `none` to disable. Note that disabling the keyboard is
necessary in order to daemonize either the message router or the services, or
else standard input will stay connected and prevent backgrounding.

There is also a network control interface which you can enable on a TCP port
which can be used for testing / scripting / demo purposes. This should not be
used in production of course as it allows an untrusted client to send arbitrary
commands to the message router. The interface accepts single letter commands
which are identical to those read from the keyboard in `keyboard-mode: demo`
mode. For example, if it's running on localhost:7001, then the following (in
Bash) would cause it to mock-receive two messages and then print its state:

```bash
for i in n n S; do
  nc localhost 7001 <<< "$i"
done
```

The control interface is not currently implemented for services.

The library code and application code are loosely coupled and interested
developers are invited to create their own applications against the library
code. In particular the services, which in the future might be a DMC
service, a peer discovery service, a pub-sub service, or something else, don't
actually send or manipulate the communicated data in a useful way yet, hence the
designation ‘mock’ services.

### Concurrency architecture, streams, message parsing

The general architecture is based on the Lwt (”light-weight threads”) library.
The word “thread” is used a bit loosely here — they are better thought of as
cooperative asynchronous promises which greatly reduce the challenges of
classical threading, like deadlocks and race conditions. A server thread running
a streaming parser places incoming messages on an internal queue, which are then
handled by a handler thread, and the promises can be manipulated by functional
programming techniques (the data structure in which promises are stored form a
so-called monad). This design allows very high throughput and a system with very
high up-time that is easy to reason about, also in its failure modes.

The stream is consumed by a buffered streaming parser which is based on
Angstrom and which expects CBOR-encoded messages as specified in the spec. It's
not necessary to implement message boundaries: the parser, when provided a
parser with type `'a Angstrom.t`, will repeatedely return values of type `'a`
and leave any unparsed input in the stream for a future attempt. The parser is
generally specified as a series of one or more choices designating the allowed
input. Because the parser is always listening on the stream, a client connection
which abruptly terminates generates a server exception. This exception is
harmless, but a client should allow the server to terminate more gracefully by
sending the CBOR-encoded bytestring `"bye"`.

Cooperative multitasking also eliminates a possible race condition that would
otherwise arise when serializing or deserializing the state — incoming messages
and timer events are delayed until the operation is finished.

### Authentication and encryption

All services in the system (where “services” here refers to both the message
router and local services) must possess an ED25519 keypair and a valid
certificate. We provide tools for generating these using the `openssl` library.

A valid certificate must contain a common name for the service. We give every
service the same common name, ‘upsycle-node’. See the configuration files if you wish
to change this. This makes it easy for services to connect to each other: the
common name must be specified when making a connection, and the TLS library will
check it against the peer’s certificate. We would otherwise need a complex
system of naming for all the possible services in the system.

The service opening the connection (the client, which may be a local service
or a message router) will request a certificate from the peer (the server,
which is always a message router). The client possesses a public key which it
confidently believes belongs to the server. This key must have been learned
out-of-band for now, and eventually will be learned via the system itself.

Upon connection it will validate the names and also validate that the server
has the corresponding private key. This is what is meant by eliminating the
need for a certificate authority: as long as the peer can prove that it
possesses the corresponding private key, the connection is considered properly
authenticated.

We also enable client authentication, which is crucial so that the server can
also perform the same check on the client. We manually pass a ‘key fingerprint
authenticator’ to the call to Conduit to accomplish this.

Encryption is a simpler story: it is a property of all TLS connections. While
TLS provides strong encryption guarantees, note that this is different from
end-to-end encryption for multicast groups, which is not implemented by this
software. This is the property which would provide a guarantee that
only authorized group members can read the message payloads, while
intermediate message routers can read the routing information but not the
payload. This is referred to in the spec under the heading “End-to-end security
for groups” and is expected to become part of UPSYCLE at a later stage.

All messages require a valid signature and `upsycle-router` will immediately
drop any which lack one.

### Message hashes

The spec designates that message IDs should be calculated using Blake3 hashes.
The cryptographic library we use
[hacl-star](https://github.com/project-everest/hacl-star) does not provide
these, however, so we use Blake2 hashes. Blake3 supported is expected to be
fairly simple to add, by implementing OCaml bindings to an appropriate
cryptographic library written in C. We allow the choice of `Blake2b_32`,
`Blake2b_256`, `Blake2s_32`, and `Blake2s_128`.

### Key-value store

We have not implemented a key-value store. This is called for in the spec in
order to maintain the state of the message router (the routing table and the
subscriptions table), and to maintain a cache of recently seen messages. We
perform these tasks in-process using OCaml data structures instead, which are
properly serialized and deserialized as desired.

### Message queue and serialization

The message router maintains a temporary queue, per destination, of messages it
can not currently deliver because the peer connection is down. These messages
expire based on their `ttl` and `expiry` fields. Upon serialization the timer is
frozen and its state is also serialized. Note that only the remaining number of
seconds is stored. Upon deserialization, all active timers are cancelled, the
current queue is thrown away and replaced with the new one, and the timers are
started again. They do not check against an absolute clock again. This is
expected to correspond with the case that interruptions in the message router
process are brief and the process is quickly restored.

Messages which are about to expire are indicated in the output of
`upsycle-router` with a red envelope icon.

### Seen

`upsycle-service` maintiains in its internal state a set of message IDs per
topic which it has recently seen. This list of IDs is sent in the `seen` field
when sending multicast updates so that the receiver can reason about causality
and request missed messages. We assume that `upsycle-router` can also send
multicast updates. However we have not implemented a `seen` cache for
`upsycle-router`. This point needs refinement.

Another scenario to think about is it A1 sends a multicast update X to A2, A2
will store that message in its `seen`. If A2 then sends a message to A1, A1 will
erroneously think that it has missed X and request it from the message router.
A1 should probably store X in its own `seen` header or otherwise keep track of
messages it has sent. This point may also need refinement.

### PULL messages

When a message router receives a PULL message for a certain message ID, it
checks its cache to try to fulfill it. If it doesn't have the message, it
currently does not try to relay the message to a different message router,
though that might make sense. This point may need refinement.

### Routing table

There are three kinds of routes which can be stored in the routing table:
a “remote service”, a “remote router”, and a “local service”. Keep in mind that
services always connect to message routers, and never the other way around, but
once the connection is established, data flow is two-way of course.

A remote service is routed through an “indirect route”, meaning that it has to
go through a remote router to get there. A remote router is routed through a
“direct route”, and a service through an “anonymous route”. Indirect and direct
routes are either anonymous or known. Anonymous routes are always inbound while
known routes can be outbound or inbound. A known route contains IP/port
information so that the message router can open an outbound connection if it
wishes, while an anonymous route is so called in order to emphasize that the
router does not possess information for re-establishing the route if it is
broken. Anonymous routes can also be defunct. A route becomes defunct after the
connection is closed, or upon deserialization. And finally a known route may or
may not contain an open connection to the peer. Once established, connections
generally stay open for as long as possible.

When receiving a unicast message from a local or remote service,
we add an entry to the routing table.

### ocaml-conduit backend

`ocaml-conduit` can be configured to use either a pure OCaml TLS stack (the
`tls` library or the well-known C library `OpenSSL`). The backend is
set by default to use the pure OCaml stack. It can be changed by a
switch to `bin/message-router` if you are experimenting with something, but our
`ocaml-conduit-dream` library does not work with `OpenSSL` and you
won't be able to run this software. Note that this is not really a limitation:
the pure OCaml stack fits very well in the Mirage ecosystem and is lighter and
in theory easier to keep secure.

### `ocaml-conduit-dream` fork

The original [https://github.com/mirage/ocaml-conduit](ocaml-conduit) API does
the tricky work of establishing a TLS connection transparently and
authenticating the peer's certificate and public key if necessary, then provides
input and output channels once a connection has been established. But it doesn't
allow the calling library to then query information about the connection, in
particular the public key of the peer. Our fork augments the callbacks for
handling a connection (in the server case) and the return value of a successful
connection (in the client case) to include extra information which is retrieved
from the peer's certificate.

We then discovered that this information was missing in the server callback,
even when TLS client authentication was enabled, meaning that the server should
have successfully checked the client's certificate. This seemed to be due to a
bug in the upstream package (see [issue #401](https://github.com/mirage/ocaml-conduit/issues/401)),
which caused the client authentication step to be skipped, even when enabled. This has also been
fixed in the fork.

## Example scenario

In the directory example/ of the repository, one can play with the
software. There is a directory example/conf/, which contains config files to run
three message routers: A, B and C, and four sevices: A1, A2, A3 and B1.
As the names suggest, the services A1, A2 and A3 use A as their message
router, and B1 uses B. Currently, C doesn't have local services.
This is illustrated in the image below;

![](images/example_configuration.svg)

To deploy the message routers and services, one first has to generate
keys and certificates for each. This can be done by invoking

```shell
# Generate fresh ED25519 private keys and SSL certificates.
example/gen-keys-and-certs
```

which generates cert-files and key-files in the example/priv/cert directory.
If the one encounters errors, it is a good idea to repeat the previous step.
If one wants to add or remove routers or services for the key-generation,
one can adjust the file example/vars.sh. Now, we can start routers and
services, to do this use:

```shell
example/run-message-router x
```

and

```shell
example/run-service yi
```

where x can be { a | b | c } and yi can be { a1 | a2 | a3 | b1 }.
In the config files of the message routers --to be found in eg/conf--
you can see that in this example, all the routers use localhost to serve,
and the respective ports which they listen to are

`a --> 7000`
`b --> 8000`
`c --> 9000`

In the config files there are quite some variables that can be tweaked;
the files for router a and service a1 give information about what most
of the options are, but there are two things which are explained in the
files for router b and router c. If a service is started before the
corresponding message router is running, it doesn't start. The design
of the system implies that a service needs a connection to a router,
otherwise it cannot participate in the network.

### Running only a message router

So, let's say we start message router a by `eg/run-message-router a`.
If all works out well, press `h` for a help message, or other keys to
perform actions. We give an overview here for completeness:

| Key | description
|-|-
| h | Show help message.
| n | Simulate receiving a unicast message with random destination.
| N | Simulate receiving 100 messages (as though sent using 'n') for each of 10 random destinations.
| m | Simulate receiving a unicast message addressed to this instance of upsycle-router.
| M | Simulate receiving 1000 messages (as though sent using 'm').
| p | Send a multicast peer-advertisement message to known remote instances of upsycle-router.
| P | Simulate receiving a multicast peer-advertisement message addressed to a random topic (group).
| S | Print the current state of this instance on the console.
| s | Serialize the current state of this instance in CBOR.
| r | Deserialize the state of this instance and replace the current state.
| t | Clear the routing table.
| T | Clear the multicast table.
| c | Clear the message queue.
| C | Clear the message cache.
| q | Stop this instance.

Quite a few of the keys correspond to 'Simulate receiving
... message(s)'. This can be done without connections to other routers
or services, because all it does is pushing the messages to the stream
of the message router (which would also happen if it would really receive
a message from another source) and then the message router processes these
messages. Output is shown on the console. The message router holds a state,
which can printed using `S`, and serialized and deserialized in CBOR
(it is then stored in a tempory file). It is instructive to print the state,
press a few keys and print the state again. For example, pressing `N`,
and shortly after `S` (and some more times `S`), shows that when the
message router receives unicast messages addressed to a local service,
it will try to relay them, and if the service is not currently up, it will queue
them for some time, depending on the queuing time which is stored in the message.


### Running services as well

Assuming message router a is running, one can start services a1, a2 and a3
or a subset (in a new terminal window). Again, pressing `h` gives a help
message, which shows the following, but with more detailed descriptions:

| Key | description
|-|-
| h | Show help message.
| j | Send JOIN messages for the multicast topics given in the configuration.
| l | Send LEAVE messages for all multicast topics we are subscribed to.
| m | Publish a multicast message to all groups that this service is a member of.
| q | Log off from the message router and quit the service.
| Q | Force-quit this service.
| p | Send a unicast message with a trivial body to all known services.
| S | Print the current state of this service on the console.
| T | Clear the multicast table.

In the config file of the service, a number of public or private keys is
included, which belong to a topic. They will appear in the state, as can be
seen by pressing `S`. A private key gives writing permission,
a public one reading. To notify the message router that a service is
interested these topics, you can send JOIN messages by pressing `j`.
The router will send a JOIN acknowledgement, if all
is in order, and will pass on multicast messages addressed for this topic to
the service from now on. Then, pressing `m` will send a multicast message to all
the groups that the service is a member of.
Upon receiving these multicast messages, the message router will look into
its routing table, see whether there are (other) services interested and
relay the message.

An easy way to demonstrate this is the following:

- start message router a
- start service a1
- start service a2
- press `j` for service a1 and service a2
- press `m` for service a1

- In the output of a1, one can see that it succesfully published to three topics;
- In the output of a, it shows that two multicast messages were received with
one subscriber, and one with no subscribers,
- The output of a2 gives that two multicast messages were received (and
they match with the ones that the message router transmitted).

Looking at the image above, the two topics must have been 'weasels' and 'whales'.

## License

Copyright © 2021 [Alleycat](https://alleycat.cc) & [petites singularités](https://ps.lesoiseaux.io)

See [LICENSES] for licensing details. This repository is [REUSE]-compliant.

[![REUSE status](https://api.reuse.software/badge/gitlab.com/public.dream/upsycle/upsycle-router)](https://api.reuse.software/info/gitlab.com/public.dream/upsycle/upsycle-router)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.

[LICENSES]: ./LICENSES
[REUSE]: https://reuse.software

## Discussion

You're welcome to discuss the software with the [DREAM Catchers]!

[DREAM Catchers]: https://dream.public.cat/c/dream-catchers/15

![NGI POINTER](https://dream.public.cat/ngi-pointer.png)

This is experimental software made for [P2Pcollab] within the DREAM project.

This project has received funding from the European Union’s Horizon 2020 research and innovation programme within the framework of the [<abbr title="Next Generation Internet">NGI</abbr>-POINTER](https://pointer.ngi.eu/) Project funded under grant agreement No 871528.

![European Commission](https://dream.public.cat/europe.png)

[P2Pcollab]: https://p2pcollab.net/
