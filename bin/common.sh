set-conduit-backend () {
  opt_conduit_backend=$1
  if [ "$opt_conduit_backend" = ocaml-tls ]; then
    xport CONDUIT_TLS native
  elif [ "$opt_conduit_backend" = openssl ]; then
    xport CONDUIT_TLS openssl
  elif [ -z "$opt_conduit_backend" ]; then
    info 'No conduit backend chosen, defaulting to ocaml-tls'
    xport CONDUIT_TLS native
  else
    error "$USAGE"
  fi
}
