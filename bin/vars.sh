rootdir="$bindir"/..
confdir="$rootdir"/conf
confupsycledemo="$rootdir"/upsycle-config-demo
ocamldirstub=upsycle-router
messagerouterexe="$ocamldirstub"/bin/message_router.exe
serviceexe="$ocamldirstub"/bin/mock_service.exe
privdir="$rootdir"/priv
certdir="$privdir"/cert
keydir="$privdir"/key
opensslconf="$confdir"/openssl-x509-self-signed.conf
certexpirydaysdefault=1
services=(message-router-a message-router-b message-router-c service-a1 service-a2 service-a3 service-b1)
