egdir="$bindir"
rootdir="$egdir"/..
confdir="$egdir"/conf
binmaindir="$rootdir"/bin
bingenkeysandcerts="$binmaindir"/gen-keys-and-certs
binrunmessagerouter="$binmaindir"/message-router
binrunservice="$binmaindir"/service

certdir="$bindir"/priv/cert
# --- services and message routers
services=(message-router-a message-router-b message-router-c service-a1 service-a2 service-a3 service-b1)
