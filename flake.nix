{
  description = "Message router";

  inputs = {
    sbrpkgs.url = "git+https://gitlab.com/public.dream/libs/ocaml-seaboar";
  };

  outputs = { self, nixpkgs, sbrpkgs }:
    let
      supportedSystems = [
        "x86_64-linux" "aarch64-linux" "armv7l-linux"
        "x86_64-darwin" "aarch64-darwin"
      ];
      supportedOcamlPackages = [
        "ocamlPackages_4_10"
        "ocamlPackages_4_11"
        "ocamlPackages_4_12"
        "ocamlPackages_4_13"
      ];
      defaultOcamlPackages = "ocamlPackages_4_12";

      forAllOcamlPackages = nixpkgs.lib.genAttrs supportedOcamlPackages;
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      nixpkgsFor =
        forAllSystems (system:
          import nixpkgs {
            inherit system;
            overlays = [ self.overlay ];
          });
    in
      {
        overlay = final: prev:
          with final;

          let mkOcamlPackages = prevOcamlPackages:
                with prevOcamlPackages;
                let ocamlPackages = rec {
                      inherit ocaml;
                      inherit findlib;
                      inherit ocamlbuild;
                      inherit opam-file-format;
                      inherit buildDunePackage;

                      hacl-star-raw =
                        stdenv.mkDerivation rec {
                          pname = "ocaml${ocaml.version}-hacl-star-raw";
                          version = "0.4.1";

                          src = fetchzip {
                            url = "https://github.com/project-everest/hacl-star/releases/download/ocaml-v${version}/hacl-star.${version}.tar.gz";
                            sha256 = "05djOEIw6tLWP7eQPczUJjedyeVVJyvxPjpyTmkXlRY=";
                            stripRoot = false;
                          };

                          sourceRoot = "./source/raw";

                          minimalOCamlVersion = "4.05";

                          postPatch = ''
                              patchShebangs ./
                                '';

                          preInstall = ''
                              mkdir -p $OCAMLFIND_DESTDIR/stublibs
                                '';

                          installTargets = "install-hacl-star-raw";

                          dontAddPrefix = true;
                          dontAddStaticConfigureFlags = true;
                          configurePlatforms = [];

                          buildInputs = [
                            which
                            ocaml
                            findlib
                          ];

                          propagatedBuildInputs = [
                            ctypes
                          ];

                          checkInputs = [
                            cppo
                          ];

                          doCheck = true;

                          meta = {
                            description = "Auto-generated low-level OCaml bindings for EverCrypt/HACL*";
                            license = lib.licenses.asl20;
                            maintainers = [ lib.maintainers.ulrikstrid ];
                            platforms = ocaml.meta.platforms;
                          };
                        };

                      hacl-star =
                        buildDunePackage {
                          pname = "hacl-star";
                          inherit (hacl-star-raw) version src meta doCheck minimalOCamlVersion;

                          useDune2 = true;

                          propagatedBuildInputs = [
                            hacl-star-raw
                            zarith
                          ];

                          buildInputs = [
                            cppo
                          ];
                        };

                      conduit-dream =
                        buildDunePackage rec {
                          pname = "conduit-dream";
                          version = "4.0.1-6.1";
                          src = fetchFromGitLab {
                            owner = "public.dream";
                            repo = "upsycle/ocaml-conduit-dream";
                            rev = "c1bcbd6c1f57426c41322ebcdc4d7c24abcb869e";
                            sha256 = "Atn0G+n3AAjgNTaAN2LGLhsAgkoseie3m2FlVaWXuSs=";
                          };
                          useDune2 = true;

                          buildInputs = [ ppx_sexp_conv ];

                          propagatedBuildInputs = [ angstrom stringext astring ipaddr ipaddr-sexp sexplib uri ];
                        };

                      conduit-lwt-dream =
                        buildDunePackage rec {
                          pname = "conduit-lwt-dream";
                          version = "4.0.1-6.1";
                          src = fetchFromGitLab {
                            owner = "public.dream";
                            repo = "upsycle/ocaml-conduit-dream";
                            rev = "c1bcbd6c1f57426c41322ebcdc4d7c24abcb869e";
                            sha256 = "Atn0G+n3AAjgNTaAN2LGLhsAgkoseie3m2FlVaWXuSs=";
                          };
                          useDune2 = true;

                          buildInputs = [ ppx_sexp_conv ];
                          propagatedBuildInputs = [ conduit-dream ocaml_lwt sexplib ];
                        };

                      conduit-lwt-unix-dream =
                        buildDunePackage rec {
                          pname = "conduit-lwt-unix-dream";
                          version = "4.0.1-6.1";
                          src = fetchFromGitLab {
                            owner = "public.dream";
                            repo = "upsycle/ocaml-conduit-dream";
                            rev = "c1bcbd6c1f57426c41322ebcdc4d7c24abcb869e";
                            sha256 = "Atn0G+n3AAjgNTaAN2LGLhsAgkoseie3m2FlVaWXuSs=";
                          };
                          useDune2 = true;

                          buildInputs = [
                            ppx_sexp_conv
                          ];
                          propagatedBuildInputs = [
                            conduit-lwt-dream ocaml_lwt uri ipaddr ipaddr-sexp tls ca-certs lwt_ssl x509
                          ];
                        };

                      upsycle-router =
                        buildDunePackage rec {
                          pname = "upsycle-router";
                          version = "0.0.1";
                          src = self;

                          useDune2 = true;
                          doCheck = true;

                          propagatedBuildInputs = [
                            batteries
                            cmdliner
                            conduit-lwt-unix-dream
                            fmt
                            fpath
                            hacl-star
                            ipaddr
                            lwt_ppx
                            ppx_compose
                            ppx_deriving
                            ppx_deriving_yaml
                            ptime
                            yaml
                            x509
                            tls
                            ca-certs

                            seaboar
                            angstrom-lwt-unix
                            cbor
                            decoders
                            decoders-cbor
                            stdint
                          ];
                        };
                    };
                in ocamlPackages;
          in
            let allOcamlPackages =
                  forAllOcamlPackages (ocamlPackages:
                    mkOcamlPackages (
                      ocaml-ng.${ocamlPackages}
                      //
                      sbrpkgs.packages.${system}.${ocamlPackages}
                    ));
            in
              allOcamlPackages // {
                ocamlPackages = allOcamlPackages.${defaultOcamlPackages};
              };

        packages =
          forAllSystems (system:
            forAllOcamlPackages (ocamlPackages:
              nixpkgsFor.${system}.${ocamlPackages}));

        defaultPackage =
          forAllSystems (system:
            nixpkgsFor.${system}.ocamlPackages.upsycle-router);
      };
}
