open! Basic

let main () =
  let () = Upsycle_router.init_crypto () in
  let priv, pub = Util_crypto_io.generate_key_pair_base64 () in
  Lwt_fmt.printf "%s@.%s@." priv pub

let () =
  let () = Util_lwt.enable_colors () in
  let f = Lwt_main.run (main ()) in
  Generate_keys_config.parse f
