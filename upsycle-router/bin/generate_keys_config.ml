open Cmdliner

let cmd f =
  let module Fn = Filename in
  let bin = Sys.argv.(0) |> Fn.basename |> Fn.remove_extension in
  let doc = "Generate a Curve25519 keypair." in
  Term.(const f),
  Term.info bin ~doc ~exits:Term.default_exits

let parse cb =
  Term.(exit @@ eval (cmd cb))
