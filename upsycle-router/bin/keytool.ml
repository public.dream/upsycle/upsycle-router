module UCrypto_io = Util_crypto_io
module Uio = Util_io

type config = Keytool_config.config

let print_0x key =
  let acc = "0x" in
  let f acc ch = acc ^ Fmt.str "%02x" (Char.code ch) in
  let str = BatString.fold_left f acc key in
  Fmt.pr "%s@." str

let pretty_print_base64_pubkey key =
  let key' = Upsycle_router.Crypto.Mk.pubkey_exn key in
  Fmt.pr "%a@." Upsycle_router.Pp.pubkey key'

let pretty_print_base64_privkey key =
  let key' = Upsycle_router.Crypto.Mk.privkey_exn key in
  Fmt.pr "%a@." Upsycle_router.Pp.privkey key'

let print_pub pubkey =
  let () = print_0x pubkey in
  pretty_print_base64_pubkey pubkey

let print_opriv = function
  | None -> Fmt.pr "[not available]@."
  | Some privkey ->
      let () = print_0x privkey in
      pretty_print_base64_privkey privkey

let main (c: config) =
  let pub, opriv = match c.mode with
  | `Cert -> UCrypto_io.pubkey_of_certfile_exn, None
  | `Key -> UCrypto_io.(pubkey_of_keyfile_exn, Some privkey_of_keyfile_exn) in
  let get (f : ?print_stderr:bool -> string list -> string -> string) =
    f [c.openssl_cmd] c.file_path in
  let pubkey = get pub in
  let oprivkey = Option.map get opriv in
  let () = Fmt.pr "%a@." Uio.underline "public" in
  let () = print_pub pubkey in
  let () = Fmt.pr "%a@." Uio.underline "private" in
  print_opriv oprivkey

let () =
  let () = Uio.enable_colors () in
  Keytool_config.parse main
