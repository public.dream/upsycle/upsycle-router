open Cmdliner

type config = {
  openssl_cmd: string;
  mode: [ `Key | `Cert ];
  file_path: string;
}

let arg_file_path =
  let doc = "The path to the certificate or key file." in
  Arg.(required @@ opt (some file) None @@ info ["i"; "in"] ~docv:"cert-or-key-path" ~doc)

let arg_openssl_cmd =
  let doc = "The full path to the openssl binary." in
  Arg.(required @@ opt (some file) None @@ info ["o"; "openssl-cmd"] ~docv:"openssl-cmd" ~doc)

let arg_mode =
  let doc = "The type of input file." in
  let parser' = function
    | "key" -> `Ok `Key
    | "cert" -> `Ok `Cert
    | x -> `Error (Fmt.str "expected key or cert, got %s" x) in
  let printer' ppf = function
    | `Cert -> Fmt.pf ppf "cert"
    | `Key -> Fmt.pf ppf "key" in
  let conv' = Arg.pconv (parser', printer') in
  Arg.(required @@ opt (some conv') None @@ info ["t"; "type"] ~docv:"key/cert" ~doc)

let cmd f =
  let module Fn = Filename in
  let bin = Sys.argv.(0) |> Fn.basename |> Fn.remove_extension in
  let doc = "Extract public key as hex string from a certificate" in
  Term.(const f $ arg_openssl_cmd $ arg_mode $ arg_file_path),
  Term.info bin ~doc ~exits:Term.default_exits

let parse cb =
  let f openssl_cmd mode file_path = cb {
    openssl_cmd;
    mode;
    file_path;
  } in Term.(exit @@ eval (cmd f))
