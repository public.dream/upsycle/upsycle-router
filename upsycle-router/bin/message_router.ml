open! Basic

module UList = Util_pure.List
module ULwt = Util_lwt
module UOption = Util_pure.Option

type config = Upsycle_router.Message_router_config.t

let lwt_push push = Lwt.return % push % some

let mk_random_keypair = Upsycle_router.generate_key_pair
let mk_random_ttl, mk_random_expiry, mk_random_revision = Upsycle_router.(mk_random_ttl, mk_random_expiry, mk_random_revision)

let pp_pubkey = Upsycle_router.Pp.pubkey

let mk_msg () =
  let ttl = mk_random_ttl `Short in
  let exp = mk_random_expiry () in
  fun ?test_tamper x y z ->
    let test_tamper' = UOption.fold false id test_tamper in
    Upsycle_router.mk_unicast_message ~ttl ~exp ~test_tamper:test_tamper' x y z

let mk_random_port_exn () =
  Upsycle_router.Mk.port_exn (2000 + Random.int 50000)

let mk_random_ip_exn () =
  let w = Int.to_string @@ Random.int 256 in
  let x = Int.to_string @@ Random.int 256 in
  let y = Int.to_string @@ Random.int 256 in
  let z = Int.to_string @@ Random.int 256 in
  Upsycle_router.Mk.ip (Ipaddr.of_string_exn @@ BatString.join "." [w; x; y ; z])

let mk_random_pd_ps () =
  let _priv, pd_pub = mk_random_keypair () in
  let _priv, ps_pub = mk_random_keypair () in
  let x = Random.int 4 in
  match x with
  | 0 -> None, None
  | 1 -> Some pd_pub, None
  | 2 -> None, Some ps_pub
  | 3 -> Some pd_pub, Some ps_pub
  | _ -> failwith "mk_random_pd_ps: that's a surprise!"

let mk_random_grps () =
  let len = Random.int 3 in
  let rec f lst n =
    if n > 0 then
      let _, pubkey = mk_random_keypair () in
      let pubkey' = Upsycle_router.Mk.multicast_group_pubkey pubkey in
      f (pubkey' :: lst) (n - 1)
    else lst in
  f [] len

let mk_random_peer_advertisement_msg priv_remote_router pub_remote_router =
  let port = mk_random_port_exn () in
  let ip = mk_random_ip_exn () in
  let addr_type = `Tcp in
  let opd, ops = mk_random_pd_ps () in
  let grps = mk_random_grps () in
  Upsycle_router.mk_encoded_peer_advertisement_message
    ~id:pub_remote_router (ip, port, addr_type) (opd, ops) ~grps
    ~rev:(mk_random_revision ()) ~ttl:(mk_random_ttl `Medium) ~exp:(mk_random_expiry ()) ~priv_key:priv_remote_router

(* Make a message addressed to the message router, but with a test string as the
   body *)
let mk_random_msg_router_message ~dst =
  let src_priv, src_pub = mk_random_keypair () in
  mk_msg ()
    (src_priv, src_pub) dst
    Seaboar.Encode.(encode string "test body")

let mk_random_relay_message ~test_tamper ?dst () =
  let src_priv, src_pub = mk_random_keypair () in
  let test_tamper' = if test_tamper = 0. then false else
    let x = int_of_float (100. *. test_tamper) in
    1 + Random.int 100 <= x in
  let dst' = Util_pure.Option.fold'
     (fun _ -> snd (mk_random_keypair ()))
     id
     dst in
  mk_msg ()
    ~test_tamper:test_tamper'
    (src_priv, src_pub) dst'
    Seaboar.Encode.(encode string "test body")

(* --- the 'simulate receive' functions work by calling this function to place
   message directly on the stream. *)
let simulate_receive = lwt_push

let simulate_receive_random_multicast_peer_advertisement_message (cnf: Upsycle_router.Message_router_config.t) prefix push_stream =
  let prf fmt = Util_lwt.mk_prf prefix fmt in
  let priv_remote_router, pub_remote_router = mk_random_keypair () in
  let lst = cnf.multicast_groups_for_peer_advertisement in
  let pa_msg = mk_random_peer_advertisement_msg priv_remote_router pub_remote_router in
  let f mg_rw =
    let seen = Upsycle_router.mk_seen cnf.seen_size in
    let ourkey = cnf.pubkey in
    let multicast_pa_msg = Upsycle_router.mk_multicast_message
      ~ttl:(mk_random_ttl `Medium)
      ~exp:(mk_random_expiry ())
      ~seen
      mg_rw
      pa_msg in
   simulate_receive push_stream @@
     Upsycle_router.test_stream_msg prefix ourkey pub_remote_router multicast_pa_msg in
  (*   If the MR only has reading permissions for the multicast group, it cannot create one. So those groups
     are filtered out*)
  let lst_rw = lst
    |> List.map Upsycle_router.Acc.multicast_group
    |> UList.flatmap (function
      | `Rw (privkey, pubkey) -> [privkey, pubkey]
      | `Ro _ -> [])
    |> List.map Upsycle_router.Mk.multicast_group_rw
  in match List.length lst with
  | 0 -> prf "no peer-advertisement groups"
  | _ ->
      let* () = prf "simulating sending a peer-advertisement message to the following groups: %a"
        (Fmt.list Upsycle_router.Pp.multicast_group_rw) lst_rw in
      Lwt_list.iter_s f lst_rw

let simulate_receive_random_msg_router_message ourkey prefix push_stream =
  let msg = mk_random_msg_router_message ~dst:ourkey in
  let _, peerkey = mk_random_keypair () in
  simulate_receive push_stream @@
    Upsycle_router.test_stream_msg prefix ourkey peerkey msg

let simulate_receive_message_for_local_services (cnf: config) ?(num=1) prefix push_stream =
  let test_tamper = cnf.test_tamper_signatures_on_mock_messages in
  let ourkey = cnf.pubkey in
  let go' dst =
    let _, peerkey = mk_random_keypair () in
    Util_lwt.repeat num @@ fun _ ->
      let msg = mk_random_relay_message ~test_tamper ~dst () in
      let msg' = Upsycle_router.test_stream_msg prefix ourkey peerkey msg in
      simulate_receive push_stream msg' in
  match Upsycle_router.Message_router_config.keys_of_key_set cnf.local_services with
  | [] -> Lwt.return `No_services
  | dsts ->
      let* () = Lwt_list.iter_p go' dsts
      in Lwt.return (`Ok (List.length dsts))

let commands_normal (cnf: config) info =
  let prf fmt = Util_lwt.mk_prf info fmt in [
    `C 'h', (fun _ -> ULwt.help_message [
      "h", "Show this help message", "";
      "n", "Simulate receiving a unicast message for each known local \
        service.", "\
        Messages simulated in this way are pushed straight to the stream, and \
        since they don't arrive via the TCP connection they don't \
        have routing information.@\n\
        Upsycle-router will then relay it or queue it.";
      "N", "Simulate receiving 100 messages (as though sent using 'n') for \
        each known local service.", "";
      "m", "Simulate receiving a unicast message addressed to this instance of upsycle-router",
        "The destination of the message is the pubkey of this upsycle-router \
        instance. The message doesn't contain a useful message router \
        instruction. The message is affected by the \
        'test-tamper-signatures-on-mock-messages' configuration field.";
      "M", "Simulate receiving 1000 messages (as though sent using 'm').", "";
      "p", "Send a multicast peer-advertisement message to known remote instances of upsycle-router.",
        "[Currently, this is not implemented] \
        There is a list of known remote message routers in the configuration, \
        and for each one, there is a list of multicast topics (groups) for peer \
        advertisements. The message is sent to each topic on each remote \
        message router.";
      "P", "Simulate receiving a multicast peer-advertisement message addressed to a random topic (group).",
        "This instance will immediately subscribe itself to the topic so that \
        the group isn't empty and the message gets processed.";
      "S", "Print the current state of this instance on the console.", "";
      "s", "Serialize the current state of this instance in CBOR.", "\
        The encoded state will be stored in a file specified in the \
        configuration. The entire state is serialized, with one exception: \
        open connections are closed upon serialized and restored as defunct \
        connections when deserializing.@\n\
        The timers, which are running for queued messages, will stop, and \
        start again upon deserializing.";
      "r", "Deserialize the state of this instance and replace the current state.",
        "See 's' for notes.";
      "t", "Clear the routing table.", "";
      "T", "Clear the multicast table.", "";
      "c", "Clear the message queue.", "";
      "C", "Clear the message cache.", "";
      "q", "Stop this instance.", "";
  ]);
  `C 'S', (fun _ ->
	  let t = match cnf.print_messages_on_console with
	  | `Long -> `Long
	  | `Short -> `Short
	  | `None -> `Short (* presumably you still want output on keypress *) in
	  prf "current state: @[%a@]@." (Upsycle_router.pp_message_router_state' t) Fmt.nop);
  `C 's', (fun _ ->
	  let* () = prf "serializing state" in
	  let t = Upsycle_router.now_f () in
	  let* () = Upsycle_router.serialize_message_router_state cnf in
	  prf "serializing state successful (took %.0f ms)" ((Upsycle_router.now_f () -. t) *. 1000.));
  `C 'r', (fun _ ->
	  let f () =
		let* () = prf "deserializing state" in
		let t = Upsycle_router.now_f () in
		let* state_str' =
		  Lwt_io.chars_of_file cnf.serialize_state_path
		  |> Lwt_stream.to_string in
		let* () = Upsycle_router.(deserialize_and_set_message_router_state (unqueue_message cnf.msg_id_hash_function) cnf.seen_size cnf.cache_size) state_str' in
		prf "deserializing state successful (took %.0f ms)" ((Upsycle_router.now_f () -. t) *. 1000.) in
	  let fail' pp e = Util_lwt.warn "key_thread: Unable to deserialize state: %a" pp e in
	  begin try%lwt f () with
	  | Failure e -> fail' Fmt.string e
	  | e -> fail' Fmt.exn e end);
]

let commands_demo (cnf: config) info push_stream =
  let prf fmt = Util_lwt.mk_prf info fmt in [
    `C 'c', (fun _ ->
        let+ () = prf "clearing message queue" in
        let () = Upsycle_router.reset_message_queue () in
        ());
    `C 'C', (fun _ ->
        let+ () = prf "clearing message cache" in
        let () = Upsycle_router.reset_message_cache () in
        ());
    `C 't', (fun _ ->
        let+ () = prf "clearing routing table" in
        let () = Upsycle_router.clear_routing_table () in
        ());
    `C 'T', (fun _ ->
        let+ () = prf "clearing multicast table" in
        let () = Upsycle_router.clear_multicast_table () in
        ());
    `C 'm', (fun _ ->
        let* () = prf "mock-receiving random message router message" in
        simulate_receive_random_msg_router_message cnf.pubkey info push_stream);
    `C 'M', (fun _ ->
        let* () = prf "mock-receiving 1000 random message router messages" in
        Util_lwt.repeat 1000 @@ fun _ ->
          simulate_receive_random_msg_router_message cnf.pubkey info push_stream);
    `C 'n', (fun _ ->
        let* () = prf "mock-receiving random messages for all local services" in
        simulate_receive_message_for_local_services cnf info push_stream >>= function
          | `No_services -> prf "no local services (check conf)"
          | `Ok n -> prf "mock-received a random message for %s" (Util_pure.english_plural' "local service" n));
    `C 'N', (fun _ ->
        let* () = prf "mock-receiving 100 random messages each to all local services" in
        simulate_receive_message_for_local_services ~num:100 cnf info push_stream >>= function
          | `No_services -> prf "no local services (check conf)"
          | `Ok n -> prf "mock-received a random message for %s" (Util_pure.english_plural' "local service" n));
    `C 'P', (fun _ ->
        let* () = prf "mock-receiving a random peer-advertisement message" in
        simulate_receive_random_multicast_peer_advertisement_message cnf info push_stream);
    `C 'p', (fun _ ->
        let* () = prf "sending multicast peer-advertisement message to known remote routers" in
        let f (remote_router: Upsycle_router.remote_router) =
          let remote_router_addr = remote_router.pubkey in
          let* res = Upsycle_router.send_peer_advertisement_about_us cnf
            ~addr_type:`Tcp ~rev:(mk_random_revision ())
            ~ttl_pa:(mk_random_ttl `Medium) ~ttl_multicast:(mk_random_ttl `Medium)
            ~exp_pa:(mk_random_expiry ()) ~exp_multicast:(mk_random_expiry ())
            info remote_router_addr in
          match res with
          | `Ok x -> prf "successfully published to %d groups" x
          | `No_groups -> prf "no groups found to publish to" in
        match cnf.remote_routers with
        | [] -> prf "No remote_routers found"
        | routers -> Lwt_list.iter_p f routers
    );
  ]

let key_default info = `Default, fun c -> Upsycle_router.keys_common info c

let key_thread' keyboard_mode (cnf: config) push_stream =
  let info' = "[CLI]" in
  let commands_default' = [ key_default info' ] in
  let commands_normal' = commands_normal cnf info' in
  let commands_demo' = commands_demo cnf info' push_stream in
  Util_lwt.start_key_thread @@ commands_default' @ match keyboard_mode with
    | `Normal -> commands_normal'
    | `Demo ->   commands_normal' @ commands_demo'

let key_thread =
  let f mode' cnf push_stream =
    let () = Util_lwt.single_key_mode () in
    key_thread' mode' cnf push_stream
  in function
    | `None -> []
    | `Normal -> [f `Normal]
    | `Demo -> [f `Demo]

let control_thread =
  let go host port (cnf: Upsycle_router.Message_router_config.t) push_stream =
    let tls_own_key = `None in
    let src = host in
    let* ctx = Conduit_lwt_unix.init ~tls_own_key ~src () in
    let info' = "[control-thread]" in
    let commands' = commands_normal cnf info' @ commands_demo cnf info' push_stream in
    let handle_connection _flow ic _oc _oconnection_info =
      let* line = Lwt_io.read_line ic in
      match String.length line with
      | 1 -> Util_lwt.command_handler commands' "control_thread" line.[0]
      | _ -> failwith "bad command" in
    let on_exn exn' = Fmt.epr "got exception: %a@." Fmt.exn exn' in
    let mode = `TCP (`Port (Upsycle_router.Acc.port port)) in
    Conduit_lwt_unix.serve ~on_exn ~ctx ~mode handle_connection
  in function
    | None -> []
    | Some (host, port) -> [go host port]

let main (cnf: config) keyboard_mode =
  let threads = List.flatten [
    key_thread keyboard_mode;
    control_thread (Upsycle_router.Acc.control_interface cnf.control_interface);
  ] in
  Upsycle_router.start_message_router ~threads cnf

let () =
  let _ = Util_lwt.at_exit_plain (fun _ -> Fmt.pr "* bye.@.") in
  let () = Util_lwt.enable_colors ~stderr:true () in
  let () = Random.init (Unix.gettimeofday () |> int_of_float) in
(*   let cb cnf bin_cnf _openssl_cmd = Lwt_main.run @@ main cnf bin_cnf in *)
  let cb cnf bin_cnf = Lwt_main.run @@ main cnf bin_cnf in
  Message_router_config.parse cb
