module UOption = Util_pure.Option
module UResult = Util_pure.Result
module UYaml = Util_yaml

open! Basic

type remote_router' = {
  cert_file: string [@key "cert-file"];
  ip: Upsycle_router.ip [@key "ip"];
  port: Upsycle_router.port [@key "port"];
  routername: string [@key "routername"];
}
[@@deriving yaml]

let mk_remote_router (r: remote_router') = `Rr (
  `Cf r.cert_file,
  `Ip r.ip,
  `P r.port,
  `Rn r.routername
)

type multicast_table_entry' = {
  group: string [@key "group"];
  subscribers: string list [@key "subscribers"];
}
[@@deriving yaml]

let mk_multicast_table_entry (mt: multicast_table_entry' ) = `Mte (
  `Gp mt.group,
  `Subs mt.subscribers
)

let addr_ip_port_of_yaml =
  let open UResult.Letsfix in function
  | `O ["addr", `String addr'; "ip", ip'; "port", port'] ->
      let f ipp = addr', ipp in f <$>
      Upsycle_router.ip_port_of_yaml (`O ["ip", ip'; "port", port'])
  | _ -> error_msg "addr_ip_port_of_yaml"

(* --- Local service (Ls) and Remote service anonymous (Rsa) are useful for
   testing and probably not useful in real life. *)
type route' = [ `Mrr of
  [ `Addr of string ] * [ `Rt of [
    | `Ls (* local-service *)
    | `Rsa (* remote-service anonymous *)
    | `Rsk of string * Upsycle_router.ip_port (* remote-service known *)
    | `Rra (* remote-router anonymous *)
    | `Rrk of Upsycle_router.ip_port (* remote-router known *)
  ]]
]

let route_route'_of_yaml =
  let open UResult.Letsfix in function
  | `O ["type", `String "local-service"] -> ok `Ls
  | `O ["type", `String "remote-service"] -> ok `Rsa
  | `O ["type", `String "remote-router"] -> ok `Rra
  | `O ["type", `String "remote-service";
        "message-router-connection-info", info'] ->
    let f (addr, ipp) = `Rsk (addr, ipp) in f <$>
    addr_ip_port_of_yaml info'
  | `O ["type", `String "remote-router";
        "connection-info", info'] ->
    let f ipp = `Rrk ipp in f <$>
    Upsycle_router.ip_port_of_yaml info'
  | _ -> error_msg "route'_of_yaml"

let route'_of_yaml =
  let open UResult.Letsfix in function
  | `O ["addr", `String addr; "route", route] ->
      let f route = `Mrr (`Addr addr, `Rt route) in
      f <$> route_route'_of_yaml route
  | _ -> error_msg "route_route'_of_yaml"
let route'_to_yaml = UYaml.to_yaml_noop

type keyboard_mode = [ `None | `Normal | `Demo ]
let keyboard_mode_of_yaml = function
  | `String "none" -> Ok `None
  | `String "normal" -> Ok `Normal
  | `String "demo" -> Ok `Demo
  | _ -> Error (`Msg "keyboard_mode_of_yaml")
let keyboard_mode_to_yaml = UYaml.to_yaml_noop

type percent = float
let percent_of_yaml =
  let err = Error (`Msg "percent_of_yaml") in
  function
  | `Float f -> if f < 0. || f > 1.
    then err else Ok f
  | _ -> err
let percent_to_yaml = UYaml.to_yaml_noop

(** This type is internal to this module and is not yet the config type which
    the library knows about. It consists of primitive types and also types like
    [control_interface] which can be derived using an _of_yaml function from
    [Upsycle_router]. Then we process these values in a second step and pass
    them to the [mk] function below. Steps which require [openssl_cmd] happen in
    the second pass. The mock_service config works the same way. *)
type config' = {
  allow_test_body: bool [@key "allow-test-body"];
  cert_path: string [@key "cert-file"];
  ip: Upsycle_router.ip [@key "ip"];
  keyboard_mode: keyboard_mode [@key "keyboard-mode"];
  control_interface: Upsycle_router.Message_router_config.control_interface option [@key "control-interface"];
  join_ack_delay: float [@key "join-ack-delay"];
  key_path: string [@key "key-file"];
  local_services: string list [@key "local-services"];
  seen_size: int [@key "seen-size"];
  cache_size: int [@key "cache-size"];
  remote_routers: remote_router' list [@key "remote-routers"];
  message_router_name: string [@key "message-router-name"];
  msg_id_hash_function: Upsycle_router.msg_id_hash_function [@key "message-id-hashing-function"];
  multicast_groups_for_peer_advertisement: Upsycle_router.multicast_group list [@key "multicast-groups-for-peer-advertisement"];
  multicast_table_init: multicast_table_entry' list [@key "multicast-table-init"];
  openssl_cmd: Upsycle_router.command [@key "openssl-cmd"];
  port: Upsycle_router.port [@key "port"];
  prefer_multicast_requestor: Upsycle_router.multicast_requestor [@key "prefer-multicast-requestor"];
  print_messages_on_console: Upsycle_router.print_messages_on_console [@key "print-messages-on-console"];
  key_printing_short: bool [@key "key-printing-short"];
  routing_table_init: route' list [@key "routing-table-init"];
  serialize_state_path: string [@key "serialize-state-file"];
  test_tamper_signatures_on_mock_messages: percent option [@key "test-tamper-signatures-on-mock-messages"];
  update_routing_table_on_client_connect: bool [@key "update-routing-table-on-client-connect"];
}
[@@deriving yaml]

let config_of_yaml conf_dir yaml =
  let open BatOption.Infix in
  let open Rresult.R.Infix in
  config'_of_yaml yaml >>= fun config' ->
    let config = Upsycle_router.Message_router_config.mk ~base_dir:conf_dir
      config'.allow_test_body
      config'.cache_size
      config'.cert_path
      config'.control_interface
      config'.ip
      config'.join_ack_delay
      config'.key_path
      config'.key_printing_short
      config'.local_services
      config'.message_router_name
      config'.msg_id_hash_function
      (List.map mk_multicast_table_entry config'.multicast_table_init)
      config'.multicast_groups_for_peer_advertisement
      config'.openssl_cmd
      config'.port
      config'.prefer_multicast_requestor
      config'.print_messages_on_console
      (List.map mk_remote_router config'.remote_routers)
      config'.routing_table_init
      config'.seen_size
      config'.serialize_state_path
      (config'.test_tamper_signatures_on_mock_messages |? 0.)
      config'.update_routing_table_on_client_connect in
    let bin_config = config'.keyboard_mode in
    Ok (config, bin_config)

open Cmdliner

let ($) = Term.($)

let arg_conf =
  let doc = "The path to the YAML config file." in
  Arg.(required @@ opt (some file) None @@ info ["c"; "config"] ~docv:"config-yaml-path" ~doc)

let cmd f =
  let bin' =
    let clean' = Filename.(remove_extension % basename) in
    Sys.argv.(0) |> clean' in
  let doc' = "Run UPSYCLE message router." in
  Term.const f $ arg_conf,
  Term.info bin' ~doc:doc' ~exits:Term.default_exits

let parse cb =
  let f conf_path =
    let conf_dir = Filename.dirname conf_path in
    let config, bin_config = Util_yaml_io.parse_file (config_of_yaml conf_dir) conf_path
    in cb config bin_config
  in Term.exit (Term.eval (cmd f))
