open Basic

module ULwt = Util_lwt

let send_message_to_peer ?(prepend="") ~ttl ~exp
    (cnf: Upsycle_router.Service_config.t) msg_router_connection peer_pubkey body =
  let prf fmt = Util_conduit.mk_prf ~prepend msg_router_connection fmt in
  let* () = prf "sending message to peer (%a) through message router"
    Upsycle_router.Pp.pubkey peer_pubkey in
  Upsycle_router.send_message_to_peer ~ttl ~exp ~body
    msg_router_connection cnf.our_privkey cnf.our_pubkey peer_pubkey

let quit msg_router_connection =
  let prf fmt = Util_conduit.mk_prf msg_router_connection fmt in
  let* () = prf "saying goodbye" in
  Util_conduit.send_terminate_message msg_router_connection

let key_thread' keyboard_mode (cnf: Upsycle_router.Service_config.t) state_r msg_router_connection =
  let prefix' = "[CLI key-thread]" in
  let prf fmt = Util_conduit.mk_prf ~prepend:prefix' msg_router_connection fmt in
  let default' = Upsycle_router.keys_common prefix' in
  let keys_normal = [
    `C 'h', (fun _ -> ULwt.help_message [
      "h", "Show this help message", "";
      "j", "Send JOIN messages for the multicast topics (groups) given in the configuration.",
          "These groups are given in the configuration. The host message router \
          updates its multicast table (when the group is new, it creates a new \
          entry, and when the service is new it creates a new entry in the list \
          of services belonging to the group), and sends a JOIN_ACK as answer to \
          each JOIN message. Upon receiving this, this service updates its \
          state, hereby remembering that it's a member of a certain multicast \
          group now. ";
      "l", "Send LEAVE messages for all multicast topics (groups) we are subscribed to.", "";
      "m", "Publish a multicast message to all groups that this service is a member of.",
          "The groups we are interested in are looked up in the state, and a \
          multicast message is sent to each one.";
      "q", "Log off from the message router and quit the service.",
          "This is achieved by sending the CBOR-encoded string \"bye\" to the message router.";
      "Q", "Force-quit this service.",
          "This will quit without logging off from the message router. Use this \
          when the connection is no longer available for whatever reason, which \
          makes it impossible to send the log-off string.";
      "p", "Send a unicast message with a trivial body to all known services.",
          "The message is sent to our host message router, whose job it is to relay it.";
      "P", "Send one or more PULL messages to the message router.",
          "This will send one PULL message per message ID in our local ‘seen’ array. \
          This doesn't correspond to a real-world use case of course because there \
          would be no reason to re-request messages we've already seen.";
      "S", "Print the current state of this service on the console.",
          "Currently, this consists of a list of multicast groups. For each group, \
          this service has either the public key, or a private key and a public key. \
          The public key gives the right to receive updates, the private one to write them.";
      "T", "Clear the multicast table.", "";
    ]);
    `C 'q', (fun _ ->
        let* () = quit msg_router_connection in default' 'q'
        |> ULwt.decorate_rejection "Unable to close connection");
    `C 'Q', (fun _ -> default' 'q'
        |> ULwt.decorate_rejection "Unable to force quit -- time for ctrl-c");
    `C 'S', (fun _ -> Upsycle_router.service_show_state cnf state_r prefix');
  ] in
  let keys_demo = [
    `C 'j', (fun _ ->
        Upsycle_router.join_groups
          ~prepend:prefix'
          ~ttl:(Upsycle_router.mk_random_ttl `Short)
          ~exp:(Upsycle_router.mk_random_expiry ())
          cnf state_r msg_router_connection cnf.multicast_groups
        >>= begin function
        | `No_groups -> prf "no groups in config"
        | `Ok n -> prf
          "successfully joined %d %s: now use 'm' to send a multicast message to the topic(s)"
          n (Util_pure.english_plural "group" n) end
        |> ULwt.decorate_rejection "Unable to join groups");
    `C 'l', (fun _ ->
        Upsycle_router.leave_all_groups
          ~prepend:prefix'
          ~ttl:(Upsycle_router.mk_random_ttl `Short)
          ~exp:(Upsycle_router.mk_random_expiry ())
          cnf state_r msg_router_connection
        >>= begin function
        | `No_groups -> prf "not subscribed to any groups"
        | `Ok n -> prf "successfully left %d %s"
          n (Util_pure.english_plural "group" n) end
        |> ULwt.decorate_rejection "Unable to leave groups");
    `C 'm', (fun _ ->
        Upsycle_router.publish_to_subscribed_groups
          ~prepend:prefix'
          ~ttl:(Upsycle_router.mk_random_ttl `Medium)
          ~exp:(Upsycle_router.mk_random_expiry ())
          cnf state_r msg_router_connection
        >>= begin function
        | `No_groups -> prf "not a member of any multicast groups (try 'j' first)"
        | `Ok n -> prf "successfully published to %d %s"
          n (Util_pure.english_plural "topic" n)
        end
        |> ULwt.decorate_rejection "Unable to publish to subscribed group(s)");
    (* --- send a message with a trivial body to all known services *)
    `C 'p', (fun _ ->
        let body' = "mmmmm" ^ string_of_float (Random.float 1e10) in
        let known_services' = cnf.known_services in
        begin match List.length known_services' with
        | 0 -> Lwt.return `No_peers
        | num_peers' ->
          let f peerkey' = send_message_to_peer
            ~prepend:prefix'
            ~ttl:(Upsycle_router.mk_random_ttl `Medium)
            ~exp:(Upsycle_router.mk_random_expiry ())
            cnf msg_router_connection peerkey' body' in
          let* () = Lwt_list.iter_p f known_services' in
          Lwt.return (`Ok num_peers')
        end >>= begin function
          | `No_peers -> prf "no known peers (add some to the config first)"
          | `Ok n -> prf "successfully sent messages to %d peers" n
        end
        |> ULwt.decorate_rejection "Unable to speak to peer");
    `C 'P', (fun _ ->
        Upsycle_router.pull_seen
        ~prepend:prefix'
        ~ttl:(Upsycle_router.mk_random_ttl `Medium)
        ~exp:(Upsycle_router.mk_random_expiry ())
        cnf state_r msg_router_connection
        >>= begin function
          | `No_seen -> prf "no seen messages (join some groups and wait to receive some first)"
          | `Ok n -> prf "successfully sent pull request for %d messages" n end
        |> ULwt.decorate_rejection "Unable to pull seen messages");
    `C 'T', (fun _ ->
      let+ () = prf "clearing multicast table" in
      Upsycle_router.service_clear_multicast_table state_r);
    `Default, fun c -> default' c;
  ] in
  let key_default = `Default, fun c -> default' c in
  ULwt.start_key_thread @@ [ key_default ] @ match keyboard_mode with
    | `Normal -> keys_normal
    | `Demo ->   keys_normal @ keys_demo

(* --- @todo repeated from bin/message-router; we may wish to make something
   like bin/common.ml *)
let key_thread keyboard_mode cnf state_r conn =
  let f mode' =
    let () = ULwt.single_key_mode () in
    key_thread' mode' cnf state_r conn in
  match keyboard_mode with
  | `None -> ULwt.zombie ()
  | `Normal -> f `Normal
  | `Demo -> f `Demo

let main (cnf: Upsycle_router.Service_config.t) keyboard_mode =
  let threads = [ key_thread keyboard_mode ] in
  Upsycle_router.start_service ~threads cnf

let () =
  let _ = ULwt.at_exit_plain (fun _ -> Fmt.pr "* bye.@.") in
  let () = ULwt.enable_colors () in
  let f config' bin_config' = Lwt_main.run (main config' bin_config') in
  Mock_service_config.parse f
