open Cmdliner

open! Basic

type keyboard_mode = [ `None | `Normal | `Demo ]
let keyboard_mode_of_yaml = function
  | `String "none" -> Ok `None
  | `String "normal" -> Ok `Normal
  | `String "demo" -> Ok `Demo
  | _ -> Error (`Msg "keyboard_mode_of_yaml")
let keyboard_mode_to_yaml = Util_yaml.to_yaml_noop

(** See comments in bin/message_router_config.ml *)
type config' = {
  service_name: string [@key "service-name"];
  message_router_pubkey: string [@key "message-router-pubkey"];
  key_file: string [@key "key-file"];
  cert_file: string [@key "cert-file"];
  message_router_port: Upsycle_router.port [@key "message-router-port"];
  message_router_hostname: string [@key "message-router-hostname"];
  message_router_ip: Upsycle_router.ip [@key "message-router-ip"];
  known_services: string list [@key "known-services"];
  multicast_groups: Upsycle_router.multicast_group list [@key "multicast-groups"];
  msg_id_hash_function: Upsycle_router.msg_id_hash_function [@key "message-id-hashing-function"];
  openssl_cmd: Upsycle_router.command [@key "openssl-cmd"];
  timeout_join_leave_ack: float [@key "timeout-join-leave-ack"];
  seen_size: int [@key "seen-size"];
  keyboard_mode: keyboard_mode [@key "keyboard-mode"];
  print_messages_on_console: Upsycle_router.print_messages_on_console [@key "print-messages-on-console"];
  key_printing_short: bool [@key "key-printing-short"]
}
[@@deriving yaml]

let config_of_yaml conf_dir yaml =
  let open Rresult.R.Infix in
  config'_of_yaml yaml >>= fun config' ->
    let config = Upsycle_router.Service_config.mk ~base_dir:conf_dir
      config'.openssl_cmd
      config'.service_name
      config'.key_file
      config'.cert_file
      config'.message_router_pubkey
      config'.message_router_port
      config'.message_router_hostname
      config'.message_router_ip
      config'.known_services
      config'.msg_id_hash_function
      config'.multicast_groups
      begin match config'.timeout_join_leave_ack with
      | 0. -> None
      | x -> Some x end
      config'.seen_size
      config'.print_messages_on_console
      config'.key_printing_short in
    let bin_config = config'.keyboard_mode in
    Ok (config, bin_config)

let arg_conf =
  let doc = "The path to the YAML config file." in
  Arg.(required @@ opt (some file) None @@ info ["c"; "config"] ~docv:"config-yaml-path" ~doc)

let cmd f =
  let module Fn = Filename in
  let bin = Sys.argv.(0) |> Fn.basename |> Fn.remove_extension in
  let doc = "Run mock UPSYCLE service." in
  Term.(const f $ arg_conf),
  Term.info bin ~doc ~exits:Term.default_exits

let read_file fn = BatFile.with_file_in fn BatIO.read_all |> String.trim

let parse cb =
  let f conf_path =
    let conf_dir = Filename.dirname conf_path in
    let config, bin_config = Util_yaml_io.parse_file (config_of_yaml conf_dir) conf_path
    in cb config bin_config
  in Term.(exit @@ eval (cmd f))
