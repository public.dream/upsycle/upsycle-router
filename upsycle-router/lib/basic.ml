exception Internal_error of string

(* Intended as a synonym for @@ which is a bit less of an eyesore, but more
   synonyms also equals more confusion. Would have preferred --, which is also
   easier to type, but OCaml's operator associativity rules forbid this
   unfortunately. *)

let (^-) = (@@)

include Util_lwt.Letsfix

let id x = x
let const = Fun.const
let ok, error = Result.(ok, error)
let some, none = Option.(some, none)
let error_msg x = error @@ `Msg x

let lwt_ok = Lwt.return % ok
let lwt_error = Lwt.return % error

let internal_error s = raise @@ Internal_error s

let info, info', warn, warn' =
  Util_lwt.(info, info', warn, warn')
