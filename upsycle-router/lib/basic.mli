exception Internal_error of string

val (^-) : ('a -> 'b) -> 'a -> 'b

include module type of Util_lwt.Letsfix

val id : 'a -> 'a
val const : 'a -> ('b -> 'a)
val ok : 'a -> ('a, 'b) result
val error : 'b -> ('a, 'b) result
val error_msg : 'a -> ('b, [> `Msg of 'a ]) result

val lwt_ok : 'a -> ('a, 'b) result Lwt.t
val lwt_error : 'b -> ('a, 'b) result Lwt.t

val some : 'a -> 'a option
val none : 'a option

val internal_error : string -> 'a

val info : ?flush:bool -> ('a, Format.formatter, unit, unit Lwt.t) format4 -> 'a
val info' : ('a, Format.formatter, unit, unit Lwt.t) format4 -> 'a
val warn : ?flush:bool -> ('a, Format.formatter, unit, unit Lwt.t) format4 -> 'a
val warn' : ('a, Format.formatter, unit, unit Lwt.t) format4 -> 'a
