(** Contains common types and values which are private to our package. *)

(* Useful throughout the codebase for uni/multicast polymorphism for messages & headers. *)
type xcast = [ `Multi | `Uni ]

let parse_conduit_stream message_decode conduit_connection onmsg onbye onerr onparse_err =
  let module SD = Seaboar.Decode in
  let parser =
    let open Seaboar.Decode.Infix in
    let msg x = `Msg x in let bye _ = `Bye in
    SD.choice ~failure_msg:"unable to parse data on stream" [
      msg <$> message_decode;
      bye <$> SD.byte_string' "bye";
    ] in
  let onok' = function
  | `Msg msg -> onmsg msg
  | `Bye -> onbye () in
  let onerr' err = onerr err in
  let onparse_err' pp_err err =
    let err' = Util_io.fmt_err "%a" pp_err err in
    onparse_err err' in
  let ic = Util_conduit.Connection.Acc.ic conduit_connection in
  let parse' = Util_encode_decode_io.parse_stream parser ic (onok', onerr') in
  try%lwt parse' with
  | Failure e -> onparse_err' Fmt.string e
  | e -> onparse_err' Fmt.exn e
