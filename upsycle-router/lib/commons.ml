(** All modules can depend on Commons, but it must not depend on any other
    module.

    This entire module is re-exported to the outside using an include from
    Upsycle_router.

    In commons.mli we hide as much of the implementations as possible, since
    that's how they're presented to the outside. As a consequence of this we
    can't access the constructors here directly from our other modules and need
    to use functions for constructing/deconstructing. *)

module SD = Seaboar.Decode
module SE = Seaboar.Encode

open! Basic

module UCrypto = Util_crypto
module UCrypto_io = Util_crypto_io
module UPure = Util_pure
module UResult = Util_pure.Result
module UYaml = Util_yaml

(** Convenience types *)

type result_msg = Rresult.R.msg
type 'a rresult = ('a, result_msg) result
type 'a to_yaml = 'a -> Yaml.value
type 'a of_yaml = Yaml.value -> 'a rresult
type 'a sencode = 'a -> Seaboar.Encode.value
type 'a sdecode = 'a Seaboar.Decode.t

(** Basic types *)

type privkey_25519 = Mirage_crypto_ec.Ed25519.priv
type privkey = Privkey of privkey_25519
type pubkey_25519 = Mirage_crypto_ec.Ed25519.pub
type pubkey = Pubkey of pubkey_25519 (* size = 32 *)
type ip = Ip of Ipaddr.t
type port = int
type ip_port = Ip_port of { ip: ip; port: port; }
type multicast_group_pubkey = pubkey
type multicast_group_privkey = privkey
type multicast_group_rw = multicast_group_privkey * multicast_group_pubkey
type multicast_group_ro = multicast_group_pubkey
type multicast_group = [ `Rw of multicast_group_rw | `Ro of multicast_group_ro ]
type msg_id_hash_function = [ `Blake2b_256 | `Blake2b_32 | `Blake2s_128 | `Blake2s_32 ]
type multicast_subscriber = pubkey
type multicast_table = (multicast_group_pubkey, multicast_subscriber BatSet.t) BatMap.t

type multicast_requestor = [ `Src | `Peerkey ]
type print_messages_on_console = [ `Short | `Long | `None ]
type host_port = string * port
type remote_router = {
  pubkey: pubkey;
  ip: ip;
  port: port;
  routername: string;
}
type command = Command of string list

let to_yaml_noop = UYaml.to_yaml_noop
let error_msg x = Error (`Msg x)
let pubkey_of_multicast_group = function
  | `Rw (_, pub) -> pub
  | `Ro pub -> pub
let string_list_of_command (Command sl) = sl
let pp_comma_list pp_el =
  let sep ppf () = Fmt.pf ppf ", " in
  Fmt.list ~sep pp_el
let fmt_error_msg ppf (`Msg s) = Fmt.pf ppf "%s" s

module T = struct
  let ip_port ip port = Ip_port { ip; port }
  let ip i = Ip i
  let privkey i = Privkey i
  let pubkey i = Pubkey i
  (** From the 'ipaddr' package *)
  let ipaddr46_4 ip = Ipaddr.V4 ip
  let ipaddr46_6 ip = Ipaddr.V6 ip
end

module Pp = struct
  let ip_pkg = Ipaddr.pp
  let ip ppf (Ip i) = ip_pkg ppf i
  let port ppf = Fmt.int ppf
  let pubkey ppf (Pubkey p) = Fmt.pf ppf "%a" UCrypto_io.pp_pubkey_25519 p
  let privkey ppf (Privkey p) = Fmt.pf ppf "%a" UCrypto_io.pp_privkey_25519 p
  let privkey_none ppf () = Fmt.pf ppf "%a" UCrypto_io.pp_privkey_none ()

  let multicast_group_rw ppf (priv, pub) = Fmt.pf ppf "%a + %a" privkey priv pubkey pub
  let multicast_group ppf = function
    | `Rw (priv, pub) -> multicast_group_rw ppf (priv, pub)
    | `Ro pub -> Fmt.pf ppf "%a + %a" privkey_none () pubkey pub
  let multicast_group_pubkey = pubkey
  let multicast_group_ro = pubkey
  let multicast_subscriber = pubkey
end

module Mk = struct
  let ip i = Ip i
  let port_exn = function
    | i when i < 0 || i > 65535 -> failwith "bad port number"
    | i -> i
  let port = function
    | i when i < 0 || i > 65535 -> error_msg "bad port number"
    | i -> Ok i
  let portf = int_of_float %> port
  let ports = int_of_string_opt %> function
    | None -> Error (`Msg "ports: not an int")
    | Some x -> port x
  let ip_port = T.ip_port
  let ip_port' ip port = Ip_port { ip; port }
  let ip_port_raw ip' port = Ip_port { ip=Ip ip'; port }
  let ip_port_raw' ip' port = Ip_port { ip=Ip ip'; port }
  let privkey = T.privkey
  let pubkey = T.pubkey
  let multicast_group_pubkey = id
  let multicast_group_ro = id
  let multicast_group_rw = id
  let multicast_subscriber = id
  let remote_router pubkey ip port routername = {
    pubkey; ip; port; routername;
  }
end

module Acc = struct
  let ip (Ip i) = i
  let port = id
  let ip_port (Ip_port {ip; port}) = ip, port
  let ip_port' (Ip_port {ip; port}) = ip, port
  let pubkey (Pubkey p) = p
  let privkey (Privkey p) = p
  let multicast_group = id
  let multicast_group_pubkey = id
  let multicast_group_privkey = id
  let multicast_group_ro = id
  let multicast_group_ro' = id
  let multicast_group_rw = id
  let multicast_group_rw' = id
  let multicast_subscriber = id
  let control_interface = id
end

module Crypto = struct
  module SD = Seaboar.Decode
  module SE = Seaboar.Encode

  open! Basic

  let pubkey_to_cstruct = UCrypto.pubkey_25519_to_cstruct % Acc.pubkey

  let pubkey_of_string s =
    let open UResult.Infix in
    UCrypto.pubkey_25519_of_string s >>| Mk.pubkey

  let pubkey_of_string_exn =
    Rresult.R.get_ok % pubkey_of_string

  let pubkey_of_base64_string s =
    let open UResult.Infix in
    Base64.decode s >>= pubkey_of_string

  let pubkey_of_base64_string_exn =
    Rresult.R.get_ok % pubkey_of_base64_string

  let privkey_of_string s =
    let open UResult.Infix in
    UCrypto.privkey_25519_of_string s >>| Mk.privkey

  let privkey_of_string_exn =
    Rresult.R.get_ok % privkey_of_string

  let privkey_of_base64_string s =
    let open UResult.Infix in
    Base64.decode s >>= privkey_of_string

  let privkey_of_base64_string_exn =
    Rresult.R.get_ok % privkey_of_base64_string

  let pubkey_of_yaml = function
  | `String s -> pubkey_of_base64_string s
  | _ -> Error (`Msg "pubkey_of_yaml")

  let pubkey_to_yaml = Acc.pubkey
    %> UCrypto.pubkey_25519_to_string
    %> fun t -> `String t

  let sign = UCrypto.sign % Acc.privkey
  let verify = UCrypto.verify % Acc.pubkey

  (* let to_key_exn f desc keystr = match f keystr with *)
  (* | Ok k -> k *)
  (* | Error (`Msg e) -> failwith @@ Fmt.str "invalid %s %s (%s)" desc keystr e *)
  (* let to_pubkey_exn = to_key_exn pubkey_of_string "pubkey" *)
  (* let to_privkey_exn = to_key_exn privkey_of_string "privkey" *)

  let pubkey_of_certfile ?(quiet=false) openssl_cmd fp =
    let open UResult.Letsfix in
    let openssl_cmd' = string_list_of_command openssl_cmd in
    pubkey_of_string =<< UCrypto_io.pubkey_of_certfile ~print_stderr:(not quiet) ~complain:(not quiet) openssl_cmd' fp
  let pubkey_of_certfile_exn ?quiet openssl_cmd fp =
    match pubkey_of_certfile ?quiet openssl_cmd fp with
    | Ok key -> key
    | Error (`Msg m) -> failwith m

  let key_of_keyfile ?print_stderr ?complain mk_key the_type openssl_cmd fp =
    let open UResult.Letsfix in
    let openssl_cmd' = string_list_of_command openssl_cmd in
    mk_key =<< UCrypto_io.key_of_keyfile ?print_stderr ?complain the_type openssl_cmd' fp
  let key_of_keyfile_exn ?print_stderr ?complain mk_key_exn the_type openssl_cmd fp =
    match key_of_keyfile ?print_stderr ?complain mk_key_exn the_type openssl_cmd fp with
    | Ok key -> key
    | Error (`Msg m) -> failwith m

  let pubkey_of_keyfile ?(quiet=false) = key_of_keyfile ~print_stderr:(not quiet) ~complain:(not quiet) pubkey_of_string `Pub
  let privkey_of_keyfile ?(quiet=false) = key_of_keyfile ~print_stderr:(not quiet) ~complain:(not quiet) privkey_of_string `Priv
  let pubkey_of_keyfile_exn ?(quiet=false) = key_of_keyfile_exn ~print_stderr:(not quiet) ~complain:(not quiet) pubkey_of_string `Pub
  let privkey_of_keyfile_exn ?(quiet=false) = key_of_keyfile_exn ~print_stderr:(not quiet) ~complain:(not quiet) privkey_of_string `Priv

  let pubkey_of_privkey = Mk.pubkey % Mirage_crypto_ec.Ed25519.pub_of_priv % Acc.privkey

  let pubkey_of_keystring_or_file ?base_dir openssl_cmd s =
    let s' = match base_dir with
    | None -> s
    | Some prefix -> UPure.mk_absolute_path prefix s in
    UResult.list_find_f_ok_result [
      begin fun () -> pubkey_of_base64_string s end;
      begin fun () -> pubkey_of_keyfile ~quiet:true openssl_cmd s' end;
      begin fun () -> pubkey_of_certfile ~quiet:true openssl_cmd s' end;
    ]
  let pubkey_of_keystring_or_file_exn ?base_dir openssl_cmd s =
    match pubkey_of_keystring_or_file ?base_dir openssl_cmd s with
    | Error es -> failwith @@ Util_io.fmt_out "pubkey_of_keystring_or_file_exn: unable to get pubkey: %a" (pp_comma_list fmt_error_msg) es
    | Ok key -> key

  let fingerprint_ed25519_sha256 = UCrypto.fingerprint_ed25519_sha256 % Acc.pubkey

  (** Be sure to call `Util_crypto_io.init` before using this. *)
  let generate_key_pair ?g () =
    let priv, pub = UCrypto_io.generate_key_pair ?g () in
    Mk.privkey priv, Mk.pubkey pub

  module Mk = struct
    let pubkey = pubkey_of_string
    let pubkey_exn = pubkey_of_string_exn
    let pubkey_base64 = pubkey_of_base64_string
    let pubkey_base64_exn = pubkey_of_base64_string_exn
    let privkey = privkey_of_string
    let privkey_exn = privkey_of_string_exn
    let privkey_base64 = privkey_of_base64_string
    let privkey_base64_exn = privkey_of_base64_string_exn
  end
end

module Encode = struct
  let ipaddr_pkg = function
    | Ipaddr.V4 _ as x -> SE.variant "V4" [SE.string (Ipaddr.to_string x)]
    | Ipaddr.V6 _ as x -> SE.variant "V6" [SE.string (Ipaddr.to_string x)]
  let ipaddr = function
    | Ip i -> SE.variant "Ipaddr" [ipaddr_pkg i]
  let ip_port = function
    | Ip_port { ip; port; } -> SE.variant "Ip_port" [ipaddr ip; SE.int port]
  let pubkey = SE.bytes % UCrypto.pubkey_25519_to_string % Acc.pubkey
  let multicast_group_pubkey = pubkey
  let multicast_subscriber = pubkey
end

module Decode = struct
  open SD.Infix
  let ipaddr_pkg =
    let f ip_of_string = SD.utf8_string >>= (ip_of_string %> function
      | Ok ip -> SD.return ip
      | Error (`Msg m) -> SD.fail m) in
    let ipv4 = f Ipaddr.V4.of_string in
    let ipv6 = f Ipaddr.V6.of_string in

    SD.variant_1 "V4" T.ipaddr46_4 ipv4 <|>
    SD.variant_1 "V6" T.ipaddr46_6 ipv6
  let ip = SD.variant_1 "Ipaddr" T.ip ipaddr_pkg
  let pubkey = Crypto.Mk.pubkey <$> SD.byte_string >>= function
    | Ok x -> SD.return x
    | Error (`Msg x) -> SD.fail (Fmt.str "pubkey_parser (%s)" x)
  let multicast_group_pubkey = pubkey
  let multicast_subscriber = pubkey
end

module Yaml = struct
  type string_or_string_list = [ `S of string | `SL of string list ]
  let string_of_file_of_yaml = function
  | `String fn -> Ok (Util_io.read_file_trim fn)
  | _ -> Error (`Msg "string_of_file_of_yaml")

  let pubkey_of_file_of_yaml fn =
    let open Rresult.R.Infix in
    string_of_file_of_yaml fn >>= Crypto.Mk.pubkey_base64

  let host_port_of_yaml =
    let open Rresult.R.Infix in
    let err = Error (`Msg "host_port_of_yaml") in
    function
    | `String s -> begin match String.split_on_char ':' s with
      | [host; port] -> Mk.ports port >>| fun port' -> host, port'
      | _ -> err end
    | _ -> err
  let host_port_to_yaml = to_yaml_noop

  let ip_of_yaml =
    let open Rresult.R.Infix in
    function
    | `String s -> Ipaddr.of_string s >>| T.ip
    | _ -> error_msg "ip_of_yaml"
  let ip_to_yaml = to_yaml_noop
  let port_of_yaml = function
    | `Float f -> Mk.port (int_of_float f)
      |> UResult.rdecorate_error (`F (Fun.const "port_of_yaml"))
    | _ -> error_msg "port_of_yaml"
  let port_to_yaml p = `Float (float_of_int p)
  let ip_port_of_yaml = function
    | `O ["ip", ip'; "port", port'] -> UResult.lift2
        Mk.ip_port
        (ip_of_yaml ip')
        (port_of_yaml port')
    | _ -> error_msg "ip_port_of_yaml"
  let ip_port_to_yaml = to_yaml_noop
  let print_messages_on_console_of_yaml = function
    | `String "short" -> Ok `Short
    | `String "long" -> Ok `Long
    | `String "none" -> Ok `None
    | _ -> Error (`Msg "print_messages_on_console")
  let print_messages_on_console_to_yaml = to_yaml_noop

  let multicast_requestor_of_yaml = function
    | `String "src" -> Ok `Src
    | `String "peerkey" -> Ok `Peerkey
    | _ -> Error (`Msg "multicast_requestor")
  let multicast_requestor_to_yaml = to_yaml_noop

  let msg_id_hash_function_of_yaml = function
    | `String "blake2b_32" -> Ok `Blake2b_32
    | `String "blake2b_256" -> Ok `Blake2b_256
    | `String "blake2s_32" -> Ok `Blake2s_32
    | `String "blake2s_128" -> Ok `Blake2s_128
    | _ -> Error (`Msg "msg_id_hash_function")
  let msg_id_hash_function_to_yaml = to_yaml_noop
  let multicast_group_to_yaml _ = `String "n/a"
  let multicast_group_of_yaml =
    let pub_of_priv = Crypto.pubkey_of_privkey in
    let to_pub = Crypto.Mk.pubkey_base64_exn in
    let to_priv = Crypto.Mk.privkey_base64_exn in
    let confirm priv pub = if pub <> pub_of_priv priv then
      failwith "multicast_group_of_yaml: invalid public/private keypair" in
    function
    | `O ["priv", `String priv; "pub", `String pub] ->
        let priv' = to_priv priv in let pub' = to_pub pub in
        let () = confirm priv' pub' in
        Ok (`Rw (priv', pub'))
    | `O ["priv", `String priv] ->
        let priv' = to_priv priv in
        Ok (`Rw (priv', pub_of_priv priv'))
    | `O ["pub", `String pub] -> Ok (`Ro (to_pub pub))
    |_ -> error_msg "multicast_group_of_yaml: invalid yaml"

  let string_or_string_list_of_yaml =
    let open UResult.Letsfix in
    let msg = "string_or_string_list_of_yaml" in
    let sl sl' = `SL sl' in function
    | `String s -> Ok (`S s)
    | `A ss -> sl <$> UYaml.strings_result ~msg ss
    | _ -> Error (`Msg msg)
  let string_or_string_list_to_yaml = to_yaml_noop

  let command_of_yaml yaml =
    let f = function `SL ss -> Command ss | `S s -> Command [s] in
    match string_or_string_list_of_yaml yaml with
    | Ok sls -> Ok (f sls)
    | Error _ -> Error (`Msg "command_of_yaml")
  let command_to_yaml = to_yaml_noop
end

let keys_common prefix =
  let prf fmt = Util_lwt.mk_prf prefix @@ fmt in
  function
  | 'q' ->
      let* () = Lwt_fmt.printf "@." in
      exit 0
  | '\n' -> Lwt_fmt.printf "@."
  | c -> prf "Unrecognised key: %c" c

let mk_random_ttl = function
  (* [3, 10] *)
  | `Short -> 3 + Random.int 8
  (* [10, 30] *)
  | `Medium -> 10 + Random.int 21

(* --- in minutes since 2020-01-01 00:00 UTC *)
let mk_random_expiry () =
  (* [1, 3] *)
  let rand_mins' = 1 + Random.int 3 in
  Time.Time_upsycle_unix.now () + 60 * rand_mins'

let mk_random_revision () =
  Random.int 20

let ip_of_string_result = Result.map T.ip % Ipaddr.of_string
