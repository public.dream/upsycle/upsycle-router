(** Contains shared types and values which are used throughout the application. *)

(**/**)
module Yaml_real := Yaml
(**/**)

(** {3 Convenience types.} *)

type result_msg = Rresult.R.msg
type 'a rresult = ('a, result_msg) result
type 'a to_yaml = 'a -> Yaml.value
type 'a of_yaml = Yaml.value -> 'a rresult
type 'a sencode = 'a -> Seaboar.Encode.value
type 'a sdecode = 'a Seaboar.Decode.t

(** {3 Basic types.} *)

(** These are the base types we use for private and public keys; no need to try
    and hide the implementation. *)
type privkey_25519 = Mirage_crypto_ec.Ed25519.priv
type pubkey_25519 = Mirage_crypto_ec.Ed25519.pub

type privkey
type pubkey
type ip
type port
type ip_port
type host_port
type command

(** {3 Types related to multicast messages.} *)

type multicast_group_pubkey
type multicast_group_privkey

(** A multicast group for which we have both a public and private key. *)
type multicast_group_rw

(** A multicast group for which we only have a public key. *)
type multicast_group_ro

type multicast_group = [ `Rw of multicast_group_rw | `Ro of multicast_group_ro ]

(** {3 Types used for configuration and choices. *} *)

(** Used for choosing the Blake2 hash function for calculating message IDs. *)
type msg_id_hash_function = [ `Blake2b_256 | `Blake2b_32 | `Blake2s_128 | `Blake2s_32 ]

(** When the message router receives a JOIN request whose [src] field doesn't
    match the actual public key of the peer, it needs to decide which one to
    actually add to the group. By default this is set to [`Src] in the
    configurations, but `Peerkey can be useful for testing or experimenting. *)
type multicast_requestor = [ `Peerkey | `Src ]

(** Configures whether or not to dump the contents of every message received by
    the message router to the console. *)
type print_messages_on_console = [ `Long | `None | `Short ]

(** {3 Types used for configuring the initial state of the message router.} *)
(* --- @todo abstract *)
type multicast_subscriber
type multicast_table = (multicast_group_pubkey, multicast_subscriber BatSet.t) BatMap.t
type remote_router = private {
  pubkey: pubkey;
  ip: ip;
  port: port;
  routername: string;
}

(** The functions in {!T} are just function versions of the constructors of
    various types, which provides a convenient way of constructing types in many
    situations. *)

module T : sig
  val ip_port : ip -> port -> ip_port
  val ip : Ipaddr.t -> ip
  val privkey : privkey_25519 -> privkey
  val pubkey : pubkey_25519 -> pubkey
  val ipaddr46_4 : 'a -> ('a, 'b) Ipaddr.v4v6
  val ipaddr46_6 : 'a -> ('b, 'a) Ipaddr.v4v6
end

(** {3 Pretty printers.} *)
module Pp : sig
  type 'a pp := Format.formatter -> 'a -> unit
  val ip_pkg : Ipaddr.t pp
  val ip : ip pp
  val port : port pp
  val pubkey : pubkey pp
  val privkey : privkey pp
  val privkey_none : unit pp
  val multicast_group : multicast_group pp
  val multicast_group_pubkey : multicast_group_pubkey pp
  val multicast_group_ro : multicast_group_ro pp
  val multicast_group_rw : multicast_group_rw pp
  val multicast_subscriber: multicast_subscriber pp
end

(** {3 Encodings for various types using Seaboar.} *)
module Encode : sig
  val ipaddr_pkg : (Ipaddr.V4.t, Ipaddr.V6.t) Ipaddr.v4v6 sencode
  val ipaddr : ip sencode
  val ip_port : ip_port sencode
  val pubkey : pubkey sencode
  val multicast_group_pubkey : multicast_group_pubkey sencode
  val multicast_subscriber : multicast_subscriber sencode
end

(** {3 Parsers for various types using Seaboar.} *)
module Decode : sig
  val ipaddr_pkg : (Ipaddr.V4.t, Ipaddr.V6.t) Ipaddr.v4v6 sdecode
  val ip : ip sdecode
  val pubkey : pubkey sdecode
  val multicast_group_pubkey : multicast_group_pubkey sdecode
  val multicast_subscriber : multicast_subscriber sdecode
end

(** {3 Common functions related to cryptography.} *)
module Crypto : sig
  module MC25519 := Mirage_crypto_ec.Ed25519
  module SD := Seaboar.Decode
  module SE := Seaboar.Encode
  val pubkey_to_cstruct : pubkey -> Cstruct.t
  val pubkey_of_string : string -> pubkey rresult
  val pubkey_of_yaml : Yaml.value -> pubkey Yaml.res
  val pubkey_to_yaml : pubkey -> Yaml.value
  val sign : privkey -> string -> string
  val verify : pubkey -> string -> string -> bool
  (** Be sure to call `Util_crypto_io.init` before using this. *)
  val generate_key_pair : ?g:Mirage_crypto_rng.g -> unit -> privkey * pubkey
  val pubkey_of_certfile : ?quiet:bool -> command -> string -> pubkey rresult
  val pubkey_of_certfile_exn : ?quiet:bool -> command -> string -> pubkey

  val pubkey_of_keyfile : ?quiet:bool -> command -> string -> pubkey rresult
  val pubkey_of_keyfile_exn : ?quiet:bool -> command -> string -> pubkey
  val privkey_of_keyfile : ?quiet:bool -> command -> string -> privkey rresult
  val privkey_of_keyfile_exn : ?quiet:bool -> command -> string -> privkey
  (** Derive the corresponding public key for a private key. *)
  val pubkey_of_privkey : privkey -> pubkey
  (* val pubkey_of_yaml' : command -> Yaml.value -> pubkey Yaml.res *)
  val pubkey_of_keystring_or_file_exn : ?base_dir:string -> command -> string -> pubkey
  val fingerprint_ed25519_sha256 : pubkey -> Mirage_crypto.Hash.hash * Cstruct.t
  module Mk : sig
    val pubkey : string -> pubkey rresult
    val pubkey_exn : string -> pubkey
    val pubkey_base64 : string -> pubkey rresult
    val pubkey_base64_exn : string -> pubkey
    val privkey : string -> privkey rresult
    val privkey_exn : string -> privkey
    val privkey_base64 : string -> privkey rresult
    val privkey_base64_exn : string -> privkey
  end
end

(** {3 Functions for constructing our types, also opaque ones, from Yaml files.} *)
module Yaml : sig
  type string_or_string_list
  type print_message_on_console := [ `Long | `None | `Short ]
  val host_port_to_yaml : host_port to_yaml
  val host_port_of_yaml : host_port of_yaml
  val ip_of_yaml : ip of_yaml
  val ip_to_yaml : ip to_yaml
  val port_of_yaml : port of_yaml
  val port_to_yaml : port to_yaml
  val ip_port_of_yaml : ip_port of_yaml
  val ip_port_to_yaml : ip_port to_yaml
  val print_messages_on_console_of_yaml : print_message_on_console of_yaml
  val print_messages_on_console_to_yaml : 'a to_yaml
  val pubkey_of_file_of_yaml : pubkey of_yaml
  val multicast_requestor_of_yaml : multicast_requestor of_yaml
  val multicast_requestor_to_yaml : multicast_requestor to_yaml
  val msg_id_hash_function_of_yaml : msg_id_hash_function of_yaml
  val msg_id_hash_function_to_yaml : msg_id_hash_function to_yaml
  val multicast_group_to_yaml : 'a -> [> `String of string ]
  val multicast_group_of_yaml : multicast_group of_yaml
  val string_of_file_of_yaml : string of_yaml
  val command_of_yaml : command of_yaml
  val command_to_yaml : command to_yaml
  val string_or_string_list_of_yaml : string_or_string_list of_yaml
  val string_or_string_list_to_yaml : string_or_string_list to_yaml
end

(** {3 Helper functions for building data structures.} *)
module Mk : sig
  val ip : Ipaddr.t -> ip
  val port_exn : int -> port
  val port : int -> port rresult
  val portf : float -> port rresult
  val ports : string -> port rresult
  val ip_port : ip -> port -> ip_port
  val ip_port' : ip -> int -> ip_port
  val ip_port_raw : Ipaddr.t -> int -> ip_port
  val ip_port_raw' : Ipaddr.t -> port -> ip_port
  val privkey : privkey_25519 -> privkey
  val pubkey : pubkey_25519 -> pubkey
  val multicast_group_pubkey : pubkey -> multicast_group_pubkey
  val multicast_group_ro : pubkey -> multicast_group_ro
  val multicast_group_rw : privkey * pubkey -> multicast_group_rw
  val multicast_subscriber : pubkey -> multicast_subscriber
  val remote_router : pubkey -> ip -> port -> string -> remote_router
end

(** {3 Helper functions for deconstructing data structures.} *)
module Acc : sig
  val ip : ip -> Ipaddr.t
  val port : port -> int
  val ip_port : ip_port -> ip * port
  val ip_port' : ip_port -> ip * int
  val privkey : privkey -> privkey_25519
  val pubkey : pubkey -> pubkey_25519
  val multicast_group : multicast_group -> [ `Rw of privkey * pubkey | `Ro of pubkey ]
  val multicast_group_privkey : multicast_group_privkey -> privkey
  val multicast_group_pubkey : multicast_group_pubkey -> pubkey
  val multicast_group_ro : multicast_group_ro -> multicast_group_pubkey
  val multicast_group_ro' : multicast_group_ro -> pubkey
  val multicast_group_rw : multicast_group_rw -> multicast_group_privkey * multicast_group_pubkey
  val multicast_group_rw' : multicast_group_rw -> privkey * pubkey
  val multicast_subscriber : multicast_subscriber -> pubkey
  val control_interface : host_port option -> (string * port) option
end

(** Provides handlers for the keys 'q', '\n', and an unknown key press, to be
    used in the key thread. *)
val keys_common : string -> char -> unit Lwt.t

(** {3 Miscellaneous helpers. } *)

(** Returns a random integer for use in the `ttl` field.

    [mk_random_ttl `Short] produces an integer in the range \[3, 10\].

    [mk_random_ttl `Medium] produces an integer in the range \[10, 30\]. *)
val mk_random_ttl : [< `Medium | `Short ] -> int

(** Returns a random integer representing minutes elapsed since 2020-01-01 00:00
    UTC, for use in the `expiry` field. *)
val mk_random_expiry : unit -> int

(** Returns a random integer in the range \[0, 19\], for use in the `revision`
    field. *)
val mk_random_revision : unit -> int

(** Converts a string to our internal {!ip} type. *)
val ip_of_string_result : string -> ip rresult

(** [ppx_deriving_yaml] requires both an [x_to_yaml] and [x_of_yaml] function
  for a type [x] in order to be able produce derivations for types which include
  [x]. However the 'of_yaml' variant is never used in this library so there's no
  sense in writing it; this function can be provided instead to satisfy the
  requirements. *)
val to_yaml_noop : 'a Util_yaml.to_yaml

(** Convenience for generating [Error (`Msg s)] from a string [s]. *)
val error_msg : string -> string rresult

val pubkey_of_multicast_group : multicast_group -> pubkey
val string_list_of_command : command -> string list
val pp_comma_list : 'a Fmt.t -> 'a list Fmt.t
