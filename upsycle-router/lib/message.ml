(**/**)
module SD = Seaboar.Decode
module SE = Seaboar.Encode
module U32 = Stdint.Uint32

module Crypto = Commons.Crypto
module CSet = Util_cache.CSet
module UCrypto = Util_crypto
module UDecode = Util_encode_decode.Decode
module UEncode = Util_encode_decode.Encode
module Uio = Util_io
module UOption = Util_pure.Option
module UString = Util_pure.String
(**/**)

open! Basic

(**/**)
(* --- @enhancement: a list type which must contain at least 1 member *)
type 'a list1 = 'a list
let list1_enc e = SE.list e
let list1_dec d = SD.array_list d

let binary_string ppf s =
  if UString.is_binary ~take:1000 s
  then Fmt.pf ppf "[binary string]"
  else Fmt.pf ppf "%s" s
let time_string =
  let tz = "UTC" in (* syscallL: gmtime *)
  let epoch = 1577836800 in
  let f (tm: Unix.tm) =
    let time = Uio.fmt_out "%02d:%02d:%02d %s" tm.tm_hour tm.tm_min tm.tm_sec tz in
    Uio.fmt_out "%4d-%02d-%02d %a"
      (1900 + tm.tm_year) (1 + tm.tm_mon)
      tm.tm_mday Uio.underline time in
  f % Unix.gmtime % float_of_int % (+) epoch

let fmt_option none_str pp ppf o =
  let none ppf () = Fmt.pf ppf none_str in
  Fmt.pf ppf "%a" (Fmt.option ~none pp) o
(**/**)

(* Common types and combinators. Called 'Com' because we already have a module 'Common' *)
module Com = struct
  type pubkey = Commons.pubkey
  type privkey = Commons.privkey
  type hash = Hash of string (* size = 32 *)
  type result' = Ok | Nok
  type ttl = Ttl of U32.t
  type expiry = Expiry of U32.t
  type msg_id = Msg_id of hash
  type seen = Seen of msg_id CSet.t
  (* --- 64 bytes *)
  type signature = Signature of string

  let fold_result ok nok = function
    | Ok -> ok ()
    | Nok -> nok ()

  let add_seen s (Seen sn) = Seen (CSet.add s sn)
  let num_seen (Seen sn) = CSet.length sn

  module T = struct
    let expiry x = Expiry x
    let hash x = Hash x
    let msg_id x = Msg_id x
    let seen x = Seen x
    let ttl x = Ttl x
    let ok = Ok
    let nok = Nok
    let signature s = Signature s
  end

  module Mk = struct
    let msg_id_of_string s = Msg_id (Hash s)
    let seen sz = Seen (CSet.empty sz)
  end

  module Acc = struct
    let signature (Signature s) = s
  end

  module Encode = struct
    let hash (Hash h) = SE.bytes h
    let msg_id (Msg_id h) = hash h
    let pubkey = Commons.Encode.pubkey
    let multicast_group_pubkey = pubkey % Commons.Acc.multicast_group_pubkey
    let seen (Seen s) = UEncode.cset msg_id s
    let ttl (Ttl t) = UEncode.u32 t
    let expiry (Expiry e) = UEncode.u32 e
    let result = SE.int % function
      | Ok -> 0
      | Nok -> 1
    let signature (Signature s) = SE.bytes s
  end

  module Decode = struct
    open SD.Infix
    let pubkey = Commons.Decode.pubkey
    let ttl = T.ttl % U32.of_int <$> SD.int
    let exp = T.expiry % U32.of_int <$> SD.int
    let src = pubkey
    let dst = pubkey
    let multicast_group_pubkey = Commons.Mk.multicast_group_pubkey <$> pubkey
    let hash = T.hash <$> SD.byte_string
    let msg_id = T.msg_id <$> hash
    let seen queue_length = T.seen <$> UDecode.cset msg_id queue_length
    let result =
      SD.choice [
        Ok <$ SD.int' 0;
        Nok <$ SD.int' 1;
      ]
    let signature = SD.lift T.signature SD.byte_string
  end

  module Pp = struct
    let pubkey = Commons.Pp.pubkey
    let multicast_group_pubkey = Commons.Pp.multicast_group_pubkey
    let multicast_group_ro = Commons.Pp.multicast_group_ro
    let ttl ppf (Ttl x) = Fmt.pf ppf "%d" (U32.to_int x)
    let expiry ppf (Expiry e) = Fmt.pf ppf "%d" (U32.to_int e)
    let expiry' ppf (Expiry e) = Fmt.pf ppf "%s" (time_string (U32.to_int e))
    let hash ppf (Hash h) =
      let () = Fmt.pf ppf "0x" in
      let f = Fmt.pf ppf "%02x" % Char.code in
      String.iter f h
    let msg_id ppf (Msg_id h) = Fmt.pf ppf "%a" hash h
    let seen' type' ppf (Seen s) = match type' with
    | `Short -> Fmt.pf ppf "seen: %a" CSet.pp_short s
    | `Long -> Fmt.pf ppf "seen: %a" (CSet.pp msg_id) s
    let seen = seen' `Long
    let signature ppf (Signature s) = Fmt.pf ppf "length: %d" (String.length s)
  end

  let fold_seen' f (Seen seen) = f (CSet.elems seen)
end

module Base = struct
  type body = Body of string
  type via = Via of Com.pubkey
  type sign = Sign of string (* size = 64 *)
  type unicast_header = Unicast_header of { src: Com.pubkey; dst: Com.pubkey; ttl: Com.ttl; exp: Com.expiry; }
  type multicast_header = Multicast_header of { grp: Commons.multicast_group_pubkey; ttl: Com.ttl; exp: Com.expiry; seen: Com.seen; }
  type t =
    | Unicast_message of { header: unicast_header; body: body; sign: sign; via: via option; }
    | Multicast_message of { header: multicast_header; body: body; sign: sign; via: via option; }

  let fold f g = function
    | Unicast_message u -> f u.header u.body u.sign u.via
    | Multicast_message m -> g m.header m.body m.sign m.via
  let fold' f g = function
    | Unicast_message _ as m -> f m
    | Multicast_message _ as m -> g m
  let fold_unwrap f g =
    let (<$>) = Option.map in
    let v (Via via') = via' in
    let f' header (Body body) (Sign sign) ovia  = f header body sign (v <$> ovia) in
    let g' header (Body body) (Sign sign) ovia  = g header body sign (v <$> ovia) in
    fold f' g'
  let fold_unwrap' f g =
    let (<$>) = Option.map in
    let v (Via via') = via' in
    let f' header (Body body) (Sign sign) ovia =
      let Unicast_header header' = header in
      f header'.src body sign (v <$> ovia) in
    let g' header (Body body) (Sign sign) ovia =
      let Multicast_header header' = header in
      g header'.grp body sign (v <$> ovia) in
    fold f' g'

  let fold_flip_multi msg g =
    let f _ _ _ _ = failwith "fold_flip_multi: not a multicast message" in
    fold f g msg
  let fold_flip_multi_unwrap msg g =
    let f _ _ _ _ = failwith "fold_flip_multi: not a multicast message" in
    fold_unwrap f g msg
  let fold_flip_multi_unwrap' msg g =
    let f _ _ = failwith "fold_flip_multi: not a multicast message" in
    fold_unwrap' f g msg

  let fold_header f g =
    let f' (Unicast_header h) _ _ _ = f h.src h.dst h.ttl h.exp in
    let g' (Multicast_header h) _ _ _ = g h.grp h.ttl h.exp h.seen in
    fold f' g'
  let fold_header' f g =
    let f' header _ _ _ = f header in
    let g' header _ _ _ = g header in
    fold f' g'

  module Acc = struct
    let body = let f _ b _ _ = b in fold f f
    let body' msg = let Body b = body msg in b
    let sign = let f _ _ s _ = s in fold f f
    let sign' msg = let Sign s = sign msg in s
    let group (Multicast_header h) = h.grp
    let ttl = let f _ _ t _ = t in let g _ t _ _ = t in fold_header f g
    let exp = let f _ _ _ e = e in let g _ _ e _ = e in fold_header f g
    let ttl' m = let Ttl t = ttl m in U32.to_int t
    let exp' m = let Expiry e = exp m in U32.to_int e
    let via = let f _ _ _ v = v in fold f f
    let via' = let f _ _ _ v = v in fold_unwrap f f
  end

  module T = struct
    let body x = Body x
    let sign x = Sign x
    let via x = Via x
    let unicast_header src dst ttl exp = Unicast_header { src; dst; ttl; exp; }
    let multicast_header grp ttl exp seen = Multicast_header { grp; ttl; exp; seen; }
  end

  module Pp = struct
    let option' pp ppf = function
      | None -> Fmt.pf ppf "none"
      | Some x -> Fmt.pf ppf "%a" pp x
    let via ppf (Via v) = Commons.Pp.pubkey ppf v
    let sign ppf (Sign s) =
      Fmt.pf ppf "(length %d)" (String.length s)
    let _sig_via ppf (Sign sig', ovia') =
      let () = Fmt.pf ppf "signature of length %d" (String.length sig') in
      Fmt.pf ppf "%a %a"
      Uio.yellow "via"
      (option' via) ovia'
    let header' type' ppf =
      let expiry ppf exp = Fmt.pf ppf "%a (%a)"
        Com.Pp.expiry exp
        Com.Pp.expiry' exp in
      match type' with
      | `Short -> begin function
        | `Uni_header (Unicast_header { src; dst; _ }) ->
            let () = Fmt.pf ppf "@[<h>" in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair cyan Com.Pp.pubkey) ("src", src) in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair yellow Com.Pp.pubkey) ("dst", dst) in
            Fmt.pf ppf "@]"
        | `Multi_header (Multicast_header { grp; seen; _ }) ->
            let () = Fmt.pf ppf "@[<h>" in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair yellow Com.Pp.multicast_group_pubkey) ("grp", grp) in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair bright_red (Com.Pp.seen' `Short)) ("seen", seen) in
            Fmt.pf ppf "@]"
      end
      | `Long -> begin function
        | `Uni_header (Unicast_header { src; dst; ttl; exp }) ->
            let () = Fmt.pf ppf "@[<v>" in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair cyan Com.Pp.pubkey) ("src", src) in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair yellow Com.Pp.pubkey) ("dst", dst) in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair green Com.Pp.ttl) ("ttl", ttl) in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair bright_red expiry) ("expiry", exp) in
            Fmt.pf ppf "@]"
        | `Multi_header (Multicast_header { grp; ttl; exp; seen }) ->
            let () = Fmt.pf ppf "@[<v>" in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair yellow Com.Pp.multicast_group_pubkey) ("grp", grp) in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair magenta Com.Pp.ttl) ("ttl", ttl) in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair green expiry) ("expiry", exp) in
            let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair bright_red (Com.Pp.seen' `Long)) ("seen", seen) in
            Fmt.pf ppf "@]"
      end
    let header = header' `Long
    let body ppf (Body b) =
      Fmt.pf ppf "(length %d)" (String.length b)
    let body' ppf (Body b) = Fmt.pf ppf "%a" binary_string b
    let message ppf msg =
      let f (s_fmt, s) header' body' sign' via' =
        Fmt.pf ppf "@[<v>~~~ %a ~~~@ %a@ %a@ %a@ %a@]"
        (Uio.fmt s_fmt) s
        (Uio.pp_section header) ("header", header')
        (Uio.pp_section body) ("body", body')
        (Uio.pp_section sign) ("signature", sign')
        (Uio.pp_section (fmt_option "none" via)) ("via", via')
      in
      let g header body sign via = f (Uio.yellow, "unicast") (`Uni_header header) body sign via in
      let h header body sign via = f (Uio.cyan, "multicast") (`Multi_header header) body sign via in
      fold g h msg
    let message_short ppf msg =
      let f (s_fmt, s) hdr' =
        Fmt.pf ppf "@[<h>%a@ %a@]"
        (Uio.fmt s_fmt) s
        (header' `Short) hdr'
      in
      let g header = f (Uio.yellow, "unicast") (`Uni_header header) in
      let h header = f (Uio.cyan, "multicast") (`Multi_header header) in
      fold_header' g h msg
  end

  module Decode = struct
    let (>>=), (<$>) = SD.((>>=), (<$>))
    let body = T.body <$> SD.byte_string
    let sign = T.sign <$> SD.byte_string
    let via = T.via <$> Com.Decode.pubkey

    let unicast_header =
      SD.map_values_4 T.unicast_header (
        ("src", Com.Decode.src),
        ("dst", Com.Decode.dst),
        ("ttl", Com.Decode.ttl),
        ("exp", Com.Decode.exp)
      )

    let multicast_header seen_length =
      SD.map_values_4 T.multicast_header (
        ("grp", Com.Decode.multicast_group_pubkey),
        ("ttl", Com.Decode.ttl),
        ("exp", Com.Decode.exp),
        ("sn", Com.Decode.seen seen_length)
      )
    let message seen_length =
      let f0 (_, header, body, sign)      = Unicast_message   { header; body; sign; via = None } in
      let f1 (_, header, body, sign)      = Multicast_message { header; body; sign; via = None } in
      let g0 (_, header, body, sign, via) = Unicast_message   { header; body; sign; via = Some via } in
      let g1 (_, header, body, sign, via) = Multicast_message { header; body; sign; via = Some via } in
      SD.choice ~failure_msg:"unable to parse base message" [
        SD.lift f0 (SD.array_tuple4 (SD.int' 0) unicast_header body sign);
        SD.lift f1 (SD.array_tuple4 (SD.int' 1) (multicast_header seen_length) body sign);
        SD.lift g0 (SD.array_tuple5 (SD.int' 0) unicast_header body sign via);
        SD.lift g1 (SD.array_tuple5 (SD.int' 1) (multicast_header seen_length) body sign via);
      ]
  end

  module Encode = struct
    let body = function Body b -> SE.bytes b
    let sign = function Sign s -> SE.bytes s
    let multicast_header = function
      | Multicast_header { grp; ttl; exp; seen; } -> SE.map' [
        "grp", Com.Encode.multicast_group_pubkey grp;
        "ttl", Com.Encode.ttl ttl;
        "exp", Com.Encode.expiry exp;
        "sn", Com.Encode.seen seen;
      ]
    let unicast_header = function
      | Unicast_header { src; dst; ttl; exp; } -> SE.map' [
        "src", Com.Encode.pubkey src;
        "dst", Com.Encode.pubkey dst;
        "ttl", Com.Encode.ttl ttl;
        "exp", Com.Encode.expiry exp;
      ]
    let via = function Via k -> Com.Encode.pubkey k

    let message =
      let module U = UEncode in
      let f n header_enc header body' sign' ovia = U.mk_list [
        [`Int n];
        [header_enc header];
        [body body'];
        [sign sign'];
        U.list_elem_opt via ovia;
      ] in function
      | Unicast_message { header; body; sign; via=ovia; } ->
        f 0 unicast_header header body sign ovia
      | Multicast_message { header; body; sign; via=ovia; } ->
        f 1 multicast_header header body sign ovia
    let encode = SE.encode message
  end

  let is_unicast =
    let f _ = true in let g _ = false in
    fold' f g

  let is_multicast =
    let f _ = false in let g _ = true in
    fold' f g

  let get_body = let f _ b _ _ = b in fold f f
  let get_signature = let f _ _ s _ = s in fold f f

  let sign_prepare header body_s =
    let type_s, header_s = match header with
    | `Uni h -> "UNICAST", Encode.unicast_header h
    | `Multi h -> "MULTICAST", Encode.multicast_header h in
    let f = BatString.join "" % List.map SE.encode_generic in
    f [ SE.string type_s; header_s; SE.string body_s ]

  let sign_message privkey header body_s =
    let s = sign_prepare header body_s in
    Crypto.sign privkey s

  let verify_signature pubkey msg =
    let header' =
      let u hdr = `Uni hdr in
      let m hdr = `Multi hdr in
      fold_header' u m msg in
    let Body body_s' = get_body msg in

    let Sign sign' = get_signature msg in
    let s = sign_prepare header' body_s' in
    if Crypto.verify pubkey sign' s
      then Lwt.return ()
      else Lwt.fail_with "bad signature"

  module Mk = struct
    let unicast ?(test_tamper=false) ~ttl ~exp ?via:via' (src_priv, src_pub) dst body_s =
      let via = Option.map T.via via' in
      let body = T.body body_s in
      let ttl' = Com.T.ttl (U32.of_int ttl) in
      let exp' = Com.T.expiry (U32.of_int exp) in
      let header = T.unicast_header src_pub dst ttl' exp' in
      let sign =
        let sign' = sign_message src_priv (`Uni header) body_s in
        T.sign (if test_tamper then sign' ^ "a" else sign') in
      Unicast_message { header; body; sign; via; }
    let multicast ?(test_tamper=false) ~ttl ~exp ?via:via' ~seen group_rw body_s =
      let via = Option.map T.via via' in
      let body = T.body body_s in
      let ttl' = Com.T.ttl (U32.of_int ttl) in
      let exp' = Com.T.expiry (U32.of_int exp) in
      (* @todo *)
      let x, y = Commons.Acc.multicast_group_rw group_rw in
      let xx = Commons.Acc.multicast_group_privkey x in
      let header = T.multicast_header y ttl' exp' seen in
      let sign =
        let sign' = sign_message xx (`Multi header) body_s in
        T.sign (if test_tamper then sign' ^ "a" else sign') in
      Multicast_message { header; body; sign; via; }
    let encoded_unicast ~ttl ~exp ?via (src_priv, src_pub) dst body =
      unicast ~ttl ~exp ?via (src_priv, src_pub) dst body
      |> Encode.encode
    let encoded_multicast ~ttl ~exp ~seen group_rw body =
      multicast ~ttl ~exp ~seen group_rw body
      |> Encode.encode
  end

  let pp = Pp.message
  let pp_short = Pp.message_short

  let get_uni_field s =
    let f = match s with
    | `Src -> fun src _ _ _ -> src
    | `Dst -> fun _ dst _ _ -> dst in
    let g _ = failwith "get_uni_field: not a unicast message" in
    fold_header f g

  let get_multi_field s =
    let f _ = failwith "get_multi_field: not a multicast message" in
    let g = match s with
    | `Grp -> fun grp _ _ _ -> grp in
    fold_header f g

  (** These raise an exception on multicast messages: caller must be sure it's a unicast message *)
  let get_uni_src = get_uni_field `Src
  let get_uni_dst = get_uni_field `Dst

  (** These raise an exception on unicast messages: caller must be sure it's a multicast message *)
  let get_multi_group = get_multi_field `Grp
  let get_multi_group' = Commons.Acc.multicast_group_pubkey % get_multi_group

  (** Returns `false` for a multicast message and therefore doesn't raise any exceptions. Works by checking the `dst` field. *)
  let is_for dst =
    let f m = get_uni_dst m = dst in
    let g _ = false in
    fold' f g

  let via_set src = let via' = Via src in function
    | Unicast_message m -> Unicast_message { m with via = Some via' }
    | Multicast_message m -> Multicast_message { m with via = Some via' }

let message_id_s hashf =
  let hash = match hashf with
  | `Blake2b_32 -> UCrypto.blake2b_32 ~size:32
  | `Blake2b_256 -> UCrypto.blake2b_256 ~size:32
  | `Blake2s_32 -> UCrypto.blake2s_32 ~size:32
  | `Blake2s_128 -> UCrypto.blake2s_128 ~size:32 in
  let encode_hdr = function
    | `Uni h -> Encode.unicast_header h
    | `Multi m -> Encode.multicast_header m in
  let mk = hash % BatString.join "" % List.map SE.encode_generic in
  let mk_s type' hdr bdy sign = mk [
    SE.string type'; encode_hdr hdr; Encode.body bdy; Encode.sign sign;
  ] in
  (* let uni hdr bdy sign _ = Com.Mk.msg_id_of_string (mk_s "UNICAST" (`Uni hdr) bdy sign) in *)
  (* let multi hdr bdy sign _ = Com.Mk.msg_id_of_string (mk_s "MULTICAST" (`Multi hdr) bdy sign) in *)
  let uni hdr bdy sign _ = mk_s "UNICAST" (`Uni hdr) bdy sign in
  let multi hdr bdy sign _ = mk_s "MULTICAST" (`Multi hdr) bdy sign in
  fold uni multi
let message_id hashf = Com.Mk.msg_id_of_string % message_id_s hashf
end

module Message_router = struct
  type message =
    | Join of { addr: Com.pubkey; local: bool }
    | Join_ack of { addr: Com.pubkey; result: Com.result' }
    | Leave of { addr: Com.pubkey }
    | Leave_ack of { addr: Com.pubkey; result: Com.result' }
    | Pull of { addr: Com.pubkey; id: Com.msg_id }
  type t = message

  let fold ~join ~join_ack ~leave ~leave_ack ~pull = function
    | Join j -> join j.addr j.local
    | Join_ack ja -> join_ack ja.addr ja.result
    | Leave l -> leave l.addr
    | Leave_ack la -> leave_ack la.addr la.result
    | Pull p -> pull p.addr p.id

  let join_ack_fold f =
    let err1 _ = failwith "not a join_ack" in
    let err2 _ _ = failwith "not a join_ack" in
    fold ~join:err2 ~leave:err1 ~leave_ack:err2 ~pull:err2 ~join_ack:f

  let leave_ack_fold f =
    let err1 _ = failwith "not a leave_ack" in
    let err2 _ _ = failwith "not a leave_ack" in
    fold ~join:err2 ~leave:err1 ~leave_ack:f ~pull:err2 ~join_ack:err2

  module Acc = struct
    let join_ack = let f _ r = r in join_ack_fold f
  end

  module T = struct
    let join addr local = Join { addr; local }
    let join_ack addr result = Join_ack { addr; result }
    let leave addr = Leave { addr }
    let leave_ack addr result = Leave_ack { addr; result }
    let pull addr id = Pull { addr; id }
  end
  module Encode = struct
    let pubkey, result = Com.Encode.(pubkey, result)
    let message = function
      | Join j -> SE.array [
        `Int 1;
        SE.map' [
          "addr", pubkey j.addr;
          "local", SE.bool j.local;
        ];
      ]
      | Join_ack ja -> SE.array [
        `Int 2;
        SE.map' [
          "addr", pubkey ja.addr;
          "result", result ja.result;
        ];
      ]
      | Leave l -> SE.array [
        `Int 3;
        SE.map' [
          "addr", pubkey l.addr;
        ];
      ]
      | Leave_ack la -> SE.array [
        `Int 4;
        SE.map' [
          "addr", pubkey la.addr;
          "result", result la.result;
        ];
      ]
      | Pull p -> SE.array [
        `Int 5;
        SE.map' [
          "addr", pubkey p.addr;
          "id", Com.Encode.msg_id p.id;
        ];
      ]
    let encode = SE.encode message
  end
  module Decode = struct
    let pubkey = Com.Decode.pubkey
    let result = Com.Decode.result
    let join' =
      SD.map_values_2 T.join (
        ("addr", pubkey),
        ("local", SD.bool)
      )
    let join_ack' =
      SD.map_values_2 T.join_ack (
        ("addr", pubkey),
        ("result", result)
      )
    let leave' =
      SD.map_values T.leave (
        ("addr", pubkey)
      )
    let leave_ack' =
      SD.map_values_2 T.leave_ack (
        ("addr", pubkey),
        ("result", result)
      )
    let pull' =
      SD.map_values_2 T.pull (
        ("addr", pubkey),
        ("id", Com.Decode.msg_id)
      )
    let join = SD.lift snd (SD.array_tuple2 (SD.int' 1) join')
    let join_ack = SD.lift snd (SD.array_tuple2 (SD.int' 2) join_ack')
    let leave = SD.lift snd (SD.array_tuple2 (SD.int' 3) leave')
    let leave_ack = SD.lift snd (SD.array_tuple2 (SD.int' 4) leave_ack')
    let pull = SD.lift snd (SD.array_tuple2 (SD.int' 5) pull')
    let message = SD.choice ~failure_msg:"unable to parse message router message" [
      join; join_ack; leave; leave_ack; pull;
    ]
  end
  module Mk = struct
    let encoded_join ~ttl ~exp (src_priv, src_pub) message_router_addr multicast_group =
      let dst = message_router_addr in
      (* --- local will always be false until the pubsub functionality exists *)
      let local = false in
      let msg = T.join multicast_group local in
      let body = Encode.encode msg in
      Base.Mk.encoded_unicast ~ttl ~exp (src_priv, src_pub) dst body
    let encoded_join_ack ~ttl ~exp (src_priv, src_pub) dst result' =
      let msg = T.join_ack dst result' in
      let body = Encode.encode msg in
      Base.Mk.encoded_unicast ~ttl ~exp (src_priv, src_pub) dst body
    let encoded_leave ~ttl ~exp (src_priv, src_pub) message_router_addr multicast_group =
      let dst = message_router_addr in
      let msg = T.leave multicast_group in
      let body = Encode.encode msg in
      Base.Mk.encoded_unicast ~ttl ~exp (src_priv, src_pub) dst body
    let encoded_leave_ack ~ttl ~exp (src_priv, src_pub) dst result' =
      let msg = T.leave_ack dst result' in
      let body = Encode.encode msg in
      Base.Mk.encoded_unicast ~ttl ~exp (src_priv, src_pub) dst body
    let encoded_pull ~ttl ~exp (src_priv, src_pub) message_router_addr multicast_group message_id =
      let dst = message_router_addr in
      let msg = T.pull multicast_group message_id in
      let body = Encode.encode msg in
      Base.Mk.encoded_unicast ~ttl ~exp (src_priv, src_pub) dst body
  end
end

module Peer_advertisement = struct
  type addr_type = Tcp | Udp
  type addr = Addr of { addr_type: addr_type; ip: Commons.ip; port: Commons.port }
  type service_type_peer_discovery = Peer_discovery
  type service_type_pubsub = Pubsub
  type 'service_type peer_service = Peer_service of { service_type: 'service_type; service_addr: Com.pubkey }
  type peer_services = service_type_peer_discovery peer_service option * service_type_pubsub peer_service option
  (* --- @enhancement uint? *)
  type revision = Revision of int
  type t = Peer_advertisement of {
    (* --- node id *)
    id: Com.pubkey;
    (* --- transport address *)
    addr: addr;
    svcs: peer_services;
    grps: Commons.multicast_group_pubkey list;
    rev: revision;
    ttl: Com.ttl;
    exp: Com.expiry;
    sign: Com.signature;
  }

  module T = struct
    let tcp = Tcp
    let udp = Udp
    let addr addr_type ip port = Addr { addr_type; ip; port }
    let addr' (addr_type, ip, port) = Addr { addr_type; ip; port }
    let revision i = Revision i
    let peer_service service_type service_addr =
      Peer_service { service_type; service_addr }
    let peer_services opd ops =
      Option.map (peer_service Peer_discovery) opd,
      Option.map (peer_service Pubsub) ops
    let peer_advertisement id addr svcs grps rev ttl exp sign = Peer_advertisement
      { id; addr; svcs; grps; rev; ttl; exp; sign }
  end

  module Acc = struct
    let id (Peer_advertisement pa) = pa.id
    let addr (Peer_advertisement pa) = pa.addr
    let addr_addr_type pa =
      let Addr addr' = addr pa in addr'.addr_type
    let addr_ip pa =
      let Addr addr' = addr pa in addr'.ip
    let addr_port pa =
      let Addr addr' = addr pa in addr'.port
    let svcs (Peer_advertisement pa) = pa.svcs
    let svcs' pa =
      let s1, s2 = svcs pa in
      match s1, s2 with
      | None, None -> []
      | Some (Peer_service svc), None -> [some @@ `Peer_discovery svc.service_addr]
      | None, Some (Peer_service svc) -> [some @@ `Pubsub svc.service_addr]
      | Some (Peer_service svc1), Some (Peer_service svc2) ->
          [some @@ `Peer_discovery svc1.service_addr; some @@ `PubSub svc2.service_addr]
    let grps (Peer_advertisement pa) = pa.grps
    let rev (Peer_advertisement pa) = pa.rev
    let rev' pa = let Revision t = rev pa in t
    let ttl (Peer_advertisement pa) = pa.ttl
    let ttl' pa = let Ttl t = ttl pa in U32.to_int t
    let exp (Peer_advertisement pa) = pa.exp
    let exp' pa = let Expiry e = exp pa in U32.to_int e
    let sign (Peer_advertisement pa) = pa.sign
    let sign' pa = Com.Acc.signature (sign pa)
  end

  module Encode = struct
    let addr_type = function
      | Tcp -> `Int 1
      | Udp -> `Int 2

    (* ip/port encoding is different from the one in Commons so that we match the spec exactly *)
    let ip_pkg = function
      | Ipaddr.V4 v4 -> SE.bytes (Ipaddr.V4.to_octets v4)
      | Ipaddr.V6 v6 -> SE.bytes (Ipaddr.V6.to_octets v6)
    let ip i = ip_pkg (Commons.Acc.ip i)

    (* The spec specifies a uint of size 2, which is what this is as long as the input is from [0, 65535]).
       @todo use uint16 throughout for port?
    *)
    let port = SE.int % Commons.Acc.port
    let addr (Addr addr) = SE.array [
      addr_type addr.addr_type;
      ip addr.ip;
      port addr.port;
    ]
    let service_addr = Com.Encode.pubkey
    let group = Commons.Encode.multicast_group_pubkey
    let revision (Revision i) = SE.int i
    let peer_service (Peer_service s) = service_addr s.service_addr
    let peer_advertisement (Peer_advertisement msg) =
      let open UEncode in
      SE.map' [
      "id", Com.Encode.pubkey msg.id;
      "addr", addr msg.addr;
      "svcs", mk_map [
        kv_opt (`Int 1) peer_service (fst msg.svcs);
        kv_opt (`Int 2) peer_service (snd msg.svcs);
      ];
      "grps", SE.list group msg.grps;
      "rev", revision msg.rev;
      "ttl", Com.Encode.ttl msg.ttl;
      "expiry", Com.Encode.expiry msg.exp;
      "sig", Com.Encode.signature msg.sign;
    ]

    let message msg = SE.array [ `Int 1; peer_advertisement msg; ]
    let encode = SE.encode message
  end

  module Decode = struct
    open SD.Infix
    let addr_type =
      SD.int >>= function
      | 1 -> SD.return Tcp
      | 2 -> SD.return Udp
      | _ -> SD.fail "Wrong integer, expected 1 or 2."
    let ip_pkg =
      SD.byte_string >>= fun s ->
        match String.length s with
        | 4 ->
            begin match Ipaddr.V4.of_octets s with
              | Ok ip -> SD.return @@ Ipaddr.V4 ip
              | Error (`Msg s) -> SD.fail s
            end
        | 16 ->
            begin match Ipaddr.V6.of_octets s with
              | Ok ip -> SD.return @@ Ipaddr.V6 ip
              | Error (`Msg s) -> SD.fail s
            end
        | _ -> SD.fail "Wrong number of bytes, expected 4 or 16."
    let ip = SD.lift (fun i -> Commons.T.ip i) ip_pkg
    let port =
      let open SD.Let in
      let* p = SD.int in
      match Commons.Mk.port p with
      | Ok x -> SD.return x
      | Error (`Msg m) -> SD.fail @@ Uio.fmt_err "port: %s" m
    let addr = SD.lift
      (fun (addr_type, ip, port) -> Addr { addr_type; ip; port })
      (SD.array_tuple3 addr_type ip port)
    let revision = SD.lift T.revision SD.int
    let services =
      let f = function
        | [] -> SD.return (None, None)
        | [(1, key)] -> SD.return (Some (T.peer_service Peer_discovery key), None)
        | [(2, key)] -> SD.return (None, Some (T.peer_service Pubsub key))
        | [(1, key1); (2, key2)] -> SD.return
          (Some (T.peer_service Peer_discovery key1),
           Some (T.peer_service Pubsub key2))
        | _ -> SD.fail "Unexpected services"
      in SD.bind f (SD.map (SD.int, Com.Decode.pubkey))
    let peer_advertisement =
      SD.map_values_8 T.peer_advertisement (
        ("id", Com.Decode.pubkey),
        ("addr", addr),
        ("svcs", services),
        ("grps", SD.array_list Commons.Decode.multicast_group_pubkey),
        ("rev", revision),
        ("ttl", Com.Decode.ttl),
        ("expiry", Com.Decode.exp),
        ("sig", Com.Decode.signature)
      )

    let message =
      SD.lift
        snd
        (SD.array_tuple2 (SD.int' 1) peer_advertisement)

  end

  module Pp = struct
	let addr_type ppf = function
	  | Udp -> Fmt.pf ppf "Udp"
	  | Tcp -> Fmt.pf ppf "Tcp"
    let addr' ppf (Addr { addr_type = addr_type'; ip; port}) =
      let () = Fmt.pf ppf "@[<v>" in
      let () = Fmt.pf ppf "%a@," Uio.(pp_hpair yellow addr_type) ("addr_type", addr_type') in
      let () = Fmt.pf ppf "%a@," Uio.(pp_hpair magenta Commons.Pp.ip) ("ip", ip) in
      let () = Fmt.pf ppf "%a" Uio.(pp_hpair cyan Commons.Pp.port) ("port", port) in
      Fmt.pf ppf "@]"
    let peer_service ppf (Peer_service {service_addr; _}) =
      Fmt.pf ppf "%a" Commons.Pp.pubkey service_addr
    let peer_services ppf (opd, ops) =
      let () = Fmt.pf ppf "@[<v>" in
      let () = Fmt.pf ppf "%a@," Uio.(pp_hpair green (fmt_option "none" peer_service)) ("Peer discovery", opd) in
      let () = Fmt.pf ppf "%a" Uio.(pp_hpair red (fmt_option "none" peer_service)) ("Pubsub", ops) in
      Fmt.pf ppf "@]"
    let revision ppf (Revision i) = Fmt.pf ppf "%d" i
    let peer_advertisement ppf (Peer_advertisement { id; addr; svcs; grps; rev; ttl; exp; sign }) =
      let () = Fmt.pf ppf "@,                         " in
      let () = Fmt.pf ppf "@[<v>@," in
      let () = Fmt.pf ppf "%a@," Uio.(pp_hpair yellow Commons.Pp.pubkey) ("id", id) in
      let () = Fmt.pf ppf "%a@," Uio.(pp_hpair magenta addr') ("addr", addr) in
      let () = Fmt.pf ppf "%a@," Uio.(pp_hpair yellow peer_services) ("peer services", svcs) in
      let () = Fmt.pf ppf "%a@," Uio.(pp_hpair bright_red (Commons.pp_comma_list Commons.Pp.multicast_group_pubkey)) ("groups", grps) in
      let () = Fmt.pf ppf "%a@," Uio.(pp_hpair cyan revision) ("revision", rev) in
      let () = Fmt.pf ppf "%a@," Uio.(pp_hpair green Com.Pp.ttl) ("ttl", ttl) in
      let () = Fmt.pf ppf "%a@," Uio.(pp_hpair cyan Com.Pp.expiry') ("expiry", exp) in
      let () = Fmt.pf ppf "%a@," Uio.(pp_hpair bright_red Com.Pp.signature) ("signature", sign) in
      Fmt.pf ppf "@]"
  end

  module Mk = struct
    let encoded ?(test_tamper=false) ~id (ip, port, addr_type) (opd, ops) ~grps ~rev ~ttl ~exp ~priv_key =
      let addr_type' = match addr_type with | `Tcp -> T.tcp in
      let addr = T.addr addr_type' ip port in
      let rev = T.revision rev in
      (* --- @todo what body should we use to compute the signature? *)
      let body = "some string" in
      let sign =
        let s = Crypto.sign priv_key body in
        Com.T.signature (if test_tamper then s ^ "a" else s) in
      let svcs = T.peer_services opd ops in
      let ttl' = Com.T.ttl (U32.of_int ttl) in
      let exp' = Com.T.expiry (U32.of_int exp) in
      let peer_advertisement = T.peer_advertisement id addr svcs grps rev ttl' exp' sign in
      peer_advertisement |> Encode.encode
  end
end

module Application = struct
  type header = Header of {
    deps: Com.msg_id list;
    from: Com.pubkey;
    to': Com.pubkey;
    seq: U32.t;
    sign: Com.signature;
  }
  type body =
    | Ack
    | Unicast of string
    | Multicast of string
    | Member_add of Com.pubkey list1
    | Member_remove of Com.pubkey list1
  type t = Application of {
    header: header;
    body: body;
  }
  module T = struct
    let header deps from to' seq sign = Header {
      deps; from; to'; seq; sign;
    }
    let ack = Ack
    let unicast s = Unicast s
    let multicast s = Multicast s
    let member_add ks = Member_add ks
    let member_remove ks = Member_remove ks
  end
  module Pp = struct
    let u32 ppf = Fmt.pf ppf "%d" % U32.to_int
    let deps = Commons.pp_comma_list Com.Pp.msg_id
    let body ppf = function
      | Ack -> Fmt.pf ppf "ack"
      | Unicast s -> Fmt.pf ppf "unicast: %a" binary_string s
      | Multicast s -> Fmt.pf ppf "multicast: %a" binary_string s
      | Member_add ks -> Fmt.pf ppf "member add: %a" (Commons.pp_comma_list Com.Pp.pubkey) ks
      | Member_remove ks -> Fmt.pf ppf "member remove: %a" (Commons.pp_comma_list Com.Pp.pubkey) ks
    let application ppf (Application { header = Header header'; body = body' }) =
      let () = Fmt.pf ppf "@[<v>" in
      let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair yellow deps) ("deps", header'.deps) in
      let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair magenta Com.Pp.pubkey) ("from", header'.from) in
      let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair bright_red Com.Pp.pubkey) ("to", header'.to') in
      let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair green u32) ("seq", header'.seq) in
      let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair cyan Com.Pp.signature) ("sign", header'.sign) in
      let () = Fmt.pf ppf "%a@ " Uio.(pp_hpair magenta body) ("body", body') in
      Fmt.pf ppf "@]"
  end
  module Encode = struct
    let deps = SE.list Com.Encode.msg_id
    let u32 = UEncode.u32
    let header (Header header') = SE.map' [
      "deps", deps header'.deps;
      "from", Com.Encode.pubkey header'.from;
      "to", Com.Encode.pubkey header'.to';
      "seq", u32 header'.seq;
      "sig", Com.Encode.signature header'.sign;
    ]
    let body = function
      | Ack -> SE.array [ `Int 0; SE.null ]
      | Unicast s -> SE.array [ `Int 1; SE.bytes s ]
      | Multicast s -> SE.array [ `Int 2; SE.bytes s ]
      | Member_add ks -> SE.array [ `Int 11; SE.map' [
        "mem", list1_enc Com.Encode.pubkey ks;
      ]]
      | Member_remove ks -> SE.array [ `Int 12; SE.map' [
        "mem", list1_enc Com.Encode.pubkey ks;
      ]]
    let message (Application { header = header'; body = body' }) =
      SE.array [ header header'; body body'; ]
    let encode = SE.encode message
  end
  module Decode = struct
    let u32 = UDecode.u32
    let deps = SD.array_list Com.Decode.msg_id
    let header = SD.map_values_5 T.header (
      ("deps", deps),
      ("from", Com.Decode.pubkey),
      ("to", Com.Decode.pubkey),
      ("seq", u32),
      ("sig", Com.Decode.signature)
    )
    let body' f n p = SD.lift (f % snd) (SD.array_tuple2 (SD.int' n) p)
    let ack = body' (const T.ack) 0 SD.null
    let unicast = body' T.unicast 1 SD.byte_string
    let multicast = body' T.multicast 2 SD.byte_string
    let member_add =
      let add' = SD.map_values T.member_add (
        "mem", list1_dec Com.Decode.pubkey
      ) in body' id 11 add'
    let member_remove =
      let remove' = SD.map_values T.member_remove (
        "mem", list1_dec Com.Decode.pubkey
      ) in body' id 12 remove'
    let body = SD.choice ~failure_msg:"Application msg body" [
      ack; unicast; multicast; member_add; member_remove;
    ]
    let message =
      let f (header, body) = Application { header; body } in
      SD.lift f (SD.array_tuple2 header body)
  end
  module Mk = struct
    let sign_ privkey str = Com.T.signature (Crypto.sign privkey (SE.encode Encode.body str))
    let mk_ ~ttl ~exp ~seen ~deps ~from ~to' ~seq body group =
      let privkey = fst @@ Commons.Acc.multicast_group_rw' group in
      let sign = sign_ privkey body in
      let header = Header { deps; from; to'; seq; sign } in
      let msg = Application { header; body } in
      let body = Encode.encode msg in
      Base.Mk.encoded_multicast ~ttl ~exp ~seen group body
    let encoded_ack ~ttl ~exp ~seen ~deps ~from ~to' ~seq group =
      let body = T.ack in
      mk_ ~ttl ~exp ~seen ~deps ~from ~to' ~seq body group
    let encoded_unicast ~ttl ~exp ~seen ~deps ~from ~to' ~seq group msg =
      let body = T.unicast msg in
      mk_ ~ttl ~exp ~seen ~deps ~from ~to' ~seq body group
    let encoded_multicast ~ttl ~exp ~seen ~deps ~from ~to' ~seq group msg =
      let body = T.multicast msg in
      mk_ ~ttl ~exp ~seen ~deps ~from ~to' ~seq body group
    let encoded_member_add ~ttl ~exp ~seen ~deps ~from ~to' ~seq group keys =
      let body = T.member_add keys in
      mk_ ~ttl ~exp ~seen ~deps ~from ~to' ~seq body group
    let encoded_member_remove ~ttl ~exp ~seen ~deps ~from ~to' ~seq group keys =
      let body = T.member_remove keys in
      mk_ ~ttl ~exp ~seen ~deps ~from ~to' ~seq body group
  end
end

let mk_encoded_unicast_message = Base.Mk.encoded_unicast
let mk_unicast_message = Base.Mk.unicast
let mk_encoded_multicast_message = Base.Mk.encoded_multicast
let mk_multicast_message = Base.Mk.multicast
let mk_encoded_mr_join_message = Message_router.Mk.encoded_join
let mk_encoded_mr_join_ack_message = Message_router.Mk.encoded_join_ack
let mk_encoded_mr_leave_message = Message_router.Mk.encoded_leave
let mk_encoded_mr_leave_ack_message = Message_router.Mk.encoded_leave_ack
let mk_encoded_mr_pull_message = Message_router.Mk.encoded_pull
let mk_encoded_peer_advertisement_message = Peer_advertisement.Mk.encoded
let mk_encoded_app_ack_message = Application.Mk.encoded_ack
let mk_encoded_app_unicast_message = Application.Mk.encoded_unicast
let mk_encoded_app_multicast_message = Application.Mk.encoded_multicast
let mk_encoded_app_member_add_message = Application.Mk.encoded_member_add
let mk_encoded_app_member_remove_message = Application.Mk.encoded_member_remove
