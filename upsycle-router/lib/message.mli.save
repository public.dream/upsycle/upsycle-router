module SD := Seaboar.Decode
module SDA := Seaboar.Decode.Angstrom
module SE := Seaboar.Encode
module U32 := Stdint.Uint32
(* val id : 'a -> 'a *)
type pubkey = Crypto.pubkey
type privkey = Crypto.privkey
type ttl = Ttl of U32.t
type expiry = Expiry of U32.t
type hash = Hash of string
type msg_id = Msg_id of hash
type seen = Seen of msg_id list
type unicast_header =
    Unicast_header of { src : pubkey; dst : pubkey; ttl : ttl option;
      exp : expiry option;
    }
type multicast_header =
    Multicast_header of { grp : pubkey; ttl : ttl option;
      exp : expiry option; seen : seen option;
    }
type header =
    [ `Multi_header of multicast_header | `Uni_header of unicast_header ]
type body = Body of string
type via = Via of pubkey
type sign = Sign of string
type message =
    Unicast_message of { header : unicast_header; body : body; sign : sign;
      via : via option;
    }
  | Multicast_message of { header : multicast_header; body : body;
      sign : sign; via : via option;
    }
module T :
  sig
    val body : string -> body
    val expiry : U32.t -> expiry
    val pubkey : string -> (pubkey, [ `Msg of string ]) result
    val sign : string -> sign
    val ttl : U32.t -> ttl
    val via : pubkey -> via
  end
module Encode :
  sig
    val mk_obj : (string * SE.value) list list -> SE.value
    val mk_list : SE.value list list -> SE.value
    val kv_opt : 'a -> ('b -> 'c) -> 'b option -> ('a * 'c) list
    val list_elem_opt : ('a -> 'b) -> 'a option -> 'b list
    val pubkey_encoder : pubkey -> SE.value
    val ttl_encoder : ttl -> SE.value
    val expiry_encoder : expiry -> SE.value
    val hash_encoder : hash -> SE.value
    val msg_id_encoder : msg_id -> SE.value
    val seen_encoder : seen -> SE.value
    val body_encoder : body -> SE.value
    val sign_encoder : sign -> SE.value
    val via_encoder : via -> SE.value
    val unicast_header_encoder : unicast_header -> SE.value
    val multicast_header_encoder : multicast_header -> SE.value
    val message_encoder : message -> SE.value
    val encode_message : message -> string
  end
val try_iter :
  ('a -> int option -> 'b option -> int -> int option) ->
  'a list -> [> `Nok | `Ok ]
module Parse :
  sig
    val ( >>= ) :
      'a SD.angstrom_t -> ('a -> 'b SD.angstrom_t) -> 'b SD.angstrom_t
    val ( <$> ) : ('a -> 'b) -> 'a SD.angstrom_t -> 'b SD.angstrom_t
    val unicast_header_parser : unicast_header SD.angstrom_t
    val multicast_header_parser : multicast_header SD.angstrom_t
    val body_parser : body SD.angstrom_t
    val sign_parser : sign SD.angstrom_t
    val pubkey_parser : pubkey SD.angstrom_t
    val via_parser : via SD.angstrom_t
    val message_parser : message SDA.t
  end
val mk_unicast :
  ?test_tamper:bool ->
  ?ttl:int -> ?exp:int -> ?via:pubkey ->
  privkey * pubkey -> pubkey ->
  string -> message
val mk_encoded_unicast :
  ?ttl:int -> ?exp:int -> ?via:pubkey ->
  privkey * pubkey -> pubkey ->
  string -> string
val get_uni_src : message -> pubkey
val get_uni_dst : message -> pubkey
val is_terminal : pubkey -> message -> bool
val is_multicast : message -> bool
val get_terminal_src : pubkey -> message -> bool * pubkey
val get_body : message -> body
val get_sign : message -> sign
val pp_pubkey :
  Format.formatter -> pubkey -> unit
val pp_ttl : Format.formatter -> ttl -> unit
val pp_expiry : Format.formatter -> expiry -> unit
val pp_hash : Format.formatter -> hash -> unit
val pp_msg_id : Format.formatter -> msg_id -> unit
val pp_seen : Format.formatter -> seen -> unit
val pp_header :
  Format.formatter ->
  [< `Multi_header of multicast_header | `Uni_header of unicast_header ] ->
  unit
val pp_body : Format.formatter -> body -> unit
val pp : Format.formatter -> message -> unit
module Public :
  sig
    val mk_encoded_unicast_message :
      ?ttl:int -> ?exp:int -> ?via:pubkey ->
      privkey * pubkey -> pubkey ->
      string -> string
    val mk_unicast_message :
      ?test_tamper:bool ->
      ?ttl:int -> ?exp:int -> ?via:pubkey ->
      privkey * pubkey -> pubkey ->
      string -> message
  end
