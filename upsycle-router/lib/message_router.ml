module Config = Message_router_config
module Route = Message_router_route
module Server = Message_router_server
module State = Message_router_state
module Stream = Message_router_stream

module UConduit = Util_conduit
module Uio = Util_io
module ULwt = Util_lwt
module UPure = Util_pure

open! Basic

let is_local (cnf: Config.t) = Util_pure.Set.find_safe cnf.local_services

(** Checks whether the unicast message came from a local service by checking its
    peerkey (not src) against the list of known services. Also provides
    protection against spoofing of src by ensuring that it matches the peerkey
    if the above check was true. Rejects on failure. *)
let verify_unicast_src (cnf: Config.t) src peerkey =
  let is_from_local = is_local cnf peerkey in
  (* --- if the src is local, then it must match the peerkey. *)
  let () = if is_from_local && src <> peerkey then failwith
    "src field of message from local service does NOT match peerkey, aborting" in
  (* --- if the src is not local (i.e. the message is from a remote MR or remote service),
     the keys may or may not match. We could possibly add more verification
     steps here in the future. *)
  Lwt.return is_from_local

let update_via info new_via stream_message =
  let message = Stream.Acc.message stream_message in
  let prf fmt = ULwt.mk_prf info fmt in
  let* () = prf "message arrived via remote node, setting via to: %a"
    Commons.Pp.pubkey new_via in
  Lwt.return @@ Message.Base.via_set new_via message

let relay_or_store (cnf: Config.t) type' info addr encoder ttl_computed msg_id msg =
  let prf fmt = ULwt.mk_prf info fmt in
  match State.get_conduit addr with
  | None ->
      let* () =
        let ttl, exp = Message.Base.Acc.(ttl msg, exp msg) in
        prf "no connection found, queueing message (ttl = %a, exp = %a)"
          Message.Com.Pp.ttl ttl
          Message.Com.Pp.expiry' exp in
      let* timer' = match State.set_unqueue_timer ttl_computed cnf.msg_id_hash_function msg (type', addr) with
        | Error (`Msg e) -> failwith @@ Uio.fmt_out "Unable to set unqueue timer (check ttl/exp): %s" e
        | Ok x -> Lwt.return x in
      let () = State.queue_message addr msg_id timer' msg in
      let () = State.got_message_queued type' in
      Lwt.return `Stored
  | Some conn' ->
    let* () = UConduit.send conn' encoder msg in
    Lwt.return `Relayed

(** Looks up a connection for the `dst` field of the message. If it has one, it
    sends the message through it, and if not, it stores the message. *)
let relay_uni_message (cnf: Config.t) info ttl_computed msg_id msg =
  let relay_or_store' type' dst' umsg' =
    let locality_s' = match type' with
    | `Local -> "local node"
    | `Remote -> "remote node"
    | `Remote_via -> "remote node (found in via field)" in
    let info' = Uio.fmt_out "%s relaying unicast message to %s at %a"
      info locality_s' Commons.Pp.pubkey dst' in
    ULwt.ignore' @@ relay_or_store
      cnf `Uni info' dst'
      Message.Base.Encode.message ttl_computed msg_id umsg' in

  let do_to_local = relay_or_store' `Local in
  let do_from_local dst' umsg' =
    match Message.Base.Acc.via' umsg' with
    (* § Unicast routing algorithm ¶ 3.3.1. “If VIA is set to V, it forwards the
       message to the message router V.” *)
    | Some via' -> relay_or_store' `Remote_via via' umsg'
    (* § Unicast routing algorithm ¶ 3.3.2. “Otherwise it looks up D in the
       routing table and forwards the message to the message router associated
       with D.” *)
    | None -> relay_or_store' `Remote dst' umsg' in

  let err' _ = failwith "relay_uni_message (): not a unicast message" in
  msg |> Fun.flip Message.Base.fold' err' @@ fun umsg' ->
    let src' = Message.Base.get_uni_src umsg' in
    let dst' = Message.Base.get_uni_dst umsg' in
    let src_is_local' = is_local cnf src' in
    let dst_is_local' = is_local cnf dst' in
    let prf fmt = ULwt.mk_prf info fmt in

    ULwt.cond [
      (* § Unicast routing algorithm ¶ 3.2. “If there's a local service
         registered with public key D, then it forwards the message to D.” *)
      dst_is_local', begin fun _ -> do_to_local dst' umsg' end;
      (* § Unicast routing algorithm ¶ 3.3. “It checks whether source S is a
         local service … ” *)
      src_is_local', begin fun _ -> do_from_local dst' umsg' end;
    ] ~otherwise: begin fun _ ->
      let s = "no matching unicast rules, dropping message" in
      let* () = prf "%s" s in
      failwith s end

let send_ack_to_client type' type_s (cnf: Config.t) info connection group_addr result' =
  let mk = match type' with
  | `Join_ack -> Message.mk_encoded_mr_join_ack_message
  | `Leave_ack -> Message.mk_encoded_mr_leave_ack_message in
  (* --- @todo ttl & exp *)
  let msg = mk ~ttl:5000 ~exp:5000 (cnf.privkey, cnf.pubkey) group_addr result' in
  let prf fmt = ULwt.mk_prf info fmt in
  let* () = Lwt_unix.sleep cnf.join_ack_delay in
  let* () = prf "sending %s" type_s in
  UConduit.send_s connection msg

let send_join_ack_to_client = send_ack_to_client `Join_ack "join_ack"
let send_leave_ack_to_client = send_ack_to_client `Leave_ack "leave_ack"

let handle_msg_verify_signature signkey message =
  Message.Base.verify_signature signkey message
    |> ULwt.decorate_rejection "message has invalid signature, dropping"

(* § Unicast routing algorithm ¶ 2. “If the message arrived via a remote
  connection … and adds the public key of the source service to the routing table.” *)

(* Note that we also update the table when a message is received from a local service. *)
let handle_unicast_msg_update_routes info src is_from_local_service stream_message =
  let oconnection = Stream.Acc.connection stream_message in
  let prf fmt = ULwt.mk_prf info fmt in
  match is_from_local_service with
  | true ->
      let* () = prf "message arrived via local service" in
      begin match oconnection with
      | None -> prf "got a test message -- no connection and not updating routing table"
      | Some conn ->
          let* () = prf "updating routing table" in
          Lwt.return @@ State.update_routes_incoming src conn `Local
      end
  | false ->
      let* () = prf "message arrived via remote node" in
      begin match oconnection with
      | None -> prf "got a test message -- no connection and not updating routing table"
      | Some conn ->
          let* () = prf "updating routing table" in
          Lwt.return @@ State.update_routes_incoming src conn `Remote
      end

let handle_msg_update_via info new_via is_from_local_service stream_message =
  if not is_from_local_service then
    let* msg' = update_via info new_via stream_message in
    Lwt.return (msg', true)
  else Lwt.return (Stream.Acc.message stream_message, false)

let handle_msg_router_join info requestor addr local =
  let prf fmt = ULwt.mk_prf info fmt in
  let* () = prf "@.    @[<v>got unicast message router %a message:@ requestor = %a@ multicast group = %a@ local = %s@]"
    Uio.green "JOIN"
    Commons.Pp.pubkey requestor
    Commons.Pp.pubkey addr
    (Bool.to_string local)
  in
  let group' = Commons.Mk.multicast_group_pubkey addr in
  let* () = prf "updating multicast table" in
  let () = State.add_to_multicast_group' group' requestor in
  (* --- @todo when does this fail? *)
  let ok = true in
  Lwt.return ok

let handle_msg_router_leave info requestor addr =
  let prf fmt = ULwt.mk_prf info fmt in
  let* () = prf "@.    @[<v>got unicast message router %a message:@ requestor = %a@ multicast group = %a@]"
    Uio.green "LEAVE"
    Commons.Pp.pubkey requestor
    Commons.Pp.pubkey addr
  in
  let group' = Commons.Mk.multicast_group_pubkey addr in
  let* () = prf "updating multicast table" in
  let () = State.remove_from_multicast_group' group' requestor in
  (* --- @todo when does this fail? *)
  let ok = true in
  Lwt.return ok

(* --- @todo if the message router doesn't have this message in the cache it
   should probably forward the request to other message routers but this isn't
   detailed in the spec. *)
let handle_pull info connection _group message_id =
  let prf fmt = ULwt.mk_prf info fmt in
  match State.get_message_from_cache message_id with
  | None -> prf "we don't have message with id %a in the cache, forwarding to other message routers (@todo)"
    Message.Com.Pp.msg_id message_id
  | Some msg -> UConduit.send connection Message.Base.Encode.message msg

let parser_test_or_real allow_test_body parser' test_string =
  let open Seaboar.Decode in
  let parser_real =
    let f x = `Real x in
    lift f parser' in
  let parser_test =
    let f x = `Test x in
    lift f (utf8_string' test_string) in
  if allow_test_body then (parser_real <|> parser_test)
  else parser_real

let handle_msg_router_msg (cnf: Config.t) info connection src peerkey message =
  let prf fmt = ULwt.mk_prf info fmt in
  let* requestor = if src = peerkey then Lwt.return src else
    let msg' which = prf
      ("src and peerkey don't match, meaning this is a relayed message -- " ^^
      "preferring %s due to prefer_multicast_requestor flag") which in
    match cnf.prefer_multicast_requestor with
    | `Src -> let* () = msg' "src" in Lwt.return src
    | `Peerkey -> let* () = msg' "peerkey" in Lwt.return peerkey in
  let join addr local =
    let* ok' = handle_msg_router_join info requestor addr local in
    let result' = Message.Com.T.(if ok' then ok else nok) in
    send_join_ack_to_client cnf info connection addr result' in
  let join_ack _ =
    failwith "handle_msg_router_msg: protocol violation: message router received JOIN_ACK" in
  let leave addr =
    let* ok' = handle_msg_router_leave info requestor addr in
    let result' = Message.Com.T.(if ok' then ok else nok) in
    send_leave_ack_to_client cnf info connection addr result' in
  let leave_ack _ =
    failwith "handle_msg_router_msg: protocol violation: message router received LEAVE_ACK" in
  let pull addr id =
    let* () = prf "got pull request for multicast group %a msg_id %a"
      Commons.Pp.pubkey addr Message.Com.Pp.msg_id id in
    handle_pull info connection addr id in
  Message.Message_router.fold ~join ~join_ack ~leave ~leave_ack ~pull message

let handle_unicast_for_us (cnf: Config.t) info stream_message message =
  let stream_message' = Stream.Acc.stream_msg stream_message in
  let src = Message.Base.get_uni_src message in
  let prf fmt = ULwt.mk_prf info fmt in
  let body' = Message.Base.Acc.body' message in
  let oconnection' = Stream.Acc.connection stream_message in
  let peerkey = stream_message'.peerkey in
  let parser' = parser_test_or_real
    cnf.allow_test_body
    (Message.Message_router.Decode.message)
    "test body" in
  match Util_encode_decode.parse_string parser' body' with
  | Ok (`Real message') ->
      begin match oconnection' with
      | None -> prf "got a message with no connection -- this is only for testing"
      | Some conn' -> handle_msg_router_msg cnf info (Route.conduit_of_connection conn') src peerkey message' end
  | Ok (`Test _) -> prf "got a test body"
  | Error e -> failwith @@ "Unable to parse message which was addressed to us: " ^ e

let handle_unicast_relay (cnf: Config.t) info ttl_computed msg_id message =
  ULwt.ignore' @@ relay_uni_message cnf info ttl_computed msg_id message

let handle_unicast (cnf: Config.t) info ttl_computed msg_id stream_message =
  let stream_message' = Stream.Acc.stream_msg stream_message in
  let message = stream_message'.message in
  let peerkey = stream_message'.peerkey in
  let ourkey = stream_message'.ourkey in
  let src = Message.Base.get_uni_src message in
  (* --- rejects if source was spoofed. *)
  let* src_is_local = verify_unicast_src cnf src peerkey in
  let prf fmt = ULwt.mk_prf info fmt in
  (* § Unicast routing algorithm ¶ 1. “… If the signature is invalid, it drops
     the message …” *)
  (* --- rejects on bad signature. *)
  let* () = handle_msg_verify_signature src message in
  let* () = prf "signature verified %a" Uio.green "✔" in
  let* () = handle_unicast_msg_update_routes info src src_is_local stream_message in
  (* § Unicast routing algorithm ¶ 2. “If the message arrived via a remote
     connection, then it sets the VIA field …” *)
  let* message', altered_via = handle_msg_update_via info src src_is_local stream_message in
  let* () = ULwt.wanneer altered_via @@ fun () ->
    (* --- @todo refactor *)
    match cnf.print_messages_on_console with
    | `None -> Lwt.return ()
    | `Short -> prf "@.    %a %a" Uio.underline "altered" Message.Base.pp_short message'
    | `Long -> prf "@.    %a %a" Uio.underline "altered" Message.Base.pp message' in
  let is_for_us = Message.Base.is_for ourkey message in
  (* § Unicast routing algorithm ¶ 3.1. “If the message is addressed to the
     message router X itself … then it processes the message.” *)
  if is_for_us then
    let+ () = handle_unicast_for_us cnf info stream_message message' in
    State.got_message_processed `Uni
  else handle_unicast_relay cnf info ttl_computed msg_id message'

let handle_peer_advertisement_msg _info pa_message =
  let* () = info "Message router received a peer_advertisement: %a" Message.Peer_advertisement.Pp.peer_advertisement pa_message in
  let ip = Message.Peer_advertisement.Acc.addr_ip pa_message in
  let port = Message.Peer_advertisement.Acc.addr_port pa_message in
  let ip_port = Commons.Mk.ip_port ip port in
  let pubkey = Message.Peer_advertisement.Acc.id pa_message in
  let+ () = info "Update state with addr: %a@, ip: %a@, and port: %a@."
    Commons.Pp.pubkey pubkey
    Commons.Pp.ip ip
    Commons.Pp.port port in
  State.update_routes_on_peer_advertisement pubkey ip_port

(** Handle a multicast message which was sent to a group we are subscribed to.
    This is expected to be either a peer advertisement message or a test message with a specific string.
 *)
let handle_multicast_message_for_us info (cnf: Config.t) is_test message =
  let body' = Message.Base.Acc.body' message in
  let prf fmt = ULwt.mk_prf info fmt in
(*   Note, right now the only multicast messages addressed to a MR that are processed, are wrapped peer_advertisements. *)
  let parser' = parser_test_or_real
    cnf.allow_test_body
    Message.Peer_advertisement.Decode.message
    "test body multicast" in
  match Util_encode_decode.parse_string parser' body' with
  | Ok (`Real pa_message) ->
      begin match is_test with
      | true -> prf "We received a test peer_advertisement: %a@,
    It has no connection -- this is only for testing and it will not be processed"
         Message.Peer_advertisement.Pp.peer_advertisement pa_message
      | false -> handle_peer_advertisement_msg info pa_message end
  | Ok (`Test _) -> prf "got a multicast test body"
  | Error e -> failwith @@ "Unable to parse the body of a multicast message which was addressed to us as a peer_advertisement: " ^ e

(* § Multicast routing algorithm ¶ 4. “It checks whether the message has been
   received before by looking up the message ID in the list of recently
   received messages. If found, it drops the message, since it has been
   processed before.” *)
let check_cache info msg_id =
  let prf fmt = ULwt.mk_prf info fmt in
  ULwt.wanneer (State.has_message_in_cache msg_id) @@ fun _ ->
    let s = "duplicate message received (already in cache) -- droppping message" in
    let* () = prf "%s" s in failwith s

let handle_multicast_direct' (cnf: Config.t) info subs ttl_computed msg_id stream_message =
  let prf fmt = ULwt.mk_prf info fmt in
  let message = Stream.Acc.message stream_message in
  let peerkey' =
    let stream_message' = Stream.Acc.stream_msg stream_message in
    stream_message'.peerkey in
  let is_test = Option.is_none @@ Stream.Acc.connection stream_message in
  (* --- rejects if message exists in cache. *)
  let* () = check_cache info msg_id in
  (* § Multicast routing algorithm ¶ 5. “It adds the message ID to the list of recently received messages …” *)
  (* § Multicast routing algorithm ¶ 6. “It saves the message in the key-value store with the message ID as key.” *)
  (* --- note, we assume step 5 and 6 are equivalent (the list = the key-value store = our cache. *)
  let () = State.add_to_message_cache msg_id message in
  (* § Multicast routing algorithm ¶ 7. “If the message arrived from a remote node R, it sets the VIA field to R.” *)
  let* message', altered_via' = handle_msg_update_via info peerkey' (is_local cnf peerkey') stream_message in
  let* () = ULwt.wanneer altered_via' @@ fun _ ->
    (* --- @todo refactor *)
    match cnf.print_messages_on_console with
    | `None -> Lwt.return ()
    | `Short -> prf "@.    %a %a" Uio.underline "altered" Message.Base.pp_short message'
    | `Long -> prf "@.    %a %a" Uio.underline "altered" Message.Base.pp message' in
  (* § Multicast routing algorithm ¶ 8. “It forwards the message to all local subscribers.” *)
  let* () =
    let l = List.length subs in
    prf "this group has %s, not counting the sender" (UPure.english_plural' "subscriber" l) in
  let f = function
    | sub' when sub' = cnf.pubkey ->
      handle_multicast_message_for_us info cnf is_test message'
    | sub' ->
      let info' = Uio.fmt_out "%s publishing message to group subscriber %a" info Commons.Pp.pubkey sub' in
      relay_or_store cnf `Multi info' sub' Message.Base.Encode.message ttl_computed msg_id message'
      |> ULwt.ignore' in
  let+ () = Lwt_list.iter_p f subs in
  State.got_message_processed `Multi

(** Direct, meaning, there was no VIA in the message, so we handle this ourselves. *)
let handle_multicast_direct (cnf: Config.t) info ttl_computed msg_id group stream_message =
  let prf fmt = ULwt.mk_prf info fmt in
  let group_pubkey' = Commons.Acc.multicast_group_pubkey group in
  let fail_unknown_group' () =
    let s = Util_io.fmt_out
      "protocol error: group %a is unknown, meaning it has no subscribers, \
      not even the sender of this message (client is misbehaving) -- dropping message"
      Commons.Pp.pubkey group_pubkey' in
    let* () = prf "%s" s in failwith s in
  let go' = function
    | 0, _ ->
      (* § Multicast routing algorithm ¶ 3. “If there are no local subscribers
         for G, it drops the message.” *)
      let s = "this group has no subscribers, not counting the sender -- dropping message" in
      let* () = prf "%s" s in failwith s
    | _, subs' -> handle_multicast_direct' cnf info subs' ttl_computed msg_id stream_message in
  match State.get_subscribers_for_multicast_group group with
  | None -> fail_unknown_group' () | Some x when x = BatSet.empty -> fail_unknown_group' ()
  | Some subs ->
      let peerkey' = Stream.Acc.peerkey stream_message in
      go' begin subs
        |> BatSet.to_seq
        |> BatSeq.map Commons.Acc.multicast_subscriber
        (* --- a multicast message should never be sent back to the peer which sent it. *)
        |> BatSeq.filter ((<>) peerkey')
        |> BatList.of_seq
        |> fun subs' -> List.length subs', subs' end

let handle_multicast (cnf: Config.t) info ttl_computed msg_id stream_message =
  let message' = Stream.Acc.message stream_message in
  let prf fmt = ULwt.mk_prf info fmt in
  Message.Base.fold_flip_multi_unwrap' message' @@ fun group body _ ovia ->
    let* () = prf "got multicast message for group %a with body: %a"
      Commons.Pp.multicast_group_pubkey group
      Message.Base.Pp.body' (Body body) in
    let group_pubkey' = Commons.Acc.multicast_group_pubkey group in
    (* § Multicast routing algorithm ¶ 1. “… If the signature is invalid, it
       drops the message …” *)
    (* --- rejects on bad signature. *)
    let* () = handle_msg_verify_signature group_pubkey' message' in
    let* () = prf "signature verified %a" Uio.green "✔" in
    match ovia with
    (* § Multicast routing algorithm ¶ 2. “If VIA is set to V, it forwards the
       message to the remote message router V, and stops processing.” *)
    | Some via' -> ULwt.ignore' @@
      relay_or_store cnf `Multi info via' Message.Base.Encode.message ttl_computed msg_id message'
    | None -> handle_multicast_direct cnf info ttl_computed msg_id group stream_message

let ttl_computed msg =
  let ttl', exp' = Message.Base.Acc.(ttl' msg, exp' msg) in
  let exp_rel' = Time.Time_upsycle_unix.to_rel exp' in
  min ttl' exp_rel'

let handle_msg (cnf: Config.t) info stream_message =
  let stream_message' = Stream.Acc.stream_msg stream_message in
  let peerkey' = stream_message'.peerkey in
  let message' = stream_message'.message in
  let info' = Uio.fmt_out "%s %s handle-msg from %a" info stream_message'.info Commons.Pp.pubkey peerkey' in
  let ttl_computed' = ttl_computed message' in
  let msg_id' = Message.Base.message_id cnf.msg_id_hash_function message' in
  let prf fmt = ULwt.mk_prf info' fmt in
  let* () = ULwt.wanneer (ttl_computed' < 0) @@ fun _ ->
    failwith "ttl or expiry is in the past, rejecting" in
  let* () = match cnf.print_messages_on_console with
    | `None -> Lwt.return ()
    | `Short -> prf "@.    @[%a@]" Message.Base.pp_short message'
    | `Long -> prf "@.    @[%a@]" Message.Base.pp message' in
  if Message.Base.is_unicast message'
    then let () = State.got_message_received `Uni in
      begin fun _ -> handle_unicast cnf info' ttl_computed' msg_id' stream_message end
      |> ULwt.on_rejection' (fun _ -> Message_router_state.got_message_rejected `Uni)
    else let () = State.got_message_received `Multi in
      begin fun _ -> handle_multicast cnf info' ttl_computed' msg_id' stream_message end
      |> ULwt.on_rejection' (fun _ -> Message_router_state.got_message_rejected `Multi)

let onconnect (cnf: Config.t) info_str conn' =
  let UConduit.Connection.Connection conn = conn' in
  let Peerkey peerkey = conn.peerkey in
  let info_str' = Uio.fmt_out "%s on-connect ->" info_str in
  let prf fmt = ULwt.mk_prf info_str' fmt in
  ULwt.wanneer cnf.update_routing_table_on_client_connect @@ fun () ->
    let* () = prf "updating routing table (update_routing_table_on_client_connect is true)" in
    let type' = if is_local cnf peerkey then `Local else `Remote in
    let () = State.update_routes_incoming' peerkey conn' type' in
    let msgs', clear' = State.get_and_delay_clear_queued_messages peerkey in
    let+ () = match List.length msgs' with
    | 0 -> prf "no queued messages for client"
    | n ->
      let* () = prf "processing %d queued messages for recently connected client" n in
      let f msg =
        let msg' = State.Message_queue.message_of_message_with_timer msg in
        let type' = if Message.Base.is_unicast msg' then `Uni else `Multi in
        let info_str' = conn.info_str ^ " relay" in
        (* --- @enhancement inefficient, would be good to retrieve this above *)
        let msg_id' = Message.Base.message_id cnf.msg_id_hash_function msg' in
        relay_or_store cnf type' info_str' peerkey Message.Base.Encode.message (ttl_computed msg') msg_id' msg' in
      let g (t, r, s) = function
        | `Relayed -> t + 1, r + 1, s
        | `Stored -> t + 1, r, s + 1 in
      let* result' = Lwt_list.map_p f msgs' in
      let (total', relayed', stored') = List.fold_left g (0, 0, 0) result' in
      ULwt.wanneer (total' != 0) @@ fun _ ->
        prf "  result: relayed %d / stored %d" relayed' stored' in
    clear' ()

let connect_to_message_routers (cnf: Config.t) =
  let hs = cnf.remote_routers in
  let rec g (h: Commons.remote_router) =
    let info_str = Uio.fmt_out "[message router %a-> message router %a]"
        (Uio.pp_yellow Commons.Pp.pubkey) cnf.pubkey
        Commons.Pp.pubkey h.pubkey in
    let ip = Commons.Acc.ip h.ip in
    Lwt.try_bind
    (fun () -> Message_router_client.connect
      ~hostname:h.routername
      ~info_str
      ~ip
      ~key_path:cnf.key_path
      ~cert_path:cnf.cert_path
      ~port:h.port
      ~pubkey:h.pubkey
      `Message_router)
    (fun _conn -> Lwt.return ())
    (function
      | Unix.Unix_error _ ->
        let* () = info "No connection established to message router %a, trying again in 5 seconds"
          (Uio.pp_red Commons.Pp.pubkey) h.pubkey in
        let* () = Lwt_unix.sleep 5. in
        g h
      | e -> warn "connect_to_message_routers: error: %a" Fmt.exn e
    )
  in
  Lwt.join (List.map g hs)

(* --- at the moment this function takes a remote address for which it expects
   to have an existing connection and will reject if it can't find one; later we
   could try to open a connection if it doesn't exist or has been closed. *)
let send_peer_advertisement_about_us (cnf: Config.t) ~addr_type ~rev ~ttl_multicast ~ttl_pa ~exp_multicast ~exp_pa info remote_addr =
  let info' = info ^ " sending peer advertisement about this node" in
  let prf fmt = ULwt.mk_prf info' fmt in
  let* () = prf "" in
  let msg_router_connection = match State.get_conduit remote_addr with
  | None -> failwith @@ Uio.fmt_out
    "send_peer_advertisement_about_us: no connection for addr: %a" Commons.Pp.pubkey remote_addr
  | Some c -> c in
  (* --- at the moment we're not advertising local services *)
  let opeer_discovery, opubsub = None, None in
  let grps = Message_router_state.get_groups_from_multicast_table () in
  let pa_msg = Message.mk_encoded_peer_advertisement_message
    ~id:cnf.pubkey (cnf.ip, cnf.port, addr_type) (opeer_discovery, opubsub) ~grps
    ~rev ~ttl:ttl_pa ~exp:exp_pa ~priv_key:cnf.privkey in
  (* --- at the moment we're not maintaining a 'seen' list in the message router *)
  let seen_of_pub = const (Message.Com.Mk.seen 1) in
  Message_router_client.publish_to_groups
    ~prepend:info' ~ttl:ttl_multicast ~exp:exp_multicast
    cnf.multicast_groups_for_peer_advertisement
    seen_of_pub msg_router_connection pa_msg

let init config =
  let () = State.init config in
  Lwt_stream.create ()

let start ?threads (cnf: Config.t) =
  let* () = ULwt.wanneer cnf.key_printing_short @@
    (fun _ -> info "public keys being shown abridged, see key-printing-short option") in
  let () = Util_crypto_io.set_key_printing_short cnf.key_printing_short in
  let () = Util_crypto_io.init () in
  let name' = Uio.fmt_out "• message-router %a •" Uio.cyan cnf.message_router_name in
  let msg_stream', push_stream' = init cnf in
  let push_stream_threads' =
    let f cb = cb cnf push_stream' in
    let g = List.map f in
    Option.fold ~none:[] ~some:g threads in
  Lwt.choose @@ [
    Server.server ~onconnect cnf name' push_stream';
    Lwt.join [
      connect_to_message_routers cnf;
      Message_router_stream.stream_read_thread cnf name' handle_msg msg_stream';
    ];
  ] @ push_stream_threads'
