module Crypto = Commons.Crypto
module UConduit = Util_conduit

open! Basic

let lwt_all_mapi f = Lwt.all % List.mapi f

let connect ~hostname ~ip ~info_str ~key_path ~cert_path ~port ~pubkey type' =
  let server_fingerprint = Crypto.fingerprint_ed25519_sha256 pubkey in
  let mk_peerkey key = Util_conduit.Peerkey (Commons.Mk.pubkey key) in
  let port' = Commons.Acc.port port in
  let* Connection conn as conn' = Util_conduit.open_client_connection
    ~info_str ~server_fingerprint ~client_auth:(`Cert (cert_path, key_path))
    hostname ip port' mk_peerkey in
  let prf = Util_lwt.mk_prf conn.info_str in
  let* () = prf "connected to message router" in
  let () = match type' with
  | `Message_router ->
      Message_router_state.update_routes_outgoing' pubkey (Commons.Mk.ip_port_raw ip port') conn'
  | `Service -> () in
  Lwt.return conn'

let send_join_to_message_router ~ttl ~exp message_router_conn our_privkey our_pubkey message_router_addr multicast_group =
  Message.mk_encoded_mr_join_message ~ttl ~exp (our_privkey, our_pubkey) message_router_addr multicast_group
  |> UConduit.send_s message_router_conn

let send_leave_to_message_router ~ttl ~exp message_router_conn our_privkey our_pubkey message_router_addr multicast_group =
  Message.mk_encoded_mr_leave_message ~ttl ~exp (our_privkey, our_pubkey) message_router_addr multicast_group
  |> UConduit.send_s message_router_conn

let send_pull_to_message_router
    ~ttl ~exp message_router_conn
    our_privkey our_pubkey message_router_addr multicast_group message_id =
  Message.mk_encoded_mr_pull_message ~ttl ~exp (our_privkey, our_pubkey) message_router_addr multicast_group message_id
  |> UConduit.send_s message_router_conn

let send_message_to_peer ~ttl ~exp ~body message_router_conn our_privkey our_pubkey peer_pubkey =
  Message.mk_encoded_unicast_message ~ttl ~exp (our_privkey, our_pubkey) peer_pubkey body
  |> UConduit.send_s message_router_conn

(* --- @todo at the moment we just send a string as the body, but it should actually be a message. What kind of message is it? *)
let send_message_to_topic ~ttl ~exp ~seen message_router_conn group_rw msg_string =
  Message.mk_encoded_multicast_message ~ttl ~exp ~seen group_rw msg_string
  |> UConduit.send_s message_router_conn

let send_message_to_topic_of_remote_router ~ttl ~exp ~seen ~pubkey group_rw msg_string =
  match Message_router_state.get_conduit pubkey with
  | None -> info "no existing connection to remote router with pubkey %a" Commons.Pp.pubkey pubkey
  | Some conn ->
    let* () = info "connection to this peer is open, sending message" in
    send_message_to_topic ~ttl ~exp ~seen conn group_rw msg_string

(** [msg] is currently a string. *)
let publish_to_groups ?prepend ~ttl ~exp groups seen_of_pub msg_router_connection msg =
  let prf fmt = Util_conduit.mk_prf ?prepend msg_router_connection fmt in
  let num_groups' = List.length groups in
  let publish' priv pub =
    let* () = send_message_to_topic ~ttl ~exp ~seen:(seen_of_pub pub)
      msg_router_connection (Commons.Mk.multicast_group_rw (priv, pub)) msg in
    Lwt.return `Ok in
  let try_publish' i mg =
    match Commons.Acc.multicast_group mg with
    | `Rw (priv, pub) ->
        let* () = prf "publishing to topic %d/%d %a" i num_groups' Commons.Pp.pubkey pub in
        publish' priv pub
    | `Ro pub ->
        let* () = prf "skipping topic %d/%d %a -- we don't have the private key"
          i num_groups' Commons.Pp.pubkey pub in
        Lwt.return `No_group in
  let* res = lwt_all_mapi try_publish' groups in
  let g sum = function
  | `Ok -> sum + 1
  | `No_group -> sum in
  match List.fold_left g 0 res with
  | 0 -> Lwt.return `No_groups
  | n -> Lwt.return (`Ok n)
