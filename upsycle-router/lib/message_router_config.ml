(* These comments apply to both {!message_router_config.ml} and
   {!service_config.ml}.

   We keep generally useful types and their yaml derivations in Commons.

   There are also some types ending in `_cnf` which are declared here, not in
   commons. These are polymorphic variant types so they are easy to construct
   and destruct without having to go through top-level exports, typically in
   application code in bin/. Basically they're just (nested) named tuples.

   The code in bin/ may also declared types with names like `remote_router'` for
   example, which are only used in that file and basically used to read yaml
   files. Those types may also contain types exported through the top-level,
   which may come from Commons or from this module, which are therefore opaque
   to the bin code, but which can be constructed using `_of_yaml` functions
   also exported through the top level.
*)

module Crypto = Commons.Crypto
module UPure = Util_pure

type multicast_requestor = Commons.multicast_requestor
type control_interface = Commons.host_port
type key_set = Commons.pubkey BatSet.t

type remote_router_cnf = [ `Rr of
  [ `Cf of string ] * [ `Ip of Commons.ip ] * [ `P of Commons.port ] *
  [ `Rn of string ]
]

type multicast_entry_cnf = [ `Mte of
  [ `Gp of string ] * [ `Subs of string list ]
]

let keys_of_key_set = BatSet.to_list

let mk_multicast_entry openssl_cmd base_dir (e: multicast_entry_cnf) =
  let `Mte ( `Gp group, `Subs subscribers ) = e in
  let subscribers' = subscribers
    |> List.map (fun subs -> subs
      |> Commons.Crypto.pubkey_of_keystring_or_file_exn ~base_dir openssl_cmd
      |> Commons.Mk.multicast_subscriber)
    |> BatSet.of_list in
  let multicast_group_pubkey' = group
    |> Commons.Crypto.pubkey_of_keystring_or_file_exn ~base_dir openssl_cmd
    |> Commons.Mk.multicast_group_pubkey in
  (multicast_group_pubkey', subscribers')

let mk_multicast_table openssl_cmd base_dir es = es
  |> List.map (mk_multicast_entry openssl_cmd base_dir)
  |> Util_pure.BatMap.of_list

type message_router_route' = [ `Mrr of
  [ `Addr of string ] * [ `Rt of [
    | `Ls (* local-service *)
    | `Rsa (* remote-service anonymous *)
    | `Rsk of string * Commons.ip_port (* remote-service known *)
    | `Rra (* remote-router anonymous *)
    | `Rrk of Commons.ip_port (* remote-router known *)
  ]]
]

let mk_routing_table_init =
  let open Message_router_route.Mk in
  let f (`Mrr (`Addr addr, `Rt rt)) =
    let mk s = match Commons.Crypto.Mk.pubkey_base64 s with
    (* --- @todo deal with error *)
    | Error (`Msg e) -> failwith @@ Util_io.fmt_out "bad base64 key %s %s" s e
    | Ok x -> x in
    let addr = mk addr in
    match rt with
  | `Ls -> local_service_defunct addr
  | `Rsa -> remote_service_defunct addr
  | `Rsk (addr_mr, ip_port) -> remote_service_known_nc addr (mk addr_mr) ip_port
  | `Rra -> remote_router_defunct addr
  | `Rrk ip_port -> remote_router_known_nc addr ip_port
  in List.map f

let mk_remote_router openssl_cmd base_dir r =
  let `Rr (`Cf cf, `Ip ip, `P port, `Rn routername) = r in
  let cf' = UPure.mk_absolute_path base_dir cf in
  let pubkey = Commons.Crypto.pubkey_of_certfile_exn openssl_cmd cf' in
  Commons.Mk.remote_router pubkey ip port routername

let mk_remote_routers openssl_cmd base_dir rs =
  List.map (mk_remote_router openssl_cmd base_dir) rs

type t = {
  allow_test_body: bool;
  cache_size: int;
  cert_path: string;
  control_interface: control_interface option;
  ip: Commons.ip;
  join_ack_delay: float;
  key_path: string;
  local_services: key_set;
  message_router_name: string;
  msg_id_hash_function: Commons.msg_id_hash_function;
  multicast_groups_for_peer_advertisement: Commons.multicast_group list;
  multicast_table_init: Commons.multicast_table;
  openssl_cmd: Commons.command;
  port: Commons.port;
  prefer_multicast_requestor: multicast_requestor;
  print_messages_on_console: Commons.print_messages_on_console;
  key_printing_short: bool;
  privkey: Commons.privkey;
  pubkey: Commons.pubkey;
  remote_routers: Commons.remote_router list;
  routing_table_init: Message_router_route.t list;
  seen_size: int;
  serialize_state_path: string;
  test_tamper_signatures_on_mock_messages: float;
  update_routing_table_on_client_connect: bool;
}

let key_set_of_cert_files openssl_cmd base_dir cfs =
  let f cf set =
    let cf' = UPure.mk_absolute_path base_dir cf in
    let pubkey = Commons.Crypto.pubkey_of_certfile_exn openssl_cmd cf' in
    BatSet.add pubkey set in
  List.fold_right f cfs BatSet.empty

let mk ~base_dir
  allow_test_body cache_size cert_path control_interface ip join_ack_delay key_path
  key_printing_short local_services message_router_name msg_id_hash_function
  multicast_table_init multicast_groups_for_peer_advertisement openssl_cmd port prefer_multicast_requestor print_messages_on_console
  remote_routers routing_table_init seen_size serialize_state_path test_tamper_signatures_on_mock_messages
  update_routing_table_on_client_connect : t =
    let mk_abs = UPure.mk_absolute_path base_dir in
    let key_path' = mk_abs key_path in {
    allow_test_body;
    cache_size;
    cert_path = mk_abs cert_path;
    control_interface;
    ip;
    join_ack_delay;
    key_path = key_path';
    local_services = key_set_of_cert_files openssl_cmd base_dir local_services;
    message_router_name;
    msg_id_hash_function;
    multicast_groups_for_peer_advertisement;
    multicast_table_init = mk_multicast_table openssl_cmd base_dir multicast_table_init;
    openssl_cmd;
    port;
    prefer_multicast_requestor;
    print_messages_on_console;
    key_printing_short;
    privkey = Commons.Crypto.privkey_of_keyfile_exn openssl_cmd key_path';
    pubkey = Commons.Crypto.pubkey_of_keyfile_exn openssl_cmd key_path';
    remote_routers = mk_remote_routers openssl_cmd base_dir remote_routers;
    routing_table_init = mk_routing_table_init routing_table_init;
    seen_size;
    serialize_state_path;
    test_tamper_signatures_on_mock_messages;
    update_routing_table_on_client_connect;
  }

let control_interface_of_yaml, control_interface_to_yaml = Commons.Yaml.(host_port_of_yaml, host_port_to_yaml)

let key_set_of_yaml = function
  | `A xs ->
      let f x key_set_result =
        let key_result = Commons.Yaml.pubkey_of_file_of_yaml x in
        Util_pure.Result.lift2
          BatSet.add
          key_result
          key_set_result in
    List.fold_right f xs (Ok BatSet.empty)
  | _ -> Error (`Msg ("key_set_of_yaml"))
let key_set_to_yaml =
  let f xs = `A xs in
  let h x acc = Crypto.pubkey_to_yaml x :: acc in
  let g s = BatSeq.fold_right h (BatSet.to_seq s) [] in
  f % g

let local_services_of_yaml = key_set_of_yaml
let local_services_to_yaml = key_set_to_yaml
