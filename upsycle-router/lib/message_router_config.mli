(** {!message_router_config.ml} and {!service_config.ml} provide a type {!t}
    which is used throughout the application for representing configuration
    values which apply to message routers / services. The type is private
    (declared using the `private` keyword), so it can only be constructed using
    the provided {!mk} function, but its members can be accessed using dot
    syntax. *)

module Crypto := Commons.Crypto

type 'a of_yaml := 'a Commons.of_yaml
type 'a to_yaml := 'a Commons.to_yaml

type control_interface = Commons.host_port
type key_set = Commons.pubkey BatSet.t

type remote_router_cnf = [ `Rr of
  [ `Cf of string ] * [ `Ip of Commons.ip ] * [ `P of Commons.port ] *
  [ `Rn of string ]
]

type multicast_entry_cnf = [ `Mte of
  [ `Gp of string ] * [ `Subs of string list ]
]

val keys_of_key_set : key_set -> Commons.pubkey list

type t = private {
  allow_test_body: bool;
  cache_size: int;
  cert_path: string;
  control_interface: control_interface option;
  ip: Commons.ip;
  join_ack_delay: float;
  key_path: string;
  local_services: key_set;
  message_router_name: string;
  msg_id_hash_function: [ `Blake2b_32 | `Blake2b_256 | `Blake2s_32 | `Blake2s_128 ];
  multicast_groups_for_peer_advertisement: Commons.multicast_group list;
  multicast_table_init: Commons.multicast_table;
  openssl_cmd: Commons.command;
  port: Commons.port;
  prefer_multicast_requestor: Commons.multicast_requestor;
  print_messages_on_console: Commons.print_messages_on_console;
  key_printing_short: bool;
  privkey: Commons.privkey;
  pubkey: Commons.pubkey;
  remote_routers: Commons.remote_router list;
  routing_table_init: Message_router_route.t list;
  seen_size: int;
  serialize_state_path: string;
  test_tamper_signatures_on_mock_messages: float;
  update_routing_table_on_client_connect: bool;
}

val key_set_of_cert_files : Commons.command -> string -> string list -> key_set

type message_router_route' = [ `Mrr of
  [ `Addr of string ] * [ `Rt of [
    | `Ls (* local-service *)
    | `Rsa (* remote-service anonymous *)
    | `Rsk of string * Commons.ip_port (* remote-service known *)
    | `Rra (* remote-router anonymous *)
    | `Rrk of Commons.ip_port (* remote-router known *)
  ]]
]

val mk :
  base_dir:string ->
  bool -> int -> string -> control_interface option -> Commons.ip -> float ->
  string -> bool -> string list -> string -> Commons.msg_id_hash_function -> multicast_entry_cnf list ->
  Commons.multicast_group list -> Commons.command -> Commons.port -> Commons.multicast_requestor ->
  Commons.print_messages_on_console -> remote_router_cnf list -> message_router_route' list ->
  int -> string -> float -> bool -> t

val control_interface_of_yaml : control_interface of_yaml
val control_interface_to_yaml : control_interface to_yaml
val key_set_of_yaml : Yaml.value -> key_set Yaml.res
val key_set_to_yaml : key_set -> Yaml.value
val local_services_of_yaml : key_set of_yaml
val local_services_to_yaml : key_set to_yaml
