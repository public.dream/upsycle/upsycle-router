module SD = Seaboar.Decode
module SE = Seaboar.Encode

module Crypto = Commons.Crypto

open! Basic

let ip_of_yaml, ip_to_yaml = Commons.Yaml.(ip_of_yaml, ip_to_yaml)
let port_of_yaml, port_to_yaml = Commons.Yaml.(port_of_yaml, port_to_yaml)

type pubkey = Commons.pubkey
type addr = pubkey
type diagnostic = Diagnostic of { ip_port: Commons.ip_port }

type connection_incoming = Connection_incoming of {
  conduit: pubkey Util_conduit.connection;
  diagnostic: diagnostic;
}
type connection_outgoing = Connection_outgoing of {
  conduit: pubkey Util_conduit.connection;
}
type connection =
  | Connection_incoming' of connection_incoming
  | Connection_outgoing' of connection_outgoing

type route_direct_incoming' = Route_direct_incoming' of connection
(* 'known' means we have an IP and a port to which we can initiate a
   connection from our end. If `connection` is `None` then it was closed or
   never established, but this is different from 'defunct' because we still
   useful information (how to (re-)connect). *)

type route_direct_known' = Route_direct_known' of {
  addr: addr;
  ip_port: Commons.ip_port;
  connection: connection option;
}
(* These represent connections which others make to us but which we can't
   initiate (e.g. a remote router which connected to us, but for which we
   don't have routing information (yet)), or don't need to initiate (e.g.
   local services).

   State transition: direct anonymous incoming -> direct known is valid,
   meaning that a peer discovery event was received while the connection
   was open. *)
type route_direct_anonymous =
  | Route_direct_incoming of route_direct_incoming'
  (* Defunct is for an incoming connection which gets closed, or a route
     which is restored from saved state or provided at startup via the config. *)
  | Route_direct_defunct
type route_direct =
  | Route_direct_anonymous of route_direct_anonymous
  | Route_direct_known of route_direct_known'

type route_indirect = Route_indirect of route_direct (* --- next-hop route *)

type route =
  | Remote_service of {
    addr: addr;
    route: route_indirect;
  }
  | Remote_router of {
    addr: addr;
    route: route_direct;
  }
  | Local_service of {
    addr: addr;
    route: route_direct_anonymous;
  }

type t = route

let diag' (Util_conduit.Connection.Connection conduit_connection) =
  let ip, port = Util_conduit.ip_port_of_flow_exn conduit_connection.flow in
  Diagnostic { ip_port = Commons.Mk.ip_port_raw ip port }

module T = struct
  let connection_incoming conduit = Connection_incoming {
    conduit; diagnostic=diag' conduit;
  }
  let connection_outgoing conduit = Connection_outgoing {
    conduit;
  }
  let connection_incoming' c = Connection_incoming' c
  let connection_outgoing' c = Connection_outgoing' c
  let route_direct_incoming' c = Route_direct_incoming' c
  let route_direct_known' addr ip_port connection = Route_direct_known' {
    addr; ip_port; connection;
  }
  let route_direct_incoming r = Route_direct_incoming r
  let route_direct_defunct = Route_direct_defunct
  let route_direct_anonymous r = Route_direct_anonymous r
  let route_direct_known r = Route_direct_known r
  let route_indirect r = Route_indirect r
  let remote_service addr ri = Remote_service { addr; route=ri; }
  let remote_router addr route = Remote_router { addr; route; }
  let local_service addr route = Local_service { addr; route; }
end

module Mk = struct
  let route_direct_known_nc' addr ip_port = T.route_direct_known' addr ip_port None
  let route_direct_known_nc'' addr ip_port _ = route_direct_known_nc' addr ip_port
  let route_direct_known_nc addr ip_port = T.route_direct_known @@ route_direct_known_nc' addr ip_port
  let local_service_defunct addr = T.local_service addr T.route_direct_defunct
  let route_direct_incoming = T.route_direct_incoming % T.route_direct_incoming'
  let route_direct_known = T.route_direct_known
  let connection_incoming conduit_connection =
    T.connection_incoming' @@ T.connection_incoming conduit_connection
  let connection_outgoing conduit_connection =
    T.connection_outgoing' @@ T.connection_outgoing conduit_connection
  let route_direct_incoming_conduit_c conduit_connection =
    route_direct_incoming @@ connection_incoming conduit_connection
  let route_direct_incoming_c connection = route_direct_incoming connection
  let local_service_alive addr = T.local_service addr % route_direct_incoming
  let local_service_conduit_c addr conduit_connection =
    local_service_alive addr @@ connection_incoming conduit_connection
  let local_service_c addr connection = local_service_alive addr @@ connection
  let remote_service addr = T.remote_service addr % T.route_indirect
  let remote_service_defunct addr = remote_service addr @@ T.route_direct_anonymous T.route_direct_defunct
  let remote_service_known_nc addr_srv addr_mr ip_port = remote_service addr_srv @@ route_direct_known_nc addr_mr ip_port
  let remote_router_anonymous addr = T.remote_router addr % T.route_direct_anonymous
  let remote_router_defunct addr = remote_router_anonymous addr @@ T.route_direct_defunct
  let remote_router_known addr = T.remote_router addr % T.route_direct_known
  let remote_router_known' addr ip_port oconduit_connection =
    remote_router_known addr @@ T.route_direct_known' addr ip_port oconduit_connection
  let remote_router_known_nc addr ip_port = remote_router_known' addr ip_port None
  let remote_router_known_c addr ip_port conduit_connection = remote_router_known' addr ip_port @@ Some conduit_connection
  let remote_router_incoming_conduit_c addr conduit_connection = remote_router_anonymous addr @@ route_direct_incoming_conduit_c conduit_connection
  let remote_router_incoming addr connection = remote_router_anonymous addr @@ route_direct_incoming_c connection
  let remote_router_outgoing addr ip_port connection = remote_router_known' addr ip_port (Some connection)
end

module Encode = struct
  let addr = Commons.Encode.pubkey
  let route_direct_known' = function
    | Route_direct_known' { addr=addr'; ip_port=ip_port'; connection=_ } ->
        SE.variant "Route_direct_known" [addr addr'; Commons.Encode.ip_port ip_port'; SE.null]
  let route_direct_anonymous =
    let defunct' = SE.variant "Route_direct_defunct" []
    in function
      | Route_direct_incoming _ -> defunct'
      | Route_direct_defunct -> defunct'
  let route_direct = function
    | Route_direct_anonymous r ->
        SE.variant "Route_direct_anonymous" [route_direct_anonymous r]
    | Route_direct_known r ->
        SE.variant "Route_direct_known" [route_direct_known' r]
  let route_indirect = function
    | Route_indirect r -> SE.variant "Route_indirect" [route_direct r]
  let t : t SE.t = function
    | Remote_service { addr=addr'; route; } -> SE.variant "Remote_service" [
      addr addr'; route_indirect route; ]
    | Remote_router { addr=addr'; route; } -> SE.variant "Remote_router" [
      addr addr'; route_direct route; ]
    | Local_service { addr=addr'; route; } -> SE.variant "Local_service" [
      addr addr'; route_direct_anonymous route; ]
end

module Decode = struct
  let addr = Commons.Decode.pubkey
  let ip_port = SD.variant_2 "Ip_port" Commons.Mk.ip_port' (Commons.Decode.ip, SD.int)
  let route_direct_known' =
    SD.variant_3 "Route_direct_known" Mk.route_direct_known_nc'' (addr, ip_port, SD.null)
  let route_direct_anonymous =
    (* --- note that we should never encounter 'incoming' in the encoding. *)
    SD.variant_0 "Route_direct_defunct" T.route_direct_defunct
  let route_direct = SD.Angstrom.choice ~failure_msg:"Route.route_direct" [
    SD.variant_1 "Route_direct_anonymous" T.route_direct_anonymous route_direct_anonymous;
    SD.variant_1 "Route_direct_known" T.route_direct_known route_direct_known';
  ]
  let route_indirect =
    SD.variant_1 "Route_indirect" T.route_indirect route_direct
  let t : t SD.t = SD.Angstrom.choice ~failure_msg:"Route.t" [
    SD.variant_2 "Remote_service" T.remote_service (addr, route_indirect);
    SD.variant_2 "Remote_router" T.remote_router (addr, route_direct);
    SD.variant_2 "Local_service" T.local_service (addr, route_direct_anonymous);
  ]
end

module Pp = struct
  let addr = Commons.Pp.pubkey
  let ip = Commons.Pp.ip
  let ip_port ppf ipp =
    let ip', port' = Commons.Acc.ip_port' ipp in
    Fmt.pf ppf "%a:%d" ip ip' port'
  let ip_port' ppf (ip, port) = ip_port ppf (Commons.Mk.ip_port ip port)
  let diagnostic ppf (Diagnostic { ip_port=ip_port'; }) = Fmt.pf ppf "(%a)" ip_port ip_port'
  let connection_incoming ppf (Connection_incoming {
    conduit=_; diagnostic=diagnostic'
  }) = diagnostic ppf diagnostic'
  let connection ppf = function
    | Connection_incoming' ci -> Fmt.pf ppf "incoming ↙ %a" connection_incoming ci
    | Connection_outgoing' _ -> Fmt.pf ppf "outgoing ↗"
  let oconnection ppf = function
    | Some c -> Fmt.pf ppf "| %a %a |" Util_io.green "✶" connection c
    | None -> Fmt.pf ppf "| %a no active connection |" Util_io.red "⊘"
  let route_direct_anonymous ppf = function
    | Route_direct_incoming r -> begin match r with
        Route_direct_incoming' c -> connection ppf c end
    | Route_direct_defunct -> Fmt.pf ppf "| %a defunct incoming connection |"
      Util_io.yellow "🐟"
  let route_direct_known ppf (Route_direct_known' {
    addr=addr'; ip_port=ip_port'; connection=oconnection';
  }) = Fmt.pf ppf "%a %a %a"
    addr addr'
    ip_port ip_port'
    oconnection oconnection'
  let route_direct ppf = function
    | Route_direct_anonymous r -> Fmt.pf ppf "anonymous %a"
      route_direct_anonymous r
    | Route_direct_known r -> Fmt.pf ppf "%a"
      route_direct_known r
  let route_indirect ppf (Route_indirect route') = Fmt.pf ppf "%a %a"
    Util_io.underline "via"
    route_direct route'
  (* --- skip addr *)
  let t ppf = function
    | Remote_service { addr=_; route=route'; } -> Fmt.pf ppf "%a %a"
      Util_io.green "remote-service"
      route_indirect route'
    | Remote_router { addr=_; route=route'; } -> Fmt.pf ppf "%a %a"
      Util_io.yellow "remote-router"
      route_direct route'
    | Local_service { addr=_; route=route'; } -> Fmt.pf ppf "%a %a"
      Util_io.blue "local-service"
      route_direct_anonymous route'
end

let pp = Pp.t
let pp_ip_port' = Pp.ip_port'

let fold ~remote_service:f ~remote_router:g ~local_service:h = function
  | Remote_service { addr; route=route_id'; } -> f addr route_id'
  | Remote_router { addr; route=route_d'; } -> g addr route_d'
  | Local_service { addr; route=route_da'; } -> h addr route_da'

let route_addr =
  let f = fun addr _ -> addr in
  fold ~remote_service:f ~remote_router:f ~local_service:f

let flatmap_route ~remote_service:f ~remote_router:g ~local_service:h = fold
  ~remote_service:(fun _ r -> f r)
  ~remote_router:(fun _ r -> g r)
  ~local_service:(fun _ r -> h r)

let map_route ~remote_service:f ~remote_router:g ~local_service:h = function
  | Remote_service r -> Remote_service { r with route=f r.route }
  | Remote_router r -> Remote_router { r with route=g r.route }
  | Local_service r -> Local_service { r with route=h r.route }

let close_connection_route_direct = function
  | Route_direct_anonymous _ -> Route_direct_anonymous (Route_direct_defunct)
  | Route_direct_known (Route_direct_known' rdk) ->
      Route_direct_known (Route_direct_known' { rdk with connection = None })

let close_connection_route_indirect = function
  | Route_indirect rd -> Route_indirect (close_connection_route_direct rd)

let close_connection_local_service _ = Route_direct_defunct

(** Note that this doesn't close the underlying conduit connection; use `util_conduit.close_connection` for that *)
let close_connection = map_route
  ~remote_service:close_connection_route_indirect
  ~remote_router:close_connection_route_direct
  ~local_service:close_connection_local_service

let connection_of_route =
  let of_route_direct_incoming' (Route_direct_incoming' c) = Some c in
  let of_route_direct_anonymous = function
    | Route_direct_incoming rdi -> of_route_direct_incoming' rdi
    | Route_direct_defunct -> None in
  let of_route_direct_known' (Route_direct_known' r) = r.connection in
  let of_route_direct = function
    | Route_direct_anonymous rda -> of_route_direct_anonymous rda
    | Route_direct_known rdk -> of_route_direct_known' rdk
  in
  let of_route_indirect (Route_indirect rd) =
    of_route_direct rd in
  let of_remote_router = of_route_direct in
  let of_local_service = of_route_direct_anonymous in
  flatmap_route
  ~remote_service:of_route_indirect
  ~remote_router:of_remote_router
  ~local_service:of_local_service

let connection_fold f g = function
  | Connection_incoming' c -> f c
  | Connection_outgoing' c -> g c

let conduit_of_connection =
  let f (Connection_incoming ci) = ci.conduit in
  let g (Connection_outgoing co) = co.conduit in
  connection_fold f g
