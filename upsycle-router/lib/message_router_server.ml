module SD = Seaboar.Decode

module Crypto = Commons.Crypto
module Config = Message_router_config
module Route = Message_router_route
module State = Message_router_state
module Stream = Message_router_stream

open Basic

let debug_peer_key = false

let close_connection key conn =
  let* () = Util_conduit.close_connection conn in
  match State.close_connection key with
  | Ok _ -> Lwt.return true
  | Error (`Msg e) ->
      let* () = Util_lwt.info
        "no connection exists for key %a, nothing to do (maybe client aborted or logged off before sending its first message): %s."
        Commons.Pp.pubkey key e
      in Lwt.return false

(* If the client disconnects, the [Util_encode_decode_io.parse_stream] promise rejects.
 * On the client side, it seems to be more difficult to detect when the server has disconnected.
 * At the moment the best we can do is try to write and then catch the exception.
 * @todo is there a way to immediate abort the client connection? *)

let handle ?onconnect (cnf : Config.t) push_stream conn' =
  let Util_conduit.Connection.Connection conn = conn' in
  let Peerkey peerkey = conn.peerkey in
  let info_str = conn.info_str in
  let info_str' = Util_io.fmt_out "%s handle-connection %a" info_str Commons.Pp.pubkey peerkey in
  let prf fmt = Util_lwt.mk_prf info_str' @@ fmt in
  let debug' () =
    let* () = prf "peer key (our representation): %a" Commons.Pp.pubkey peerkey in
    let _, fingerp = Crypto.fingerprint_ed25519_sha256 peerkey in
    prf "peer key hexdump: %a" Cstruct.hexdump_pp fingerp in
  let* () = prf "new connection from %a:%d" Ipaddr.pp conn.peerip conn.peerport in
  let* () = match onconnect with
  | None -> Lwt.return ()
  | Some f -> f cnf info_str' conn' in
  let* () = Util_lwt.wanneer debug_peer_key debug' in
  let route_conn' = Route.Mk.connection_incoming conn' in
  let onmsg' msg =
    let () = push_stream % some @@ Stream.stream_msg route_conn' conn.info_str cnf.pubkey peerkey msg
    in Lwt.return true in
  let onbye' () =
    let* () = prf "client said adieu, closing connection if it still exists" in
    let* () = close_connection peerkey conn' >>=
      Util_lwt.wanneer_flip @@ fun () -> prf "connection closed"
    in Lwt.return false in
  let onerr' err =
    let* () = prf "Failed to parse: %s" err
    in Lwt.return `Fail in
  (* --- fulfilling with () is better than a rejection here, which would trigger a server exception. *)
  let onparse_err' err =
    let* () = info "error: %s: closing client connection if it still exists" err in
    let* ok = close_connection peerkey conn' in
    if ok then prf "connection closed" else Lwt.return ()

  in Commonp.parse_conduit_stream (Message.Base.Decode.message cnf.seen_size) conn' onmsg' onbye' onerr' onparse_err'

let server ?onconnect (cnf: Config.t) info push_stream =
  let handle' = handle ?onconnect cnf push_stream in
  let mk_peerkey key = Util_conduit.Peerkey (Commons.Mk.pubkey key) in
  let openssl_cmd = Commons.string_list_of_command cnf.openssl_cmd in
  let ip = Commons.Acc.ip cnf.ip in
  Util_conduit.start_server ~info_prefix:info ~openssl_cmd cnf.cert_path cnf.key_path ip (Commons.Acc.port cnf.port) mk_peerkey handle'
