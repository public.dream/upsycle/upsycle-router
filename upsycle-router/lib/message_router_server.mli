val server :
  ?onconnect:(Message_router_config.t -> string -> Commons.pubkey Util_conduit.Connection.t -> unit Lwt.t) ->
  Message_router_config.t ->
  string ->
  (Message_router_stream.stream_msg option -> unit) -> unit Lwt.t
