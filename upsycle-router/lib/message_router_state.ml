(** As the name suggests, this is a stateful module. The mutable parts are contained in the module `Mut`. *)

open Basic

module SD = Seaboar.Decode
module SE = Seaboar.Encode

module Crypto = Commons.Crypto
module CMap = Util_cache.CMap

module Route = Message_router_route

module UBatMap = Util_pure.BatMap
module UList = Util_pure.List
module UResult = Util_pure.Result

let table_enc, table_dec = Util_encode_decode.(Encode.table, Decode.table)
let minus = Fun.flip (-)

module Message_queue = struct
  type timer_handler_data_message_type = Commonp.xcast
  type timer_handler_data = timer_handler_data_message_type * Commons.pubkey * Message.Base.t
  type message_with_timer = Message_with_timer of {
    message: Message.Base.t; timer: timer_handler_data Util_lwt_seaboar.Timer.t;
  }
  type t = (Message.Com.msg_id, message_with_timer) BatMap.t
  let message_of_message_with_timer (Message_with_timer t) = t.message
  let timer_of_message_with_timer (Message_with_timer t) = t.timer
  let num_msgs = BatMap.cardinal
  let pp ppf mq =
    let f m =
      let timer' = timer_of_message_with_timer m in
      let remaining' = Util_lwt_seaboar.Timer.Acc.remaining' timer' in
      let color' = if remaining' < 5 then Util_io.bright_red else Fmt.string in
      Fmt.pf ppf "%a" color' "✉ " in
    BatEnum.iter f (BatMap.values mq)
  let empty = BatMap.empty
  let add msg_id timer message =
    BatMap.add msg_id (Message_with_timer { message; timer })
  let remove_result hashf msg =
    (* --- @todo inefficient to recalculate *)
    let k = Message.Base.message_id hashf msg in
    UBatMap.remove_result k
  let map_messages f = BatList.of_enum % BatEnum.map f % BatMap.values
  let messages' =
    let f (Message_with_timer mwt) = mwt.message in
    map_messages f
  let timers' t =
    let f (Message_with_timer mwt) = mwt.timer in
    map_messages f t
  let messages = map_messages id
  let cancel_timeout = Util_lwt_seaboar.Timer.cancel % timer_of_message_with_timer
  let force_finish_timeout = Util_lwt_seaboar.Timer.force_finish % timer_of_message_with_timer
  let cancel_all_timeouts = List.iter cancel_timeout % messages
  let encode =
    let open Message.Base.Encode in
    let type_enc = function
      | `Uni -> SE.int 0
      | `Multi -> SE.int 1 in
    let timer_handler_data (type', addr, msg) = SE.array [
      type_enc type'; Commons.Encode.pubkey addr; Message.Base.Encode.message msg
    ] in
    let message_with_timer (Message_with_timer mwt) = SE.variant "Message_with_timer" [
      message mwt.message; Util_lwt_seaboar.Timer.encode timer_handler_data mwt.timer;
    ] in table_enc (Message.Com.Encode.msg_id) message_with_timer
  let decode unqueue_message seen_length =
    let open Message.Base.Decode in
    let f message timer = Message_with_timer { message; timer } in
    let type_dec = SD.choice [
      SD.lift (fun _ -> `Uni) (SD.int' 0);
      SD.lift (fun _ -> `Multi) (SD.int' 1);
    ] in
    let timer_handler_data = SD.array_tuple3 type_dec Commons.Decode.pubkey (Message.Base.Decode.message seen_length) in
    let message_with_timer =
      SD.variant_2 "Message_with_timer" f (
      (message seen_length), Util_lwt_seaboar.Timer.decode unqueue_message timer_handler_data
    ) in
    let p = table_dec Message.Com.Decode.msg_id message_with_timer in
    SD.lift UList.to_table p
end

type addr = Commons.pubkey
type routes = Routes_table of (addr, Message_router_route.route) BatMap.t
type multicast = Multicast_table of Commons.multicast_table
type message_queue = Message_queue of (addr, Message_queue.t) BatMap.t
type message_cache = Message_cache of (Message.Com.msg_id, Message.Base.t) CMap.t

(* --- unicast * multicast *)
type stats = Stats of {
  received: int * int;
  (* In the case of unicast, this means the message router processed some
     instruction that was addressed to it.
     For multicast it means the message router published the message (this stat
     gets updated even if there were no subscribers). *)
  processed: int * int;
  queued: int * int;
  relayed: int * int;
  rejected: int * int;
  expired: int * int;
}

type state = State of {
  stats: stats; routes: routes; multicast: multicast;
  message_queue: message_queue; message_cache: message_cache;
}
type t = state

module Acc = struct
  let stats (State s) = s.stats
  let routes (State s) = s.routes
  let multicast (State s) = s.multicast
  let message_queue (State s) = s.message_queue
  let message_cache (State s) = s.message_cache
end

module T = struct
  let state stats routes multicast message_queue message_cache = State {
    stats; routes; multicast; message_queue; message_cache;
  }
  let stats received processed queued relayed rejected expired = Stats {
    received; processed; queued;
    relayed; rejected; expired;
  }
  let routes t = Routes_table t
  let multicast t = Multicast_table t
  let message_queue t = Message_queue t
  let message_queue' = message_queue % BatMap.of_seq % List.to_seq
  let message_cache bq = Message_cache bq
end

module Mk = struct
  let state rcv_tup proc_tup qu_tup rel_tup rej_tup exp_tup routes msgs =
    let stats = T.stats rcv_tup proc_tup qu_tup rel_tup rej_tup exp_tup in
    T.state stats routes msgs
  let message_cache' sz = CMap.empty sz
  let message_cache sz = Message_cache (message_cache' sz)
end

module Pp = struct
  let list fmt' ppf =
    let _sep ppf () = Fmt.pf ppf ",@ " in
    Fmt.pf ppf "@[<v>%a@]" (Fmt.list fmt')
  let batset fmt' ppf = list fmt' ppf % BatSet.to_list
  let pubkey = Commons.Pp.pubkey
  let routing_table ppf (Routes_table table') =
    if BatMap.is_empty table' then Fmt.pf ppf "@[empty@]"
    else
      let () = Fmt.pf ppf "@[<v>" in
      let f k v =
        Fmt.pf ppf "%a -> %a@," pubkey k Message_router_route.pp v in
      let () = table' |> BatMap.iter f in
      Fmt.pf ppf "@]"
  let multicast_table ppf (Multicast_table table') =
    if BatMap.is_empty table' then Fmt.pf ppf "@[empty@]"
    else
      let () = Fmt.pf ppf "@[<v>" in
      let f k v = Fmt.pf ppf "%a ->@,  %a@," Commons.Pp.multicast_group_pubkey k (batset Commons.Pp.multicast_subscriber) v in
      let () = table' |> BatMap.iter f in
      Fmt.pf ppf "@]"
  let stats ppf (Stats st) =
    let uni_rcv, multi_rcv = st.received in
    let uni_pr, multi_pr = st.processed in
    let uni_qu, multi_qu = st.queued in
    let uni_rel, multi_rel = st.relayed in
    let uni_rej, multi_rej = st.rejected in
    let uni_ex, multi_ex = st.expired in
    let if_not_zero ppp pp = function | 0 -> pp | _ -> ppp pp in
    let open Util_io in
    Fmt.pf ppf (
      "@[<v>" ^^
      "%a:   received = %a | processed = %a | queued = %a | relayed = %a | rejected = %a | expired = %a@ " ^^
        "%a: received = %a | published = %a | queued = %a | relayed = %a | rejected = %a | expired = %a@]"
    ) cyan "unicast"
      (if_not_zero pp_green Fmt.int uni_rcv) uni_rcv
      (if_not_zero pp_green Fmt.int uni_pr) uni_pr
      (if_not_zero pp_yellow Fmt.int uni_qu) uni_qu
      (if_not_zero pp_green Fmt.int uni_rel) uni_rel
      (if_not_zero pp_bright_red Fmt.int uni_rej) uni_rej
      (if_not_zero pp_bright_red Fmt.int uni_ex) uni_ex
      magenta "multicast"
      (if_not_zero pp_green Fmt.int multi_rcv) multi_rcv
      (if_not_zero pp_green Fmt.int multi_pr) multi_pr
      (if_not_zero pp_yellow Fmt.int multi_qu) multi_qu
      (if_not_zero pp_green Fmt.int multi_rel) multi_rel
      (if_not_zero pp_bright_red Fmt.int multi_rej) multi_rej
      (if_not_zero pp_bright_red Fmt.int multi_ex) multi_ex
  let message_queue ppf (Message_queue queue') =
    if BatMap.is_empty queue' then Fmt.pf ppf "@[empty@]"
    else
      let () = Fmt.pf ppf "@[<v>" in
      let f k v = Fmt.pf ppf "%a -> %a@," pubkey k (Message_queue.pp) v in
      let () = queue' |> BatMap.iter f in
      Fmt.pf ppf "@]"
  let message_cache' type' ppf (Message_cache cache) =
    if CMap.is_empty cache then Fmt.pf ppf "@[empty@]"
    else
      let pp = match type' with
      | `Short -> CMap.pp_short
      | `Long -> CMap.pp Message.Com.Pp.msg_id in
      Fmt.pf ppf "%a" pp cache
  let t ppf s = Fmt.pf ppf
    "@[<v>%a@,%a@,%a@,%a@,%a@]"
    (Util_io.pp_section stats) ("message stats", Acc.stats s)
    (Util_io.pp_section routing_table) ("routing table", Acc.routes s)
    (Util_io.pp_section multicast_table) ("multicast groups table", Acc.multicast s)
    (Util_io.pp_section message_queue) ("message queue", Acc.message_queue s)
    (Util_io.pp_section (message_cache' `Long)) ("message cache", Acc.message_cache s)
  let t_short ppf s = Fmt.pf ppf
    "@[<v>%a@,%a@,%a@,%a@,%a@]"
    (Util_io.pp_section stats) ("message stats", Acc.stats s)
    (Util_io.pp_section routing_table) ("routing table", Acc.routes s)
    (Util_io.pp_section multicast_table) ("multicast groups table", Acc.multicast s)
    (Util_io.pp_section message_queue) ("message queue", Acc.message_queue s)
    (Util_io.pp_section (message_cache' `Short)) ("message cache", Acc.message_cache s)
end

module Encode = struct
  let list fmt' = SE.list fmt'
  let batset fmt' = SE.list fmt' % BatSet.to_list
  let pubkey = Message.Com.Encode.pubkey
  let routes = function
    | Routes_table table ->
        let e = table_enc pubkey Message_router_route.Encode.t table in
        SE.variant "Routes_table" [e]
  let stats =
    let tuple (e1, e2) (x, y) = SE.array [ e1 x; e2 y ] in
    function
      | Stats { received; processed; queued; relayed; rejected; expired } ->
        SE.variant "Stats" [
          tuple (SE.int, SE.int) received;
          tuple (SE.int, SE.int) processed;
          tuple (SE.int, SE.int) queued;
          tuple (SE.int, SE.int) relayed;
          tuple (SE.int, SE.int) rejected;
          tuple (SE.int, SE.int) expired;
        ]
  let multicast = function
    | Multicast_table table' ->
        let e = table_enc Commons.Encode.multicast_group_pubkey (batset Commons.Encode.multicast_subscriber) table' in
        SE.variant "Multicast_table" [e]
  let message_queue = function
    | Message_queue table' ->
        let e = table_enc pubkey Message_queue.encode table' in
        SE.variant "Message_queue" [e]
  let message_cache = function
    | Message_cache cache ->
        let e = Util_encode_decode.Encode.cmap (Message.Com.Encode.msg_id, Message.Base.Encode.message) cache in
        SE.variant "Message_cache" [e]
  let t = function
    | State { stats = st; routes = rt; multicast = multi; message_queue = queue; message_cache = cache; } ->
        SE.variant "State" [stats st; routes rt; multicast multi; message_queue queue; message_cache cache]
end

module Decode = struct
  let (<$>) = SD.(<$>)
  let batset p = BatSet.of_list <$> SD.array_list p
  let pubkey = Commons.Decode.pubkey
  let routes =
    let q = table_dec pubkey Route.Decode.t in
    let p = SD.lift Util_pure.List.to_table q in
    SD.variant_1 "Routes_table" T.routes p
  let stats =
    let tuple = SD.array_tuple2 SD.int SD.int in
    SD.variant_6 "Stats" T.stats (tuple, tuple, tuple, tuple, tuple, tuple)
  let multicast =
    let q = table_dec Commons.Decode.multicast_group_pubkey (batset Commons.Decode.multicast_subscriber) in
    let p = SD.lift Util_pure.List.to_table q in
    SD.variant_1 "Multicast_table" T.multicast p
  let message_queue unqueue_message seen_length =
    let p = table_dec pubkey (Message_queue.decode unqueue_message seen_length) in
    SD.variant_1 "Message_queue" T.message_queue' p
  let message_cache seen_size cache_size =
    let p = Util_encode_decode.Decode.cmap (Message.Com.Decode.msg_id, Message.Base.Decode.message seen_size) cache_size in
    SD.variant_1 "Message_cache" T.message_cache p
  let t unqueue_message seen_size cache_size = SD.variant_5 "State" T.state (
    stats, routes, multicast, message_queue unqueue_message seen_size, message_cache seen_size cache_size
  )
end

module Pure = struct
  (* --- helper for updating stats *)
  let g t f (u, m) = match t with `Uni -> f u, m | `Multi -> u, f m

  let update_stats f g h i j k = function Stats {
    received; processed; queued; relayed; rejected; expired;
  } -> T.stats (f received) (g processed) (h queued) (i relayed) (j rejected) (k expired)
  let update_num_messages_received t f (State s) =
    let stats' = update_stats (g t f) id id id id id s.stats in
    State { s with stats = stats' }
  let update_num_messages_processed t f (State s) =
    let stats' = update_stats id (g t f) id id id id s.stats in
    State { s with stats = stats' }
  let update_num_messages_queued t f (State s) =
    let stats' = update_stats id id (g t f) id id id s.stats in
    State { s with stats = stats' }
  let update_num_messages_relayed t f (State s) =
    let stats' = update_stats id id id (g t f) id id s.stats in
    State { s with stats = stats' }
  let update_num_messages_rejected t f (State s) =
    let stats' = update_stats id id id id (g t f) id s.stats in
    State { s with stats = stats' }
  let update_num_messages_expired t f (State s) =
    let stats' = update_stats id id id id id (g t f) s.stats in
    State { s with stats = stats' }
  let set_num_messages_queued t n s =
    update_num_messages_queued t (Fun.const n) s
  let update_routes f (State s) =
    let g (Routes_table rt) = Routes_table (f rt) in
    State { s with routes = g s.routes }
  let update_routes' f (State s) =
    let g (Routes_table rt) = f rt |> Result.fold
      ~ok:(fun routes' -> ok @@ State { s with routes = T.routes routes' })
      ~error:error
    in g s.routes
  let update_multicast_groups f (State s) =
    let g (Multicast_table mt) = Multicast_table (f mt) in
    State { s with multicast = g s.multicast }
  let update_message_cache f (State s) =
    let g (Message_cache q) = Message_cache (f q) in
    State { s with message_cache = g s.message_cache }
  let update_connection_for_table addr f rt =
    Util_pure.BatMap.modify_result ~not_found:(`Pp Commons.Pp.pubkey) addr f rt
  let update_message_queue f (State s) =
    let g (Message_queue q) = Message_queue (f q) in
    State { s with message_queue = g s.message_queue }
  let update_message_queue_result f (State s) =
    let open UResult.Letsfix in
    let Message_queue q = s.message_queue in
    let* q' = f q in
    Ok (State { s with message_queue = Message_queue q' })

  let close_connection_for_table addr rt =
    let f = Route.close_connection in
    update_connection_for_table addr f rt

  let close_connection addr =
    update_routes' @@ close_connection_for_table addr

  let add_or_replace_route addr route =
    let f = BatMap.add addr route in
    update_routes f

  let clear_routing_table = update_routes (const BatMap.empty)

  let get_route addr (State state) =
    let Routes_table rt = state.routes in
    BatMap.find_opt addr rt

  let get_connection addr st =
    let (>>=) = Option.bind in
    get_route addr st >>= Route.connection_of_route

  let get_conduit addr st =
    let (<$>) = Option.map in
    Route.conduit_of_connection <$> get_connection addr st

  let get_multicast_table (State st) = st.multicast

  (* --- note that `modify_def` will first apply the default, then transform it (which is what we want) *)

  (** Will create the group if it doesn't exist. *)
  let add_to_multicast_group group requestor =
    let f = BatMap.modify_def BatSet.empty group (BatSet.add requestor) in
    update_multicast_groups f

  let remove_from_multicast_group group requestor =
    let g = function
      (* --- @todo deal with missing *)
      | None -> None
      | Some set -> set |> BatSet.remove requestor |> function
        | x when x = BatSet.empty -> None
        | x -> Some x in
    let f = BatMap.modify_opt group g in
    update_multicast_groups f

  let clear_multicast_table = update_multicast_groups (const BatMap.empty)

  let get_subscribers_for_multicast_group group st =
    let Multicast_table table' = get_multicast_table st in
    BatMap.find_opt group table'

  let get_groups_from_multicast_table st =
    let Multicast_table table' = get_multicast_table st in
    table' |> Util_pure.List.of_table |> List.map (fun (k, _v) -> k)

  let queue_message addr msg_id timer msg =
    (* @todo why does modify_def call the update function twice? *)
    let f =
      BatMap.modify_def Message_queue.empty addr (Message_queue.add msg_id timer msg) in
    update_message_queue f

  let unqueue_message addr hashf msg =
    let open UResult.Letsfix in
    let err = Util_io.fmt_out "no stored messages for addr %a" Commons.Pp.pubkey addr in
    let update state =
      let f = Message_queue.remove_result hashf msg in
      let* state' = UBatMap.modify_result' ~not_found:(`Err err) addr f state in
      let state'' =
        let queue' = BatMap.find addr state' in
        match Message_queue.num_msgs queue' with
        (* --- remove entry if no messages left *)
        | 0 -> BatMap.remove addr state'
        | _ -> state' in
      Ok state''
    in update_message_queue_result update

  let get_all_queued_messages (State st) =
    let (Message_queue queue') = st.message_queue in
    let flatmap f = BatEnum.(flatten % map f) in
    flatmap (BatMap.values) (BatMap.values queue')

  let get_and_clear_queued_messages addr (State st) =
    let (>>|) a b = Option.map b a in
    let module UBatMap = Util_pure.BatMap in
    let Message_queue table = st.message_queue in
    UBatMap.extract_opt addr table >>| fun (queue', table') ->
      let () = Message_queue.cancel_all_timeouts queue' in
      Message_queue.messages' queue',
      State { st with message_queue = Message_queue table' }

  (** Note that this causes the expired stat to increase. *)
  let clear_message_queue_force_finish =
    BatEnum.iter Message_queue.force_finish_timeout % get_all_queued_messages

  let clear_message_queue_cancel =
    BatEnum.iter Message_queue.cancel_timeout % get_all_queued_messages

  (** Clears the table but not the timeouts -- you must be sure to clear these
     another way. *)
  let clear_message_queue_structure (State st) =
    State { st with message_queue = Message_queue BatMap.empty }

  let get_queued_messages addr (State st) =
    let Message_queue table = st.message_queue in
    match BatMap.find_opt addr table with
    | None -> []
    | Some xs -> xs |> Message_queue.messages

  let add_to_message_cache msg_id msg = update_message_cache (CMap.add msg_id msg)
  let get_message_from_cache msg_id (State st) =
    let Message_cache cache' = st.message_cache in
    CMap.get msg_id cache'
  let clear_message_cache =
    let f cmap = let size = CMap.size cmap in CMap.empty size in
    update_message_cache f

  let serialize (cnf: Message_router_config.t) =
    Util_encode_decode_io.encode_and_save Encode.t cnf.serialize_state_path

  let deserialize unqueue_message seen_size cache_size =
    Result.fold ~ok:lwt_ok ~error:lwt_error %
    SD.run_parser_string (Decode.t unqueue_message seen_size cache_size)
end

module Mut = struct
  let cur = ref @@ Mk.state
    (0, 0) (0, 0) (0, 0) (0, 0) (0, 0) (0, 0)
    (T.routes BatMap.empty) (T.multicast BatMap.empty)
    (T.message_queue BatMap.empty) (Mk.message_cache 1)
  let set' = (:=) cur
  let get' () = !cur
  let get f = f (get' ())
  let update f = let x = get f in set' (get f); x
  (* --- update, where f returns a `result` and we only continue in the Ok case. *)
  let update_result f = Result.map set' % f @@ get' ()

  let update_routes = update % Pure.update_routes
  let update_multicast_groups = update % Pure.update_multicast_groups
  let update_message_cache = update % Pure.update_message_cache

  let set_num_messages_queued typ n = update @@ Pure.set_num_messages_queued typ n
  let update_num_messages_received typ f = update @@ Pure.update_num_messages_received typ f
  let update_num_messages_processed typ f = update @@ Pure.update_num_messages_processed typ f
  let update_num_messages_queued typ f = update @@ Pure.update_num_messages_queued typ f
  let update_num_messages_relayed typ f = update @@ Pure.update_num_messages_relayed typ f
  let update_num_messages_rejected typ f = update @@ Pure.update_num_messages_rejected typ f
  let update_num_messages_expired typ f = update @@ Pure.update_num_messages_expired typ f

  let add_or_replace_route addr route = update @@ Pure.add_or_replace_route addr route
  let clear_routing_table () = update @@ Pure.clear_routing_table
  let clear_multicast_table () = update @@ Pure.clear_multicast_table

  let add_to_multicast_group group requestor = update @@ Pure.add_to_multicast_group group requestor
  let remove_from_multicast_group group requestor = update @@ Pure.remove_from_multicast_group group requestor

  let close_connection addr = update_result @@ Pure.close_connection addr

  let get_subscribers_for_multicast_group = get % Pure.get_subscribers_for_multicast_group

  let get_groups_from_multicast_table () = get Pure.get_groups_from_multicast_table

  let queue_message addr msg_id timer msg = update @@ Pure.queue_message addr msg_id timer msg
  let unqueue_message addr hashf msg = update_result @@ Pure.unqueue_message addr hashf msg
  let get_all_queued_messages () = get @@ Pure.get_all_queued_messages
  (** This form is actually not that useful: we only want to clear the queue when
     we're sure the messages have been relayed. *)
  let get_and_clear_queued_messages addr' =
    let (<$>) = Option.map in
    let f (msgs', state') = let () = set' state' in msgs' in
    let pure' = get % Pure.get_and_clear_queued_messages in
    f <$> pure' addr'
  let get_and_delay_clear_queued_messages addr' =
    let msgs' = get @@ Pure.get_queued_messages addr' in
    let f () = List.iter Message_queue.force_finish_timeout msgs' in
    msgs', f

  let cancel_all_msg_timeouts () =
    BatEnum.iter Message_queue.cancel_timeout (get_all_queued_messages ())
  let clear_message_queue_force_finish () = get Pure.clear_message_queue_force_finish
  (** An alternate implementation -- doing it with 'force_finish' has a small
     lag, while this is immediate. Also force_finish causes the 'expired' stat
     to increase. *)
  let clear_message_queue_cancel () =
    let () = get Pure.clear_message_queue_cancel
    in ignore @@ update Pure.clear_message_queue_structure
  let clear_message_cache () = ignore @@ update Pure.clear_message_cache

  let add_to_message_cache msg_id msg = update (Pure.add_to_message_cache msg_id msg)
  let get_message_from_cache msg_id = get (Pure.get_message_from_cache msg_id)

  let serialize config = get (Pure.serialize config)
  let deserialize_and_set unqueue_message seen_size cache_size str' =
    let () = cancel_all_msg_timeouts () in
    Pure.deserialize unqueue_message seen_size cache_size str' >>= function
    | Ok state' -> Lwt.return @@ set' state'
    | Error e -> warn "Error deserializing, aborting set state: %s" e

  let pp' type' ppf _ =
    let ppt = match type' with `Short -> Pp.t_short | `Long -> Pp.t in
    Fmt.pf ppf "@[%a@]" ppt @@ get' ()
end

let pp = Pp.t

let init (cnf: Message_router_config.t) =
  let do_cache () = Mut.update_message_cache @@
    Fun.const (Mk.message_cache' cnf.cache_size) in
  let do_routing () =
    let f (route': Route.t) =
      let addr' = Route.route_addr route'
      in Mut.add_or_replace_route addr' route' in
    let g = ignore % f in
    List.iter g cnf.routing_table_init in
  let do_multicast () =
    Mut.update_multicast_groups (fun _ -> cnf.multicast_table_init) in
  ignore (do_cache ()); ignore (do_routing ()); ignore (do_multicast ())

(* @todo what would be a more generic name? *)
let update_routes_on_peer_advertisement addr ip_port =
  let route' = Route.Mk.remote_router_known_nc addr ip_port in
  ignore @@ Mut.add_or_replace_route addr route'

let update_routes_incoming addr connection typ =
  let route' = match typ with
  | `Local -> Route.Mk.local_service_c addr connection
  | `Remote -> Route.Mk.remote_router_incoming addr connection in
  ignore @@ Mut.add_or_replace_route addr route'

let update_routes_incoming' addr conduit_connection typ =
  let route_conn' = Route.Mk.connection_incoming conduit_connection in
  update_routes_incoming addr route_conn' typ

let update_routes_outgoing addr ip_port connection =
  let route' = Route.Mk.remote_router_outgoing addr ip_port connection in
  ignore @@ Mut.add_or_replace_route addr route'

let update_routes_outgoing' addr ip_port conduit_connection =
  let route_conn' = Message_router_route.Mk.connection_outgoing conduit_connection in
  update_routes_outgoing addr ip_port route_conn'

let add_to_multicast_group group requestor =
  ignore @@ Mut.add_to_multicast_group group requestor

let add_to_multicast_group' group requestor =
  add_to_multicast_group group (Commons.Mk.multicast_subscriber requestor)

let remove_from_multicast_group group requestor =
  ignore @@ Mut.remove_from_multicast_group group requestor

let remove_from_multicast_group' group requestor =
  remove_from_multicast_group group (Commons.Mk.multicast_subscriber requestor)

(** Error means that no entry was found with this addr. If there is one, this
    always succeeds (the underlying call to close the connection is not called
    here and must be called separately). *)
let close_connection = Mut.close_connection

let serialize = Mut.serialize
let deserialize_and_set = Mut.deserialize_and_set

let get_connection addr = Pure.get_connection addr (Mut.get' ())
let get_conduit addr = Pure.get_conduit addr (Mut.get' ())

let got_message_received type' = ignore @@
  Mut.update_num_messages_received type' @@ (+) 1
let got_message_processed type' = ignore @@
  Mut.update_num_messages_processed type' @@ (+) 1
let got_message_queued type' = ignore @@
  Mut.update_num_messages_queued type' @@ (+) 1
let got_message_relayed type' = ignore @@
  Mut.update_num_messages_relayed type' @@ (+) 1
let got_message_rejected type' = ignore @@
  Mut.update_num_messages_rejected type' @@ (+) 1
let got_message_expired type' = ignore @@
  Mut.update_num_messages_expired type' @@ (+) 1
let ungot_message_queued type' = ignore @@
  Mut.update_num_messages_queued type' @@ minus 1

let get_subscribers_for_multicast_group = Mut.get_subscribers_for_multicast_group
let get_groups_from_multicast_table = Mut.get_groups_from_multicast_table

let get_and_delay_clear_queued_messages = Mut.get_and_delay_clear_queued_messages
let queue_message addr msg_id timer msg = ignore @@ Mut.queue_message addr msg_id timer msg
let unqueue_message addr hashf msg = Mut.unqueue_message addr hashf msg
let reset_message_queue () =
  let () = Mut.clear_message_queue_cancel () in
  [ `Uni; `Multi ] |> List.iter (
    fun t -> ignore @@ Mut.set_num_messages_queued t 0
  )
let reset_message_cache = Mut.clear_message_cache

let clear_routing_table () = ignore @@ Mut.clear_routing_table ()
let clear_multicast_table () = ignore @@ Mut.clear_multicast_table ()

let unqueue_message_with_stats ~expired hashf (type', addr, msg) =
  let () = ungot_message_queued type' in
  let () = if expired then got_message_expired type' in
  unqueue_message addr hashf msg

(* This resolves the result match and just prints straight to stderr on error.
   Because it's often run asynchronously it's difficult to try and pattern match
   the result otherwise. *)
let unqueue_message_with_stats' ~expired hashf (type', addr, msg) =
  match unqueue_message_with_stats ~expired hashf (type', addr, msg) with
  | Ok () -> ()
  | Error (`Msg e) -> Fmt.epr "unable to unqueue message: %s@." e

let set_unqueue_timer secs hashf msg (type', addr) =
  let data' = type', addr, msg in
  let f ~expired _ = unqueue_message_with_stats' ~expired hashf data' in
  Util_lwt_seaboar.Timer.mk f data' secs

let add_to_message_cache msg_id msg = ignore (Mut.add_to_message_cache msg_id msg)
let get_message_from_cache = Mut.get_message_from_cache
let has_message_in_cache = Option.is_some % get_message_from_cache
