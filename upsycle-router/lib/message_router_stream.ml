open! Basic

type stream_msg' = {
  info: Util_conduit.info;
  (** The public key of the message router as determined from its configuration. *)
  ourkey: Commons.pubkey;
  peerkey: Commons.pubkey;
  message: Message.Base.t;
}

type stream_msg =
  | Stream_msg of {
    connection: Message_router_route.connection;
    stream_msg: stream_msg'
  }
  (** Used for sending e.g. mock messages using the keyboard *)
  | Test_stream_msg of {
    stream_msg: stream_msg'
  }

type t = stream_msg

let stream_msg connection info ourkey peerkey message =
  let stream_msg = { info; ourkey; peerkey; message } in
  Stream_msg { connection; stream_msg }

let test_stream_msg info ourkey peerkey message =
  let stream_msg = { info; ourkey; peerkey; message } in
  Test_stream_msg { stream_msg }

let fold_msg f g = function
  | Stream_msg { connection; stream_msg = m } -> f connection m
  | Test_stream_msg { stream_msg = m } -> g m

let fold_msg' f g =
  let f' c m = f c m.info m.ourkey m.peerkey m.message in
  let g' m = g m.info m.ourkey m.peerkey m.message in
  fold_msg f' g'

module Acc = struct
  let stream_msg = let f _ m = m in let g m = m in fold_msg f g
  let peerkey = let f _ _ _ p _ = p in let g _ _ p _ = p in fold_msg' f g
  let info = stream_msg %> fun m -> m.info
  let connection = let f c _ = Some c in let g _ = None in fold_msg f g
  let message = let f _ m = m.message in let g m = m.message in fold_msg f g
end

let stream_read_thread cnf info handle_msg stream =
  let f () =
    let* stream_msg' = Lwt_stream.next stream in
    let fail' pp e = Util_lwt.warn "stream_read_thread: handle message failed (%a)@." pp e in
    try%lwt handle_msg cnf info stream_msg' with
    | Failure e -> fail' Fmt.string e
    | e -> fail' Fmt.exn e
  in Util_lwt.loop f
