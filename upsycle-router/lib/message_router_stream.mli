type pubkey := Commons.pubkey

type stream_msg' = {
  info: Util_conduit.info;
  (** The public key of the message router as determined from its configuration. *)
  ourkey: pubkey;
  peerkey: pubkey;
  message: Message.Base.t;
}

type stream_msg =
  | Stream_msg of {
    connection: Message_router_route.connection;
    stream_msg: stream_msg'
  }
  (** Used for sending e.g. mock messages using the keyboard *)
  | Test_stream_msg of {
    stream_msg: stream_msg'
  }

type t = stream_msg

val stream_msg :
  Message_router_route.connection ->
  Util_conduit.info ->
  pubkey ->
  pubkey ->
  Message.Base.t ->
  stream_msg

val test_stream_msg :
  Util_conduit.info ->
  pubkey ->
  pubkey ->
  Message.Base.t ->
  stream_msg

val fold_msg : (Message_router_route.connection -> stream_msg' -> 'a) -> (stream_msg' -> 'a) -> t -> 'a
val fold_msg' : (Message_router_route.connection -> string -> pubkey -> pubkey -> Message.Base.t -> 'a) -> (string -> pubkey -> pubkey -> Message.Base.t -> 'a) -> t -> 'a

module Acc : sig
  val stream_msg : t -> stream_msg'
  val peerkey : t -> pubkey
  val info : t -> Util_conduit.info
  val connection : t -> Message_router_route.connection option
  val message : t -> Message.Base.t
end

val stream_read_thread : Message_router_config.t -> string -> (Message_router_config.t -> string -> stream_msg -> unit Lwt.t) -> stream_msg Lwt_stream.t -> unit Lwt.t
