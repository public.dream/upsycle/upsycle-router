open! Basic

module SD = Seaboar.Decode

module Config = Service_config
module Crypto = Commons.Crypto
module Watch = Util_lwt.Watch

module UBatMap = Util_pure.BatMap
module UResult = Util_pure.Result
module ULwt = Util_lwt

type watchers = [ `Join_ack of Commons.pubkey | `Leave_ack of Commons.pubkey ]

(** The state is completely immutable and the calling functions need to keep
    track of it (generally by passing around a `state_r` reference), unlike
    with message_router_state.ml, where one mutable reference is managed by the
    module itself. *)

module State = struct
  type multicast_group = Commons.multicast_group
  type multicast_group_info = Commons.multicast_group * Message.Com.seen
  type multicast_table = (Commons.pubkey, multicast_group_info) BatMap.t
  type state = State of {
    seen_size: int;
    multicast_table: multicast_table;
    watchers: watchers Watch.t;
  }
  module T = struct
    let state seen_size multicast_table watchers = State { seen_size; multicast_table; watchers }
  end
  module Mk = struct
    let t len =
      let watchers' = Watch.mk () in
      T.state len BatMap.empty watchers'
    let entry num_seen multicast_group = multicast_group, Message.Com.Mk.seen num_seen
  end
  module Acc = struct
    let seen_size (State s) = s.seen_size
    let watchers (State s) = s.watchers
    let multicast_table (State s) = s.multicast_table
    let multicast_table_keys = UBatMap.keys_list % multicast_table
    let multicast_group_info = UBatMap.values_list % multicast_table
    let multicast_pubkeys = List.map fst % multicast_group_info
    let multicast_seens = List.map snd % multicast_group_info
    let seen pubkey = Option.map snd % BatMap.find_opt pubkey % multicast_table
  end
  module Pp = struct
    let multicast ~type' ppf table' =
      if BatMap.is_empty table' then Fmt.pf ppf "@[empty@]"
      else
        let () = Fmt.pf ppf "@[<v>" in
        let f _ (group', seen') = Fmt.pf ppf "٭ %a ->@,  %a@,"
          Commons.Pp.multicast_group group' (Message.Com.Pp.seen' type') seen' in
        let () = table' |> BatMap.iter f in
        Fmt.pf ppf "@]"

    let multicast_table type' ppf table' =
      if BatMap.is_empty table' then Fmt.pf ppf "@[empty@]"
      else
        let () = Fmt.pf ppf "@[<v>" in
        let f _ (group', seen') = Fmt.pf ppf "٭ %a ->@,  %a@,"
          Commons.Pp.multicast_group group' (Message.Com.Pp.seen' type') seen' in
        let () = table' |> BatMap.iter f in
        Fmt.pf ppf "@]"

    let t' type' ppf s =
      Fmt.pf ppf
      "@[<v>%a@]"
      (Util_io.pp_section (multicast_table type')) ("multicast groups table", Acc.multicast_table s)
    let t = t' `Long
  end
  let update_watchers f (State st) =
    State { st with watchers = f st.watchers }
  let set_multicast_table t (State st) =
    State { st with multicast_table = t }
  let set_multicast_table' = Fun.flip set_multicast_table
  let update_multicast_table f (State st) =
    set_multicast_table (f st.multicast_table) (State st)
  let update_multicast_table_result f (State st) =
    f st.multicast_table |> UResult.rmap
      ~decorate_error:(`S "update_multicast_table_result")
      (set_multicast_table' (State st))
  let set_watchers w = update_watchers (Fun.const w)
  let add_multicast_group mg st =
    let group_info' = Mk.entry (Acc.seen_size st) mg in
    let f = BatMap.add (Commons.pubkey_of_multicast_group mg) group_info' in
    update_multicast_table f st
  let set_multicast_table tbl = update_multicast_table (Fun.const tbl)
  let add_multicast_groups = List.fold_right add_multicast_group
  let clear_multicast_groups = let f = const BatMap.empty in update_multicast_table f
  let set_multicast_groups grps = add_multicast_groups grps % clear_multicast_groups
  let add_seen pubkey msg_id =
    let g (mg, seen) = mg, Message.Com.add_seen msg_id seen in
    let f tb = UBatMap.modify_result pubkey g tb in
    update_multicast_table_result f
    %> UResult.rdecorate_error (`S "add_seen: Multicast group doesn't exist")
end

let watch state_r type' =
  let watchers = State.Acc.watchers !state_r in
  let watch', watchers' = Watch.wait type' watchers in
  state_r := State.set_watchers watchers' !state_r; watch'

let connect_to_message_router (cnf: Config.t) =
  let* () = info "our pubkey %a" Commons.Pp.pubkey cnf.our_pubkey in
  let* () = info "connecting to message router" in
  let service_name = Some ("mock-service " ^ cnf.service_name) in
  let info_str = (Util_io.fmt_out "[service %a-> message router %a]"
      Util_io.yellow
      (Option.fold ~none:"" ~some:(Fun.flip (^) " ") service_name)
      Commons.Pp.pubkey cnf.message_router_pubkey) in
  let ip = Commons.Acc.ip cnf.message_router_ip in
  let port = cnf.message_router_port in
  Message_router_client.connect
    ~hostname:cnf.message_router_hostname
    ~info_str
    ~ip
    ~key_path:cnf.key_path
    ~cert_path:cnf.cert_path
    ~port
    ~pubkey:cnf.message_router_pubkey
    `Service
    |> Util_lwt.decorate_rejection "Unable to connect to message router"

let handle_ack mk_watched type_s state_r info fold msg =
  let watchers = State.Acc.watchers !state_r in
  let prf fmt = Util_lwt.mk_prf info fmt in
  let err' = Util_lwt.warn
    "Error on notify: %s message was unexpected: no watchers for key %a" type_s in
  let f group_addr' result' =
    let* () = prf "got %s for topic %a" type_s Commons.Pp.pubkey group_addr' in
    let ok () = begin match Watch.notify (mk_watched group_addr') watchers with
    | `Ok _ -> Lwt.return ()
    | `No_watchers _ -> err' Commons.Pp.pubkey group_addr' end in
    let nok () = failwith @@ Util_io.fmt_out "%s returned not ok" type_s in
    Message.Com.fold_result ok nok result' in
  fold f msg

(** `msg_str` is the body of the unicast message, i.e. the encoded encapsulated message. *)
let handle_unicast state_r info msg_str =
  let _prf fmt = Util_lwt.mk_prf info fmt in
  let parser' = SD.choice ~failure_msg:"handle_unicast" [
    SD.lift (fun ja -> `Join_ack ja) Message.Message_router.Decode.join_ack;
    SD.lift (fun la -> `Leave_ack la) Message.Message_router.Decode.leave_ack;
  ] in
  let* () = match Util_encode_decode.parse_string parser' msg_str with
  | Ok (`Join_ack msg) -> handle_ack
    (fun group_addr' -> `Join_ack group_addr')
    "JOIN_ACK" state_r info Message.Message_router.join_ack_fold msg
  | Ok (`Leave_ack msg) -> handle_ack
    (fun group_addr' -> `Leave_ack group_addr')
    "LEAVE_ACK" state_r info Message.Message_router.leave_ack_fold msg
  | Error e -> warn "Error: got unexpected message (not JOIN_ACK or LEAVE_ACK): %s" e in
  (* --- keep reading stream *)
  Lwt.return true

let warnm (`Msg m) = warn "%s" m

let handle_multicast (cnf: Config.t) state_r info msg =
  let prf fmt = Util_lwt.mk_prf info fmt in
  Message.Base.fold_flip_multi_unwrap' msg @@ fun group body _ _ ->
    let* () = prf "got multicast message for group %a with body: %a"
      Commons.Pp.multicast_group_pubkey group
      Message.Base.Pp.body' (Body body) in
  let msg_str' = Message.Base.Acc.body' msg in
  let msg_id' = Message.Base.message_id cnf.msg_id_hash_function msg in
  let mg = Message.Base.get_multi_group' msg in
  let* () =
    let ok state' = Lwt.return (state_r := state') in
    State.add_seen mg msg_id' !state_r
    |> UResult.rdecorate_error
      (`S "Got a message for a group we're not subscribed to (probably the message router is misbehaving)")
    |> UResult.fold ok warnm in
  let _ = msg_str' in let _ = info in
  Lwt.return true

let read_thread (cnf: Config.t) state_r msg_router_connection =
  let info = Util_conduit.Connection.Acc.info_str msg_router_connection in
  let prf fmt = Util_lwt.mk_prf info fmt in
  let onmsg' msg =
    if Message.Base.is_unicast msg then
      let msg' = Message.Base.Acc.body' msg in
      handle_unicast state_r info msg'
    else handle_multicast cnf state_r info msg in
  let onbye' () =
    let* () = prf "they said bye" in
    Lwt.return false in
  let onerr' err =
    let* () = prf "error: %s" err in
    Lwt.return `Fail in
  let onparse_err' err = prf "parse error: %s" err in
  Commonp.parse_conduit_stream (Message.Base.Decode.message cnf.seen_size) msg_router_connection onmsg' onbye' onerr' onparse_err'

let start ?(threads=[]) (cnf: Config.t) =
  let* () = ULwt.wanneer cnf.key_printing_short @@
    (fun _ -> info "public keys being shown abridged, see key-printing-short option") in
  let () = Util_crypto_io.set_key_printing_short cnf.key_printing_short in
  let* msg_router_connection = connect_to_message_router cnf in
  let state' = ref (State.Mk.t cnf.seen_size) in
  let threads' =
    let f g = g cnf state' msg_router_connection in
    List.map f (threads @ [ read_thread ]) in
  Lwt.join threads'

let send_join_leave_to_message_router
    type' ~ttl ~exp ?(prepend="")
    (cnf: Config.t) msg_router_connection multicast_group =
  let prf fmt = Util_conduit.mk_prf ~prepend msg_router_connection fmt in
  let info_s', send' = match type' with
  | `Join -> "join", Message_router_client.send_join_to_message_router
  | `Leave -> "leave", Message_router_client.send_leave_to_message_router in
  let* () = prf "sending %s request for multicast group %a" info_s'
    Commons.Pp.pubkey multicast_group in
  send' ~ttl ~exp msg_router_connection cnf.our_privkey cnf.our_pubkey cnf.message_router_pubkey multicast_group
  |> Util_lwt.decorate_rejection @@ Util_io.fmt_out "Unable to send %s message" info_s'

let send_join_to_message_router ~ttl ~exp = send_join_leave_to_message_router `Join ~ttl ~exp
let send_leave_to_message_router ~ttl ~exp = send_join_leave_to_message_router `Leave ~ttl ~exp

let send_pull_to_message_router
    ~ttl ~exp ?(prepend="")
    (cnf: Config.t) msg_router_connection multicast_group msg_id =
  let prf fmt = Util_conduit.mk_prf ~prepend msg_router_connection fmt in
  let* () = prf "sending PULL request for multicast group = %a message id = %a"
    Commons.Pp.pubkey multicast_group
    (Util_io.pp_blue Message.Com.Pp.msg_id) msg_id in
  Message_router_client.send_pull_to_message_router
    ~ttl ~exp msg_router_connection
    cnf.our_privkey cnf.our_pubkey cnf.message_router_pubkey multicast_group msg_id
  |> Util_lwt.decorate_rejection "Unable to send PULL message"

(* --- @enhancement allow partial fail (currently we fail if any one fails) *)
let join_or_leave_groups type' ?prepend ~ttl ~exp (cnf: Config.t) state_r msg_router_connection groups =
  match List.length groups with
  | 0 -> Lwt.return `No_groups
  | num_groups' -> begin
    let set', type_s', send', mk_watched' = match type' with
    | `Join_ack -> groups, "JOIN_ACK", send_join_to_message_router, fun group -> `Join_ack group
    | `Leave_ack -> [], "LEAVE_ACK", send_leave_to_message_router, fun group -> `Leave_ack group in
    let f mg = fun _ ->
      let group = Commons.pubkey_of_multicast_group mg in
      let* () = send' ?prepend ~ttl ~exp cnf msg_router_connection group in
      let s = Util_io.fmt_out "%s %a" type_s' Commons.Pp.pubkey group in
      Util_lwt.timeout' s cnf.timeout_join_leave_ack @@ watch state_r (mk_watched' group) in
    (* We always do it in parallel. For testing/demo we can set the message router
       to sleep before sending join_ack/leave_ack messages, and that makes it
       sequential anyway. *)
    let aggreggator' = Util_lwt.aggregator `Parallel in
    let* () = aggreggator' % List.map f @@ groups in
    let () = state_r := State.set_multicast_groups set' !state_r in
    Lwt.return (`Ok num_groups')
  end

let join_groups ?prepend ~ttl ~exp cnf state_r msg_router_connection groups =
  join_or_leave_groups `Join_ack ?prepend ~ttl ~exp cnf state_r msg_router_connection groups

let leave_all_groups ?prepend ~ttl ~exp (cnf: Config.t) state_r msg_router_connection =
  let groups' = State.Acc.multicast_pubkeys !state_r in
  join_or_leave_groups `Leave_ack ?prepend ~ttl ~exp cnf state_r msg_router_connection groups'

(** This currently sends a fixed test string + random float as the message. *)
let publish_to_subscribed_groups ?prepend ~ttl ~exp _cnf state_r msg_router_connection =
  let groups = State.Acc.multicast_pubkeys !state_r in
  let seen_of_pub pub = match State.Acc.seen pub !state_r with
  | None -> failwith "Internal error: multicast table is corrupted"
  | Some seen' -> seen' in
  let msg' = "some-data-update-message" ^ string_of_float (Random.float 1e10) in
  Message_router_client.publish_to_groups ?prepend ~ttl ~exp groups seen_of_pub msg_router_connection msg'

(** This is for testing only -- in a real situation it wouldn't be useful. We
    take the first message id in the seen queue of each multicast group we're
    subscribed to and send a PULL to the message router to re-request it. *)

let pull_seen ?prepend ~ttl ~exp (cnf: Config.t) state_r msg_router_connection =
  let _prf fmt = Util_conduit.mk_prf ?prepend msg_router_connection fmt in
  let groups' = State.Acc.multicast_group_info !state_r in
  let pull' (mg, seen) = match Message.Com.num_seen seen with
  | 0 -> Lwt.return `No_seen
  | _ ->
      let msg_id' = seen |> Message.Com.fold_seen' List.hd in
      let pubkey' = Commons.pubkey_of_multicast_group mg in
      let* () = send_pull_to_message_router
        ?prepend ~ttl ~exp cnf msg_router_connection pubkey' msg_id' in
      Lwt.return `Ok in
  let* res = Lwt.all (List.map pull' groups') in
  let g sum = function
  | `Ok -> sum + 1
  | `No_seen -> sum in
  match List.fold_left g 0 res with
  | 0 -> Lwt.return `No_seen
  | n -> Lwt.return (`Ok n)

let show_state (cnf: Config.t) state_r info =
  let prf fmt = Util_lwt.mk_prf info fmt in
  let type' = match cnf.print_messages_on_console with
  | `None -> `Short
  | `Short -> `Short
  | `Long -> `Long in
  prf "current state: @[%a@]" (State.Pp.t' type') !state_r

let clear_multicast_table state_r =
  state_r := State.clear_multicast_groups !state_r
