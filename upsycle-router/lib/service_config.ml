module UPure = Util_pure

type pubkey = Commons.pubkey
type privkey = Commons.privkey

type config = {
  service_name: string;
  key_path: string;
  cert_path: string;
  our_pubkey: pubkey;
  our_privkey: privkey;
  message_router_pubkey: pubkey;
  message_router_port: Commons.port;
  message_router_hostname: string;
  message_router_ip: Commons.ip;
  known_services: pubkey list;
  msg_id_hash_function: Commons.msg_id_hash_function;
  multicast_groups: Commons.multicast_group list;
  timeout_join_leave_ack: float option;
  seen_size: int;
  print_messages_on_console: Commons.print_messages_on_console;
  key_printing_short: bool;
}

let mk ~base_dir
  openssl_cmd
  service_name
  key_path
  cert_path
  message_router_pubkey
  message_router_port
  message_router_hostname
  message_router_ip
  known_services
  msg_id_hash_function
  multicast_groups
  timeout_join_leave_ack
  seen_size
  print_messages_on_console
  key_printing_short =
    let mk_abs = UPure.mk_absolute_path base_dir in
    let mk_key = Commons.Crypto.pubkey_of_keystring_or_file_exn ~base_dir openssl_cmd in
    let key_path' = mk_abs key_path in {
      service_name;
      key_path = key_path';
      cert_path = mk_abs cert_path;
      our_pubkey = Commons.Crypto.pubkey_of_keyfile_exn openssl_cmd key_path';
      our_privkey = Commons.Crypto.privkey_of_keyfile_exn openssl_cmd key_path';
      message_router_pubkey = mk_key message_router_pubkey;
      message_router_port;
      message_router_hostname;
      message_router_ip;
      known_services = List.map mk_key known_services;
      msg_id_hash_function;
      multicast_groups;
      timeout_join_leave_ack;
      seen_size;
      print_messages_on_console;
      key_printing_short;
    }

type t = config
