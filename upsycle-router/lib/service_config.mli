type pubkey := Commons.pubkey
type privkey := Commons.privkey
(* private so that it can't be constructed from outside, but dot syntax still
   works .*)
type config = private {
  service_name : string;
  key_path : string;
  cert_path : string;
  our_pubkey : pubkey;
  our_privkey : privkey;
  message_router_pubkey : pubkey;
  message_router_port : Commons.port;
  message_router_hostname : string;
  message_router_ip : Commons.ip;
  known_services: pubkey list;
  msg_id_hash_function : Commons.msg_id_hash_function;
  multicast_groups : Commons.multicast_group list;
  timeout_join_leave_ack : float option;
  seen_size : int;
  print_messages_on_console :
    Commons.print_messages_on_console;
  key_printing_short: bool;
}
val mk :
  base_dir:string ->
  Commons.command ->
  string ->
  string ->
  string ->
  string ->
  Commons.port ->
  string ->
  Commons.ip ->
  string list ->
  Commons.msg_id_hash_function ->
  Commons.multicast_group list ->
  float option ->
  int -> Commons.print_messages_on_console -> bool -> config
type t = config
