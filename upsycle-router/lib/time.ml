module type Time_in = sig
  val time : unit -> float
end

module type Time = sig
  val epoch : int
  val now : unit -> int
  val now_f : unit -> float
  val to_rel : int -> int
end

module Time_unix_in : Time_in = struct
  let time = Unix.gettimeofday
end

module Time_upsycle (Time: Time_in) : Time = struct
  (** 2020-01-01 00:00:00 UTC expressed as a standard epoch *)
  (* node -e "console.log (Number (new Date ('2020-01-01 00:00:00 UTC')) / 1000)" *)
  let epoch = 1577836800
  let now () = int_of_float (Time.time ()) - epoch
  let now_f () = Time.time () -. float_of_int epoch
  let to_rel secs' = secs' - now ()
end

module Time_upsycle_unix : Time = Time_upsycle (Time_unix_in)
