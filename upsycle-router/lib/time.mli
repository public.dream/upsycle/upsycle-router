module type Time_in = sig val time : unit -> float end
module Time_unix_in : Time_in
module type Time = sig
  val epoch : int
  val now : unit -> int
  val now_f : unit -> float
  val to_rel : int -> int
end
module Time_upsycle (Time : Time_in) : Time
module Time_upsycle_unix : Time
