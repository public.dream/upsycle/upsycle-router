open! Basic

(* Wrapping the modules like this lets us selectively hide more of the
   implementation instead of exporting the whole module to the outside. But it
   also means more repetition in the .mli file. *)

module Message_router_config = struct
  include Message_router_config
end
module Service_config = struct
  include Service_config
end

(* This little wrapping trick generates slightly nicer documentation with odoc
   (for example instead of Upsycle_router__.Commons it just says Commons. *)
module Commons = struct
  include Commons
end
include Commons

(* Re-exported types from modules other than Commons. *)

type message_router_route = Message_router_route.t

let init_crypto () = Util_crypto_io.init ()

let start_message_router = Message_router.start

let serialize_message_router_state = Message_router_state.serialize
let deserialize_and_set_message_router_state = Message_router_state.deserialize_and_set

let send_join_to_message_router = Message_router_client.send_join_to_message_router
let send_message_to_peer = Message_router_client.send_message_to_peer
let send_message_to_topic = Message_router_client.send_message_to_topic
let send_message_to_topic_of_remote_router = Message_router_client.send_message_to_topic_of_remote_router

let send_peer_advertisement_about_us = Message_router.send_peer_advertisement_about_us

let join_groups = Service.join_groups
let leave_all_groups = Service.leave_all_groups
let publish_to_subscribed_groups = Service.publish_to_subscribed_groups
let pull_seen = Service.pull_seen
let service_show_state = Service.show_state
let service_clear_multicast_table = Service.clear_multicast_table

let mk_encoded_unicast_message = Message.mk_encoded_unicast_message
let mk_encoded_multicast_message = Message.mk_encoded_multicast_message
let mk_multicast_message = Message.mk_multicast_message
let mk_unicast_message = Message.mk_unicast_message
let mk_encoded_peer_advertisement_message = Message.mk_encoded_peer_advertisement_message

let pp_message_router_state = Message_router_state.pp
let pp_message_router_state' = Message_router_state.Mut.pp'

let get_connection = Message_router_state.get_connection
let get_groups_from_multicast_table = Message_router_state.get_groups_from_multicast_table

let mk_privkey_base64_exn = Commons.Crypto.Mk.privkey_base64_exn
let mk_pubkey_base64_exn = Commons.Crypto.Mk.pubkey_base64_exn
let mk_seen = Message.Com.Mk.seen

let stream_msg = Message_router_stream.stream_msg
let test_stream_msg = Message_router_stream.test_stream_msg

let unqueue_message = Message_router_state.unqueue_message_with_stats'
let reset_message_queue = Message_router_state.reset_message_queue
let reset_message_cache = Message_router_state.reset_message_cache

let clear_routing_table = Message_router_state.clear_routing_table
let clear_multicast_table = Message_router_state.clear_multicast_table

let now = Time.Time_upsycle_unix.now
let now_f = Time.Time_upsycle_unix.now_f

let start_service = Service.start

let pubkey_of_certfile_exn = Commons.Crypto.pubkey_of_certfile_exn
let pubkey_of_keyfile_exn = Commons.Crypto.pubkey_of_keyfile_exn
let privkey_of_keyfile_exn = Commons.Crypto.privkey_of_keyfile_exn
let generate_key_pair = Commons.Crypto.generate_key_pair

let parse_conduit_stream conn seen_length = Commonp.parse_conduit_stream (Message.Base.Decode.message seen_length) conn

include Yaml

(** {!Message} *)

module Message = struct
  include Message
end

module Basic = struct
  include Basic
end

module Util_cache = struct
  include Util_cache
end

module Util_conduit = struct
  include Util_conduit
end

module Util_crypto_io = struct
  include Util_crypto_io
end

module Util_crypto = struct
  include Util_crypto
end

module Util_encode_decode_io = struct
  include Util_encode_decode_io
end

module Util_encode_decode = struct
  include Util_encode_decode
end

module Util_io = struct
  include Util_io
end

module Util_lwt = struct
  include Util_lwt
end

module Util_lwt_seaboar = struct
  include Util_lwt_seaboar
end

module Util_pure = struct
  include Util_pure
end

module Util_yaml_io = struct
  include Util_yaml_io
end

module Util_yaml = struct
  include Util_yaml
end
