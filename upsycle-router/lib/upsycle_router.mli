(** This is the top-level entrypoint module for Upsycle_router. *)

(** The entire {!Commons} module is re-exported through the top level. *)

(** {3 Commons re-exports. } *)

module Commons : sig
  include module type of Commons (** @inline *)
end
include module type of Commons (** @inline *)

(** {!Message_router_config} and {!Service_config} are used for configuring an
    executable which uses this library code. See the code for
    [upsycle-router.exe] and [upsycle-service.exe] in the bin directory for
    usage. *)

module Message_router_config : sig
  type remote_router_cnf = [ `Rr of
    [ `Cf of string ] * [ `Ip of Commons.ip ] * [ `P of Commons.port ] *
    [ `Rn of string ]
  ]
  type multicast_entry_cnf = [ `Mte of
    [ `Gp of string ] * [ `Subs of string list ]
  ]
  type message_router_route' = [ `Mrr of
    [ `Addr of string ] * [ `Rt of [
      | `Ls (* local-service *)
      | `Rsa (* remote-service anonymous *)
      | `Rsk of string * Commons.ip_port (* remote-service known *)
      | `Rra (* remote-router anonymous *)
      | `Rrk of Commons.ip_port (* remote-router known *)
    ]]
  ]

  type control_interface = Commons.host_port
  type key_set = Commons.pubkey BatSet.t
  type t = Message_router_config.t

  val keys_of_key_set : key_set -> Commons.pubkey list
  val mk :
    base_dir:string ->
    bool -> int -> string -> control_interface option -> Commons.ip ->
    float -> string -> bool -> string list -> string -> Commons.msg_id_hash_function ->
    multicast_entry_cnf list -> Commons.multicast_group list ->
    Commons.command -> Commons.port -> Commons.multicast_requestor ->
    Commons.print_messages_on_console -> remote_router_cnf list ->
    message_router_route' list -> int -> string -> float -> bool -> t

  val control_interface_of_yaml : control_interface of_yaml
  val control_interface_to_yaml : control_interface to_yaml
  val key_set_of_yaml : key_set of_yaml
  val key_set_to_yaml : key_set to_yaml
  val local_services_of_yaml : key_set of_yaml
  val local_services_to_yaml : key_set to_yaml
end
module Service_config : sig
  include module type of Service_config
end

(** {3 Re-exported configuration types from modules other than Commons.} *)

type message_router_route = Message_router_route.t

(** {3 Aliases used in this interface.} *)

type timer_handler := expired:bool -> Message_router_state.Message_queue.timer_handler_data -> unit
type join_leave_return := [ `No_groups | `Ok of int ]
type publish_return := [ `No_groups | `Ok of int ]
type pull_seen_return := [ `No_seen | `Ok of int ]

(** Initialize the cryptography functions in {!Mirage_crypto_rng} (calls
    [Mirage_crypto_rng_lwt.initialize] underneath). It's important to call this
    during initialization, before using any of those functions.
    *)
val init_crypto : unit -> unit

(** {3 Starting / using a message router. }

    Note that the message router keeps track of its own mutable state as a
    singleton, which saves us from having to manually keep track of the state
    and pass it to all the functions. It also means that you can only start one
    message router per process. *)

(**/**)
type push_stream := Message_router_stream.stream_msg option -> unit
type push_thread_cb := Message_router_config.t -> push_stream -> unit Lwt.t
(**/**)

val start_message_router : ?threads:(push_thread_cb list) -> Message_router_config.t -> unit Lwt.t
val serialize_message_router_state : Message_router_config.t -> unit Lwt.t
val deserialize_and_set_message_router_state : timer_handler -> int -> int -> string -> unit Lwt.t
val send_peer_advertisement_about_us : Message_router_config.t -> addr_type:[ `Tcp ] -> rev:int -> ttl_multicast:int -> ttl_pa:int -> exp_multicast:int -> exp_pa:int -> string -> pubkey -> [> `No_groups | `Ok of int ] Lwt.t
val get_connection : Message_router_state.addr -> Message_router_route.connection option
val get_groups_from_multicast_table : unit -> multicast_group_pubkey list
val unqueue_message : expired:bool -> Commons.msg_id_hash_function -> Commonp.xcast * pubkey * Message.Base.t -> unit
val reset_message_queue : unit -> unit
val reset_message_cache : unit -> unit
val clear_routing_table : unit -> unit
val clear_multicast_table : unit -> unit

(** {3 Common functions for message router clients. }

    This refers to either message routers or services who want to communicate
    with a remote message router. *)

val send_message_to_peer : ttl:int -> exp:int -> body:string -> pubkey Util_conduit.connection -> privkey -> pubkey -> pubkey -> unit Lwt.t
val send_join_to_message_router : ttl:int -> exp:int -> 'a Util_conduit.connection -> privkey -> pubkey -> pubkey -> pubkey -> unit Lwt.t
val send_message_to_topic : ttl:int -> exp:int -> seen:Message.Com.seen -> 'a Util_conduit.connection -> multicast_group_rw -> string -> unit Lwt.t
val send_message_to_topic_of_remote_router : ttl:int -> exp:int -> seen:Message.Com.seen -> pubkey:pubkey -> multicast_group_rw -> string -> unit Lwt.t

(** {3 Starting / using a service. }

    In the case of services the caller is responsible for keeping track of the
    state and most functions accept a state reference which they will mutate or
    query.
    *)

val start_service : ?threads:(Service_config.t -> Service.State.state ref -> pubkey Util_conduit.Connection.t -> unit Lwt.t) list -> Service_config.t -> unit Lwt.t
val join_groups : ?prepend:string -> ttl:int -> exp:int -> Service_config.t -> Service.State.state ref -> 'a Util_conduit.Connection.t -> multicast_group list -> join_leave_return Lwt.t
val leave_all_groups : ?prepend:string -> ttl:int -> exp:int -> Service_config.t -> Service.State.state ref -> 'a Util_conduit.Connection.t -> join_leave_return Lwt.t
val publish_to_subscribed_groups : ?prepend:string -> ttl:int -> exp:int -> Service_config.t -> Service.State.state ref -> 'a Util_conduit.Connection.t -> publish_return Lwt.t
val pull_seen : ?prepend:string -> ttl:int -> exp:int -> Service_config.t -> Service.State.state ref -> 'a Util_conduit.Connection.t -> pull_seen_return Lwt.t
val service_show_state : Service_config.t -> Service.State.state ref -> string -> unit Lwt.t
val service_clear_multicast_table : Service.State.state ref -> unit

(** {3 Creating CBOR-encoded strings with various types of messages. } *)

val mk_encoded_unicast_message : ttl:int -> exp:int -> ?via:pubkey -> privkey * pubkey -> pubkey -> string -> string
val mk_encoded_multicast_message : ttl:int -> exp:int -> seen:Message.Com.seen -> multicast_group_rw -> string -> string
val mk_unicast_message : ?test_tamper:bool -> ttl:int -> exp:int -> ?via:pubkey -> privkey * pubkey -> pubkey -> string -> Message.Base.t
val mk_multicast_message : ?test_tamper:bool -> ttl:int -> exp:int -> ?via:pubkey -> seen:Message.Com.seen -> multicast_group_rw -> string -> Message.Base.t
val mk_encoded_peer_advertisement_message : ?test_tamper:bool -> id:pubkey -> Commons.ip * port * [ `Tcp ] -> pubkey option * pubkey option -> grps:multicast_group_pubkey list -> rev:int -> ttl:int -> exp:int -> priv_key:privkey -> string

(** {3 Creating data structures and values.} *)

val mk_privkey_base64_exn : string -> privkey
val mk_pubkey_base64_exn : string -> pubkey
val mk_seen : int -> Message.Com.seen

(** {3 Pretty printers.} *)

val pp_message_router_state : Format.formatter -> Message_router_state.t -> unit
val pp_message_router_state' : [ `Short | `Long ] -> Format.formatter -> 'a -> unit

(** {3 Time. } *)

val now : unit -> int
val now_f : unit -> float

(** {3 Stream manipulation and parsing. } *)

val parse_conduit_stream : 'a Util_conduit.Connection.t -> int -> (Message.Base.t -> bool Lwt.t) -> (unit -> bool Lwt.t) -> (string -> [< `Fail | `Scrub of int ] Lwt.t) -> (string -> unit Lwt.t) -> unit Lwt.t
val stream_msg : Message_router_route.connection -> Util_conduit.info -> pubkey -> pubkey -> Message.Base.t -> Message_router_stream.stream_msg
val test_stream_msg : Util_conduit.info -> pubkey -> pubkey -> Message.Base.t -> Message_router_stream.stream_msg

(** {3 Helper functions for dealing with certificates and keys.} *)

val pubkey_of_certfile_exn : ?quiet:bool -> command -> string -> pubkey
val pubkey_of_keyfile_exn : ?quiet:bool -> command -> string -> pubkey
val privkey_of_keyfile_exn : ?quiet:bool -> command -> string -> privkey
val generate_key_pair : ?g:Mirage_crypto_rng.g -> unit -> privkey * pubkey

(** We flatten this module so that [\[@@deriving yaml\]] works transparently from the outside. *)
include module type of Commons.Yaml

(**/**)
(* Some modules (might) provide their own yaml encoding functions: *)
(**/**)

(** {3 Message API.} *)

(** We export the {!Message} module here, which may be useful
    for constructing and understanding the various kinds of messages. It's
    generally not necessary to use this module directly. *)

module Message : sig
  include module type of Message
end

(** {3 “Basic” (implicit imports) and utilities. } *)

(** We export {!Basic} and all the utils here for convenience and for better
    documentation,
    though they are also available directly as libraries. {!Basic} is imported
    using [open] by nearly every module. *)

module Basic : sig
  include module type of Basic
end

module Util_cache : sig
  include module type of Util_cache
end

module Util_conduit : sig
  include module type of Util_conduit
end

module Util_crypto_io : sig
  include module type of Util_crypto_io
end

module Util_crypto : sig
  include module type of Util_crypto
end

module Util_encode_decode_io : sig
  include module type of Util_encode_decode_io
end

module Util_encode_decode : sig
  include module type of Util_encode_decode
end

module Util_io : sig
  include module type of Util_io
end

module Util_lwt : sig
  include module type of Util_lwt
end

module Util_lwt_seaboar : sig
  include module type of Util_lwt_seaboar
end

module Util_pure : sig
  include module type of Util_pure
end

module Util_yaml_io : sig
  include module type of Util_yaml_io
end

module Util_yaml : sig
  include module type of Util_yaml
end
