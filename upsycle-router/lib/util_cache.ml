(** `Cache_map` and `Cache_set` are caches with a fixed number of elements,
    which remember the order in which elements are added. When the cache is full
    it evicts the oldest element before adding a new one.

    Note that there is no way to remove an element from the cache (aside from
    waiting until it gets pushed out).

    Note also that adding a key/value pair to `Cache_table` will have no effect
    if the key already exists. So this kind of table is particularly useful when
    the key and value correspond to each other uniquely, for example if the key
    is a hash of the value. *)

(** This is the underlying implementation which will be passed to the functor.)
    `('a, 'b) t` is the type here whether we're using sets or tables. In the
    case of sets the `'b` is a dummy variable which we will erase later. *)

module type In = sig
  type ('a, 'b) t
  val empty : ('a, 'b) t
  val length : ('a, 'b) t -> int
  val is_empty : ('a, 'b) t -> bool
  val has : 'a -> ('a, 'b) t -> bool
  val get : 'a -> ('a, 'b) t -> 'b option
  val remove : 'a -> ('a, 'b) t -> ('a, 'b) t

  (** This doubling of these functions is necessary because we need add
      functions of the form `elem -> set -> set` as well as `key -> value -> table -> table`.
      The list functions are doubled because we need lists of elements as well
      as of key/value pairs. *)
  val add1 : 'a -> ('a, 'b) t -> ('a, 'b) t
  val add2 : 'a -> 'b -> ('a, 'b) t -> ('a, 'b) t
  val of_list1 : 'a list -> ('a, 'b) t
  val of_list2 : ('a * 'b) list -> ('a, 'b) t
  val to_list1 : ('a, 'b) t -> 'a list
  val to_list2 : ('a, 'b) t -> ('a * 'b) list
end

module Cache_set : In = struct
  type ('a, 'b) t = 'a BatSet.t
  let empty = BatSet.empty
  let length = BatSet.cardinal
  let is_empty = BatSet.is_empty
  let has x = (<>) None % BatSet.find_opt x
  let get _ _ = failwith "hidden"
  let add1 = BatSet.add
  let add2 _ _ = failwith "hidden"
  let of_list1 = BatSet.of_list
  let of_list2 _ = failwith "hidden"
  let to_list1 = BatSet.to_list
  let to_list2 _ = failwith "hidden"
  let remove = BatSet.remove
end

module Cache_map : In = struct
  type ('a, 'b) t = ('a, 'b) BatMap.t
  let empty = BatMap.empty
  let length = BatMap.cardinal
  let is_empty = BatMap.is_empty
  let has k = (<>) None % BatMap.find_opt k
  let get = BatMap.find_opt
  let add1 _ = failwith "hidden"
  let add2 = BatMap.add
  let of_list1 _ = failwith "hidden"
  let of_list2 = BatMap.of_seq % BatSeq.of_list
  let to_list1 _ = failwith "hidden"
  let to_list2 = BatList.of_seq % BatMap.to_seq
  let remove = BatMap.remove
end

module Make (U: In) = struct
  type ('a, 'b) t = Cache of int * 'a List.t * ('a, 'b) U.t
  module Internal = struct
    let add data (Cache (s, l, x) as b) =
      let is_max = U.length x = s in
      let has', add' = match data with
      | `Single el -> U.has el, fun l x -> Cache (s, l @ [el], U.add1 el x)
      | `Pair (k, v) -> U.has k, fun l x -> Cache (s, l @ [k], U.add2 k v x)
      in if has' x then b
      else
        let l', x' = match is_max, l with
        | true, [] -> failwith "impossible"
        | true, h :: tl -> tl, U.remove h x
        | false, _ -> l, x
        in add' l' x'
    let of_list f g s elems =
      let l = BatList.take s elems in
      let l' = g l in
      Cache (s, l', f l)
    (* only for debugging *)
    let length_array (Cache (_, l, _)) = List.length l
  end

  let empty s =
    if s < 1 then failwith "Cache: Make: s must be >= 0"
    else Cache (s, [], U.empty)

  let length (Cache (_, _, x)) = U.length x
  let size (Cache (s, _, _)) = s
  let is_empty (Cache (_, _, x)) = U.is_empty x
  let has e (Cache (_, _, x)) = U.has e x
  let get e (Cache (_, _, x)) = U.get e x

  let add1 el = Internal.add (`Single el)
  let add2 k v = Internal.add (`Pair (k, v))

  let of_list1 s elems = Internal.of_list U.of_list1 Fun.id s elems
  let of_list2 s elems = Internal.of_list U.of_list2 (fun l -> List.map fst l) s elems

  let to_list1 (Cache (_, _, x)) = U.to_list1 x
  let to_list2 (Cache (_, _, x)) = U.to_list2 x
  let elems (Cache (_, l, _)) = l

  let pp which el_pp ppf (Cache (s, _, _) as bq) =
    let which_s' = match which with `Set -> "set" | `Table -> "table" in
    let sep ppf () = Fmt.pf ppf ",@ " in
    let n = length bq in
    Fmt.pf ppf "bounded-%s(%d/%d) = [@[%a@]]" which_s' n s
      (Fmt.list ~sep el_pp) (elems bq)
  let pp_short which ppf (Cache (s, _, _) as bq) =
    let which_s' = match which with `Set -> "set" | `Table -> "table" in
    let n = length bq in
    Fmt.pf ppf "bounded-%s(%d/%d)" which_s' n s

  (* only for debugging *)
  let length_array__ = Internal.length_array
end

module Cs = Make (Cache_set)

module CSet = struct
  (** We use the private constructor `C` defined using GADT syntax to make the
      `'b` dummy variable existential and thus erase it from the user-facing
      type. *)
  type 'a t = C : ('a, 'b) Cs.t -> 'a t
  let empty s = C (Cs.empty s)
  let length (C q) = Cs.length q
  let size (C q) = Cs.size q
  let is_empty (C q) = Cs.is_empty q
  let has x (C q) = Cs.has x q
  let add x (C q) = C (Cs.add1 x q)
  let of_list s xs = C (Cs.of_list1 s xs)
  let to_list (C q) = Cs.to_list1 q
  let elems (C q) = Cs.elems q
  let pp el_pp ppf (C q) = Cs.pp `Set el_pp ppf q
  let pp_short ppf (C q) = Cs.pp_short `Set ppf q
  let length_array__ (C q) = Cs.length_array__ q
end

module Cm = Make (Cache_map)

module CMap = struct
  include Cm
  let add = add2
  let of_list = of_list2
  let to_list = to_list2
  let pp el_pp ppf = pp `Table el_pp ppf
  let pp_short ppf = pp_short `Table ppf
end
