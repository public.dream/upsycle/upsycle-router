module CSet : sig
  type 'a t
  val empty : int -> 'a t
  val length : 'a t -> int
  val size : 'a t -> int
  val is_empty : 'a t -> bool
  val has : 'a -> 'a t -> bool
  val add : 'a -> 'a t -> 'a t
  val of_list : int -> 'a list -> 'a t
  val to_list: 'a t -> 'a list
  val elems : 'a t -> 'a list
  val pp : 'a Fmt.t -> Format.formatter -> 'a t -> unit
  val pp_short : Format.formatter -> 'a t -> unit

  val length_array__ : 'a t -> int
end

module CMap : sig
  type ('a, 'b) t
  val empty : int -> ('a, 'b) t
  val length : ('a, 'b) t -> int
  val size : ('a, 'b) t -> int
  val is_empty : ('a, 'b) t -> bool
  val has : 'a -> ('a, 'b) t -> bool
  val get : 'a -> ('a, 'b) t -> 'b option
  val add : 'a -> 'b -> ('a, 'b) t -> ('a, 'b) t
  val of_list : int -> ('a * 'b) list -> ('a, 'b) t
  val to_list: ('a, 'b) t -> ('a * 'b) list
  val elems : ('a, 'b) t -> 'a list
  val pp : 'a Fmt.t -> Format.formatter -> ('a, 'b) t -> unit
  val pp_short : Format.formatter -> ('a, 'b) t -> unit

  val length_array__ : ('a, 'b) t -> int
end
