module SE = Seaboar.Encode

module ULwt = Util_lwt
module UOption = Util_pure.Option
module UString = Util_pure.String

(** util_conduit doesn't depend on any of our modules like Crypto, Common etc.,
    hence the type parameter on `'a Connection`.
    All IP addresses are `Ipaddr.t` and ports are `int`. *)

let verbose_authenticators = false
let debug_peer_key = false

type info = string

type 'a peerkey = Peerkey of 'a

module Connection = struct
  type 'a t = Connection of {
    flow: Conduit_lwt_unix.flow;
    ic: Lwt_io.input Lwt_io.channel;
    oc: Lwt_io.output Lwt_io.channel;
    oc_fmt: Lwt_fmt.formatter;
    peerkey: 'a peerkey;
    peerip: Ipaddr.t;
    peerport: int;
    info_str: info;
  }

  module Acc = struct
    let ic (Connection c) = c.ic
    let oc (Connection c) = c.oc
    let oc_fmt (Connection c) = c.oc_fmt
    let info_str (Connection c) = c.info_str
  end
end

type 'a connection = 'a Connection.t

let null_authenticator tag ?ip:_ ~host:ohost certs =
  let () = if verbose_authenticators then
    Fmt.pr "[%s null_authenticator] host=%s, # certs is %d@." tag
      (Option.fold ~none:"NONE" ~some:(Fmt.str "%a" Domain_name.pp) ohost)
      (List.length certs) in
  Ok None

let key_fingerprints_authenticator tag (hash, fingerprint) ?ip:_ ~host:ohost certs =
  let () = if verbose_authenticators then
    Fmt.pr "[%s key_fingerprints_authenticator] host=%s, # certs is %d@." tag
      (Option.fold ~none:"NONE" ~some:(Fmt.str "%a" Domain_name.pp) ohost)
      (List.length certs) in
  let time () = Some (Ptime_clock.now ()) in
  X509.Authenticator.server_key_fingerprint ~time ~hash ~fingerprint ~host:ohost certs

(** Raises an exception if the given flow structure is not the `TCP` variant. *)
let ip_port_of_flow_exn = function
  | Conduit_lwt_unix.TCP flow -> flow.ip, flow.port
  | _ -> raise (Invalid_argument "ip_port_of_flow_exn: must be TCP flow")

(** Raises an exception if the contained flow structure is not the `TCP` variant. *)
let ip_port_of_conn_exn (Connection.Connection conn) = ip_port_of_flow_exn conn.flow

let peerkey_of_connection_info info mk_peerkey =
  let open ULwt.Letsfix in
  let peerkey' = Conduit_connection_info.keycb info () in
  let peerkey = match peerkey' with
  | `None -> failwith "Expected to retrieve a peer key, got none"
  | `Error e -> failwith @@ "Couldn't retrieve peer key: " ^ e
  | `Key k -> begin match k with
      | `ED25519 k' -> k'
      | _ -> failwith "Expected an ED25519 peer key"
    end in
  let* () = Util_lwt.wanneer debug_peer_key @@ fun _ ->
    Util_lwt.info "peer key hash: %a" Conduit_connection_info.pp_key peerkey' in
  Lwt.return @@ mk_peerkey peerkey

(** The TLS library will always check `hostname'` against the CN (and/or
    Subject Alternate Names, if present) of the server certificate. If
    `server_fingerprint` is given then it will also check that the key
    fingerprint matches, and do a second hostname check. The second hostname
    check is redundant but is built in to the library code. So we just send
    `hostname'` through as part of the fingerprint.

    The promise will reject if `hostname'` can't be parsed as a `[ \`host ]
    Domain_name.t` *)

let open_client_connection
    ?(info_str="") ?server_fingerprint ?(client_auth=`None)
    hostname' ip' port' mk_peerkey =
  let open ULwt.Letsfix in
  let client = `TLS (`Hostname hostname', `IP ip', `Port port') in
  let tls_authenticator = match server_fingerprint with
  | None -> null_authenticator "client"
  | Some (hash, fp) ->
      key_fingerprints_authenticator "client" (hash, fp) in
  let* flow, ic, oc, oconnection_info =
    let* ctx = match client_auth with
    | `None -> Lwt.return Conduit_lwt_unix.default_ctx
    | `Cert (c, k) ->
        let c', k' = `Crt_file_path c, `Key_file_path k in
        let tls_own_key = `TLS (c', k', `No_password) in
        Conduit_lwt_unix.init ~tls_own_key ~tls_authenticator () in
    Conduit_lwt_unix.connect ~ctx client in

  let connection_info = match oconnection_info with
  | None -> failwith "interface mismatch with conduit: since this is a TLS connection we expect non-empty connection info -- aborting"
  | Some c -> c in
  let* peerkey = peerkey_of_connection_info connection_info mk_peerkey in
  Lwt.return (Connection.Connection {
    flow; ic; oc; peerkey; oc_fmt = Lwt_fmt.of_channel oc; info_str;
    peerip = ip'; peerport = port';
  })

(** Raises an exception if `conduit` returns empty connection info. *)

let start_server ?info_prefix ?openssl_cmd cert_path key_path ip port mk_peerkey handle =
  let open ULwt.Letsfix in
  let crt = `Crt_file_path cert_path in
  let key = `Key_file_path key_path in
  let pass = `No_password in
  let tls_own_key = `TLS (crt, key, pass) in
  let own_key = UOption.fold "[key unknown]" (
    fun cmd' -> Util_crypto_io.pubkey_of_keyfile_exn cmd' key_path
  ) openssl_cmd in
  let mode = `TLS (crt, key, pass, `Port port) in
  let src = Ipaddr.to_string ip in
  let ip_port_s = Util_io.fmt_out "%s:%d" src port in
  let info_str = Util_io.fmt_out "%s[TLS server %a]"
    (UOption.fold "" (UString.append " ") info_prefix)
    Util_io.blue ip_port_s in
  let prf fmt = Util_lwt.mk_prf info_str fmt in
  let catch exn' = Fmt.epr "Got server exception: %a@." Fmt.exn exn' in
  let tls_authenticator = null_authenticator "server" in
  let* ctx = Conduit_lwt_unix.init ~tls_own_key ~tls_authenticator ~src () in
  (** `handle_connection` will raise an exception if the `flow` is not a TCP flow, which it must always be. *)
  let handle_connection flow ic oc oconnection_info =
    let connection_info = match oconnection_info with
    | None -> failwith "interface mismatch with conduit: since this is a TLS connection we expect non-empty connection info -- aborting"
    | Some c -> c in
    let* peerkey = peerkey_of_connection_info connection_info mk_peerkey in
    let peerip, peerport = ip_port_of_flow_exn flow in
    handle (Connection.Connection {
      flow; ic; oc; info_str;
      peerkey; peerip; peerport;
      oc_fmt = Lwt_fmt.of_channel oc;
    }) in
  let* () = prf "[pubkey=%s] starting" (Base64.encode_string own_key) in
  let* () = Conduit_lwt_unix.serve ~on_exn:catch ~ctx ~mode handle_connection in
  prf "terminated"

let send_s (Connection.Connection conn) =
  Util_lwt.fprintf conn.oc_fmt "%s"

let send connection encoder data =
  send_s connection (SE.encode encoder data)

let send_terminate_message connection =
  send connection SE.bytes "byte"

let close ic oc =
  let close' c s =
    let f () = Lwt_io.close c in
    let catch' e =
      Util_lwt.warn "Unable to close %s channel: %a" s Fmt.exn e in
    Lwt.catch f catch' in
  Lwt.join [close' oc "output"; close' ic "input"]

let close_connection (Connection.Connection conn) = close conn.ic conn.oc

(* --- for reference only -- this is how to make an authenticator which can authenticate
   a certificate chain given a ca_path like '/etc/ssl/certs'. *)
let capath_authenticator_lwt ca_path =
  let open ULwt.Letsfix in

  let failure msg = Lwt.fail @@ Failure msg in
  let (</>) a b = a ^ "/" ^ b in

  let extension str =
    let n = String.length str in
    let rec scan = function
      | i when i = 0 -> None
      | i when str.[i - 1] = '.' ->
          Some (String.sub str i (n - i))
      | i -> scan (pred i) in
    scan n in

  let catch_invalid_arg th h =
    Lwt.catch (fun () -> th)
      (function
        | Invalid_argument msg -> h msg
        | exn                  -> Lwt.fail exn) in

  let read_file path =
    let open Lwt_io in
    open_file ~mode:Input path >>= fun file ->
    read file >|= Cstruct.of_string >>= fun cs ->
    close file >|= fun () ->
    cs in

  let certs_of_pem path =
    catch_invalid_arg
      (read_file path >|= fun pem ->
       match X509.Certificate.decode_pem_multiple pem with
       | Ok cs -> cs
       | Error (`Msg m) -> invalid_arg ("failed to parse certificates " ^ m))
      (failure % Printf.sprintf "Certificates in %s: %s" path) in

  let read_dir path =
    let open Lwt_unix in
    let rec collect acc d =
      readdir_n d 10 >>= function
        | [||] -> Lwt.return acc
        | xs   -> collect (Array.to_list xs @ acc) d in
    opendir path >>= fun dir ->
    collect [] dir >>= fun entries ->
    closedir dir >|= fun () ->
    entries in

  let certs_of_pem_dir path =
    read_dir path
    >|= List.filter (fun file -> extension file = Some "crt")
    >>= Lwt_list.map_p (fun file -> certs_of_pem (path </> file))
    >|= List.concat in

  let time () = Some (Ptime_clock.now ()) in
  let of_cas cas = X509.Authenticator.chain_of_trust ~time cas
  in certs_of_pem_dir ca_path >|= of_cas

let mk_prf ?prepend ?append (Connection.Connection conn) =
  Util_lwt.mk_prf ?prepend ?append conn.info_str
