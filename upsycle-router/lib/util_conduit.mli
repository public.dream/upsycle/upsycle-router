(** util_conduit doesn't depend on any of our modules like Crypto, Common etc.,
    hence the type parameter on `'a Connection`.
    All IP addresses are `Ipaddr.t` and ports are `int`. *)

type info = string
type 'a peerkey = Peerkey of 'a
type 'a fmt := ('a, Format.formatter, unit, unit Lwt.t) format4 -> 'a
type ic := Lwt_io.input Lwt_io.channel
type oc := Lwt_io.output Lwt_io.channel
type 'a mk_peerkey := Mirage_crypto_ec.Ed25519.pub -> 'a peerkey

module Connection : sig
  type 'a t = Connection of {
    flow: Conduit_lwt_unix.flow;
    ic: ic;
    oc: oc;
    oc_fmt: Lwt_fmt.formatter;
    peerkey: 'a peerkey;
    peerip: Ipaddr.t;
    peerport: int;
    info_str: info;
  }
  module Acc : sig
    val ic : 'a t -> ic
    val oc : 'a t -> oc
    val oc_fmt : 'a t -> Lwt_fmt.formatter
    val info_str : 'a t -> info
  end
end

type 'a connection = 'a Connection.t

val ip_port_of_flow_exn : Conduit_lwt_unix.flow -> Ipaddr.t * int
val ip_port_of_conn_exn : 'a connection -> Ipaddr.t * int
val open_client_connection :
  ?info_str:string -> ?server_fingerprint:(Mirage_crypto.Hash.hash * Cstruct.t) ->
  ?client_auth:[ `None | `Cert of string * string] ->
  string -> Ipaddr.t -> int -> 'a mk_peerkey -> 'a connection Lwt.t
val start_server : ?info_prefix:string -> ?openssl_cmd:(string list) -> string -> string -> Ipaddr.t -> int -> 'a mk_peerkey -> ('a connection -> unit Lwt.t) -> unit Lwt.t
val send_s : 'a connection -> string -> unit Lwt.t
val send : 'a connection -> 'b Seaboar.Encode.encoder -> 'b -> unit Lwt.t
val send_terminate_message : 'a connection -> unit Lwt.t
val close : 'a Lwt_io.channel -> 'a Lwt_io.channel -> unit Lwt.t
val close_connection : 'a connection -> unit Lwt.t
type fingerprint := Cstruct.t
val capath_authenticator_lwt : string -> X509.Authenticator.t Lwt.t
val key_fingerprints_authenticator : string -> Mirage_crypto.Hash.hash * fingerprint -> X509.Authenticator.t
val null_authenticator : string -> X509.Authenticator.t
val mk_prf : ?prepend:string -> ?append:string -> 'a connection -> 'b fmt
