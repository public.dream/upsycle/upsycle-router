(* Make the Error part into a `Msg of string *)
let key_25519_of_cstruct key_of_cstruct =
  let m x = `Msg x in
  let f = m % function
  | `At_infinity -> "At_infinity"
  | `Invalid_format -> "Invalid_format"
  | `Invalid_length -> "Invalid_length"
  | `Invalid_range -> "Invalid_range"
  | `Low_order -> "Low_order"
  | `Not_on_curve -> "Not_on_curve" in
  Result.map_error f % key_of_cstruct

let pubkey_25519_of_cstruct = key_25519_of_cstruct Mirage_crypto_ec.Ed25519.pub_of_cstruct
let privkey_25519_of_cstruct = key_25519_of_cstruct Mirage_crypto_ec.Ed25519.priv_of_cstruct

let pubkey_25519_to_cstruct = Mirage_crypto_ec.Ed25519.pub_to_cstruct
let privkey_25519_to_cstruct = Mirage_crypto_ec.Ed25519.priv_to_cstruct

let pubkey_25519_of_string = Cstruct.of_string %> pubkey_25519_of_cstruct
let pubkey_25519_to_string = pubkey_25519_to_cstruct %> Cstruct.to_string

let privkey_25519_of_string = Cstruct.of_string %> privkey_25519_of_cstruct
let privkey_25519_to_string = privkey_25519_to_cstruct %> Cstruct.to_string

let privkey_25519_to_string_base64 = privkey_25519_to_string %> Base64.encode_string
let pubkey_25519_to_string_base64 = pubkey_25519_to_string %> Base64.encode_string

let fingerprint_ed25519_sha256 key =
  let hash = `SHA256 in
  hash, X509.Public_key.fingerprint ~hash (`ED25519 key)

let sign privkey =
  Cstruct.of_string
  %> Mirage_crypto_ec.Ed25519.sign ~key:privkey
  %> Cstruct.to_string

let verify pubkey sign' msg' =
  let msg = Cstruct.of_string msg' in
  let sign = Cstruct.of_string sign' in
  Mirage_crypto_ec.Ed25519.verify ~key:pubkey sign ~msg

let blake_str ?size f = Bytes.to_string % f ?size % Bytes.of_string
let blake2b_32' ?(size=64) b = Hacl_star.Hacl.Blake2b_32.hash b size
let blake2b_32 ?size = blake_str ?size blake2b_32'
let blake2b_256' ?(size=64) b = Hacl_star.Hacl.Blake2b_256.hash b size
let blake2b_256 ?size = blake_str ?size blake2b_256'
let blake2s_32' ?(size=32) b = Hacl_star.Hacl.Blake2s_32.hash b size
let blake2s_32 ?size = blake_str?size blake2s_32'
let blake2s_128' ?(size=32) b = Hacl_star.Hacl.Blake2s_128.hash b size
let blake2s_128 ?size = blake_str?size blake2s_128'
