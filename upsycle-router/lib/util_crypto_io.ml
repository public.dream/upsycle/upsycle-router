module UCrypto = Util_crypto
module Uio = Util_io
module UString = Util_pure.String

let key_printing_short = ref false
let set_key_printing_short b = key_printing_short := b

let pp_privkey_25519 ppf =
  Mirage_crypto_ec.Ed25519.priv_to_cstruct
  %> Cstruct.to_string
  %> Base64.encode_string
  %> Fmt.pf ppf "|%a:%s|" Uio.bright_red "privkey"

let pp_privkey_none ppf () =
  Fmt.pf ppf "|%a:%s|"
    Uio.bright_red "privkey"
    "(none)"

let pp_pubkey_25519 ppf =
  let f pos len str = String.sub str pos len in
  Mirage_crypto_ec.Ed25519.pub_to_cstruct
  %> Cstruct.to_string
  %> Base64.encode_string
  %> (if !key_printing_short = true then f 0 4 else (fun x -> x))
  %> Fmt.pf ppf "|%a:%s|" Uio.green "pubkey"

let pubkey_of_certfile ?(print_stderr=true) ?(complain=true) openssl_cmd fp =
  let ok, out, err = Uio.cmd ~die:false ~print_stderr ~complain @@ openssl_cmd @ [
    "x509"; "-pubkey"; "-in"; fp; "-noout";
  ] in
  if ok then
    let ls = UString.(lines (chomp out)) in
    let key64 = match List.length ls with
      | 3 -> BatList.at ls 1
      | _ -> failwith @@ Fmt.str "pubkey_of_certfile: unexpected output from openssl: %s" out in
    let key = Base64.decode_exn key64 in
    Ok (String.sub key (String.length key - 32) 32)
  else Error (`Msg ("pubkey_of_certfile: command failed: " ^ err))

let pubkey_of_certfile_exn ?print_stderr openssl_cmd fp =
  match pubkey_of_certfile ?print_stderr openssl_cmd fp with
  | Ok key -> key
  | Error (`Msg m) -> failwith m

let key_of_keyfile ?(print_stderr=true) ?(complain=true) the_type openssl_cmd fp =
  let ok, out, err = Uio.cmd ~die:false ~print_stderr ~complain @@ openssl_cmd @ [
    "pkey"; "-in"; fp; "-outform"; "DER";
  ] @ begin match the_type with
  | `Pub -> ["-pubout"]
  | `Priv -> [] end in
  if ok
  then Ok (String.sub out (String.length out - 32) 32)
  else Error (`Msg ("key_of_keyfile: command failed: " ^ err))

let key_of_keyfile_exn ?print_stderr the_type openssl_cmd fp =
  match key_of_keyfile ?print_stderr the_type openssl_cmd fp with
  | Ok key -> key
  | Error (`Msg m) -> failwith m

let pubkey_of_keyfile_exn ?print_stderr = key_of_keyfile_exn ?print_stderr `Pub
let privkey_of_keyfile_exn ?print_stderr = key_of_keyfile_exn ?print_stderr `Priv

(* ------ random number / key generation *)

let init () =
  Mirage_crypto_rng_lwt.initialize ()

let generate_key_pair ?g () =
  let priv, pub = Mirage_crypto_ec.Ed25519.generate ?g () in
  priv, pub

let generate_key_pair_base64 ?g () =
  let priv, pub = generate_key_pair ?g () in
  let priv' = priv |> UCrypto.privkey_25519_to_string_base64 in
  let pub' = pub |> UCrypto.pubkey_25519_to_string_base64 in
  priv', pub'
