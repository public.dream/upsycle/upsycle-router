module SD = Seaboar.Decode
module SE = Seaboar.Encode
module U32 = Stdint.Uint32

module Encode = struct
  (** These flatten one level, useful for variable length lists with optional
      elements for example. *)
  let mk_map = SE.map % List.flatten
  let mk_map' = SE.map' % List.flatten
  let mk_list = SE.array % List.flatten

  let kv_opt k enc o =
    let f v = [k, v] in
    Option.fold ~none:[] ~some:(f % enc) o
  let list_elem_opt enc o =
    let f = Util_pure.List.sngl in
    Option.fold ~none:[] ~some:(f % enc) o

  (* --- @enhancement add to seaboar *)
  let u32 = SE.int % U32.to_int

  (** Encodes a `BatMap.t`. *)
  (* --- @enhancement make more efficient *)
  let table kenc venc table =
    let f (key, value) = (kenc key, venc value) in
    let table' = table |> Util_pure.table_map_as_pairs f
    in SE.map table'
  (** Encodes a `BatSet.t`. *)
  (* --- @enhancement make more efficient *)
  let set el_enc = SE.list el_enc % BatSet.to_list

  let cset el_enc cache =
    SE.list el_enc (Util_cache.CSet.elems cache)
  let cmap (kenc, venc) cache =
    let e (k, v) = SE.array [kenc k; venc v] in
    SE.list e (Util_cache.CMap.to_list cache)
end

module Decode = struct
  open SD.Infix
  (* --- @enhancement add to seaboar *)
  let u32 = U32.of_int <$> SD.int
  (* @todo shouldn't this produce a BatMap.t? *)
  let table kdec vdec = SD.map (kdec, vdec)
  let set el_dec = BatSet.of_list <$> SD.array_list el_dec
  let cset el_dec sz = Util_cache.CSet.of_list sz <$> SD.array_list el_dec
  let cmap (kdec, vdec) sz =
    let q = SD.array_tuple2 kdec vdec in
    Util_cache.CMap.of_list sz <$> SD.array_list q
end

let parse_string ?(consume=`All) =
  SD.run_parser_string ~consume
