module SE := Seaboar.Encode
module U32 := Stdint.Uint32

type 'a sencode := 'a -> Seaboar.Encode.value
type 'a sdecode := 'a Seaboar.Decode.t

module Encode : sig
  val mk_map : (SE.value * SE.value) list list sencode
  val mk_map' : (string * SE.value) list list sencode
  val mk_list : SE.value list list sencode
  val kv_opt : 'a -> ('b -> 'c) -> 'b option -> ('a * 'c) list
  val list_elem_opt : ('a -> 'b) -> 'a option -> 'b Util_pure.List.t
  val u32 : U32.t sencode
  val table : 'a sencode -> 'b sencode -> ('a, 'b) BatMap.t sencode
  val set : 'a sencode -> 'a BatSet.t -> SE.value
  val cset : 'a sencode -> 'a Util_cache.CSet.t sencode
  val cmap : 'a sencode * 'b sencode -> ('a, 'b) Util_cache.CMap.t sencode
end
module Decode : sig
  val u32 : U32.t sdecode
  val table : 'a sdecode -> 'b sdecode -> ('a * 'b) list sdecode
  val set : 'a sdecode -> 'a BatSet.t sdecode
  val cset : 'a sdecode -> int -> 'a Util_cache.CSet.t sdecode
  val cmap : 'a sdecode * 'b sdecode -> int -> ('a, 'b) Util_cache.CMap.t sdecode
end
val parse_string : ?consume:[ `All | `Prefix ] -> 'a sdecode -> string -> ('a, string) result
