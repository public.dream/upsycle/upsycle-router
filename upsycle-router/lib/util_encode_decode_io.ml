module U32 = Stdint.Uint32

module SD = Seaboar.Decode
module SE = Seaboar.Encode

module ULwt = Util_lwt

(** This module could move to a different project once it stabilises. For now it
    introduces a dependency on angstrom and angstrom-lwt-unix. *)

module Parse_stream = struct
  open ULwt.Letsfix
  let rerun f parser unconsumed ic n =
    let Angstrom.Buffered.({ buf; off; len; }) = unconsumed in
    let state = Angstrom.Buffered.parse parser in
    match len with
    | _ when n < 0 -> failwith "rerun: rewinding stream not implemented"
    | l when l < n -> Lwt.return ()
    | l ->
        let off', len' = off + n, l - n in
        let state' =
          let buf' = Bigstringaf.sub ~off:off' ~len:len' buf in
          Angstrom.Buffered.feed state (`Bigstring buf') in
        Angstrom_lwt_unix.with_buffered_parse_state state' ic >>= f

  (** If [onok'] or [onerr'] reject, the client connection will be broken. *)

  let get_parser parser' ic (onok', onerr') =
    let rec f = fun (unconsumed, result) ->
      let rerun' = rerun f parser' unconsumed ic in
      match result with
      (* --- parsing failed. *)
      | Error err ->
          let* recover' = onerr' err in
          begin match recover' with
          (* --- fail and close client connection *)
          | `Fail -> Lwt.fail_with "invalid input"
          (* --- advance or rewind stream (if possible) by `x` (0 is also ok), and try
           * again (if there's anything left). *)
          | `Scrub x -> rerun' x end
      (* --- parsing succeeded: do not advance the unconsumed part, run onok', and if
       * it returns true, reloop the parser. *)
      | Ok a -> onok' a >>= function
          | true -> rerun' 0
          | false -> Lwt.return ()
    in f
end

let parse_stream parser' ic (onok', onerr') =
  let open ULwt.Letsfix in
  Angstrom_lwt_unix.parse parser' ic >>= Parse_stream.get_parser parser' ic (onok', onerr')

let encode_and_save enc path x =
  let string' = SE.encode enc x in
  let f ch = Lwt_io.write ch string' in
  Lwt_io.with_file ~mode:Lwt_io.Output path f
