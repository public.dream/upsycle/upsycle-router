let stdin_raw () =
  let open Unix in
  let cur = tcgetattr stdin in
  let termio = {
    cur with
    c_icanon = false;
    c_echo = false;
  } in
  tcsetattr stdin TCSANOW termio

let check_tty die fd name =
  if not Unix.(isatty fd) then
    if die then failwith (Fmt.str "File descriptor (%s) is not connected to a tty." name)
    else false
  else true

let check_tty_stdin ?(die=true) () = check_tty die Unix.stdin "stdin"
let check_tty_stdout ?(die=true) () = check_tty die Unix.stdout "stdout"
let check_tty_stderr ?(die=true) () = check_tty die Unix.stderr "stderr"

(* --- @todo *)
let warn = Fmt.epr

let pp_map_horizontal' ppf map' show_k show_v =
  let open Format in
  let xs = ref [] in
  let f k v =
    let x = Printf.sprintf "%s: %s" (show_k k) (show_v v) in
    xs := !xs @ [x] in
  fprintf ppf "{";
  BatMap.iter f map';
  fprintf ppf "%s" (BatString.join ", " !xs);
  fprintf ppf "}"

let pp_map' ppf map' show_k show_v =
  let open Format in
  let f k v =
    fprintf ppf "    %s: %s\n" (show_k k) (show_v v) in
  fprintf ppf "{\n";
  BatMap.iter f map';
  fprintf ppf "}"

let fmt a b c = Fmt.fmt "%a" b a c
let fmt_out x = Fmt.(str_like stdout) x
let fmt_err x = Fmt.(str_like stderr) x

let pp_map_horizontal' = pp_map_horizontal'

let mk_pp_style s = Fmt.styled s

let pp_blue x = mk_pp_style (`Fg `Blue) x
let pp_cyan x = mk_pp_style (`Fg `Cyan) x
let pp_green x = mk_pp_style (`Fg `Green) x
let pp_magenta x = mk_pp_style (`Fg `Magenta) x
let pp_red x = mk_pp_style (`Fg `Red) x
let pp_yellow x = mk_pp_style (`Fg `Yellow) x

let pp_underline x = mk_pp_style (`Underline) x

let pp_bright_blue x = mk_pp_style (`Fg (`Hi `Blue)) x
let pp_bright_red x = mk_pp_style (`Fg (`Hi `Red)) x

let blue, cyan, green, magenta, red, yellow, underline, bright_blue, bright_red =
  let s = Fmt.string in
    pp_blue s, pp_cyan s, pp_green s, pp_magenta s, pp_red s,
    pp_yellow s, pp_underline s, pp_bright_blue s, pp_bright_red s

let pp_hpair p1 p2 ppf (x1, x2) = Fmt.pf ppf "@[%a -> %a@]" p1 x1 p2 x2
let pp_section p ppf (x1, x2) = Fmt.pf ppf "@[<v>%a@ %a@ @]" underline x1 p x2

let write_binary_file out_path' data' =
  let chan = open_out_bin out_path' in
  let () = output_string chan data' in
  close_out chan

(** exit code on signalled / stopped is 1 *)
let cmd
    ?(die=true) ?(complain=true) ?(print_stderr=true)
    args =
  let cmd', args' = match args with
  | [] -> failwith "cmd: empty args"
  | c :: a -> c, c :: a in
  let piep : ('a, Format.formatter, unit) format -> 'a = fun fmt ->
    if die then Fmt.epr ("٭ error: " ^^ fmt ^^ "@.")
    else Fmt.epr ("٭ warning: " ^^ fmt ^^ "@.") in
  let check_exit_status = function
	| Unix.WEXITED 0 -> true
    | Unix.WEXITED r ->
        if complain then piep "the process terminated with exit code (%d)" r;
        if die then exit r;
        false
    | Unix.WSIGNALED n ->
        if complain then piep "the process was killed by a signal (number: %d)" n;
        if die then exit 1;
        false
    | Unix.WSTOPPED n ->
        if complain then piep "the process was stopped by a signal (number: %d)" n;
        if die then exit 1;
        false
  in
  let stdout, stdin, stderr = Unix.open_process_args_full cmd' (Array.of_list args') [||] in
  let bo = Buffer.create 100 in let be = Buffer.create 100 in
  let read b c =
    try while true do Buffer.add_channel b c 1 done
    with End_of_file -> () in
  let () =
    read bo stdout;
    read be stderr;
    if print_stderr then Fmt.epr "%s" (Buffer.contents be) in
  let ok = check_exit_status @@ Unix.close_process_full (stdout, stdin, stderr) in
  ok, Buffer.contents bo, Buffer.contents be

let enable_colors ?(stdout=true) ?(stderr=false) () =
  let f formatter' = Fmt.set_style_renderer formatter' `Ansi_tty in
  if stdout && check_tty_stdout ~die:false () then
      f Fmt.stdout;
  if stderr && check_tty_stderr ~die:false () then
      f Fmt.stderr

let read_file_trim fn = BatFile.with_file_in fn BatIO.read_all |> String.trim
