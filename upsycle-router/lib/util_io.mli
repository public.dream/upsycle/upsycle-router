type 'a fmt := ('a, Format.formatter, unit) format
type 'a fmt4 := ('a, Format.formatter, unit, string) format4

val stdin_raw : unit -> unit
val check_tty_stdin : ?die:bool -> unit -> bool
val check_tty_stdout : ?die:bool -> unit -> bool
val check_tty_stderr : ?die:bool -> unit -> bool
val warn : 'a fmt -> 'a
val pp_map_horizontal' :
  Format.formatter ->
  ('a, 'b) BatMap.t -> ('a -> string) -> ('b -> string) -> unit
val pp_map' :
  Format.formatter ->
  ('a, 'b) BatMap.t -> ('a -> string) -> ('b -> string) -> unit
val fmt : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a -> unit
val fmt_out : 'a fmt4 -> 'a
val fmt_err : 'a fmt4 -> 'a
val pp_blue : 'a Fmt.t -> 'a Fmt.t
val pp_cyan : 'a Fmt.t -> 'a Fmt.t
val pp_green : 'a Fmt.t -> 'a Fmt.t
val pp_magenta : 'a Fmt.t -> 'a Fmt.t
val pp_red : 'a Fmt.t -> 'a Fmt.t
val pp_yellow : 'a Fmt.t -> 'a Fmt.t
val pp_bright_blue : 'a Fmt.t -> 'a Fmt.t
val pp_bright_red : 'a Fmt.t -> 'a Fmt.t
val pp_underline : 'a Fmt.t -> 'a Fmt.t
val pp_hpair : 'a Fmt.t -> 'b Fmt.t -> ('a * 'b) Fmt.t
val pp_section : 'a Fmt.t -> (string * 'a) Fmt.t
val blue: string Fmt.t
val cyan : string Fmt.t
val green : string Fmt.t
val magenta : string Fmt.t
val red : string Fmt.t
val yellow : string Fmt.t
val bright_blue : string Fmt.t
val bright_red : string Fmt.t
val underline : string Fmt.t
val write_binary_file : string -> string -> unit
val cmd : ?die:bool -> ?complain:bool -> ?print_stderr:bool -> string list -> bool * string * string
val enable_colors : ?stdout:bool -> ?stderr:bool -> unit -> unit
val read_file_trim : string -> string
