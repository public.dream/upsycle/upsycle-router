module UString = Util_pure.String

module Infix = struct
  include Lwt.Infix
  let (<$>) = Lwt.map
  let (<*>) lf lx =
    lf >>= fun f ->
    lx >>= fun x ->
    Lwt.return (f x)
end
module Letsfix = struct
  include Infix
  let (let+) a b = Lwt.map b a
  let (and+) = Lwt.both
  let ( let* ) = Lwt.bind
  let ( and* ) = (and+)
end

open Letsfix

type aggregator = [ `Sequential | `Parallel ]

(** A simple notification system.
    @enhancement use react? *)
module Watch = struct
  type watcher = (unit Lwt_condition.t * unit Lwt.t) list
  type 'a t = ('a, watcher) BatMap.t
  let mk () = BatMap.empty
  let wait type' watchers' =
    let cond' = Lwt_condition.create () in
    let watch_t' = Lwt_condition.wait cond' in
    let f = List.cons (cond', watch_t') in
    watch_t', BatMap.modify_def [] type' f watchers'
  let notify type' watchers' =
    match BatMap.find_opt type' watchers' with
    | None -> `No_watchers type'
    | Some ws when List.length ws = 0 -> `No_watchers type'
    | Some ws ->
      let f (cond', _) = Lwt_condition.signal cond' () in
      let () = List.iter f ws in
      `Ok (List.length ws)
end

let wanneer' cond' f = if cond' () then f () else Lwt.return ()
let wanneer = wanneer' % Fun.const
let wanneer_flip = Fun.flip wanneer
let wanneer_flip' = Fun.flip wanneer'

let at_exit_plain f =
  let g _ = f (); exit 0 in
  let _ = Lwt_unix.on_signal Sys.sigint g in
  Lwt_main.Exit_hooks.add_first (f %> Lwt.return)

let ignore' t = let+ _ = t in ()
let ignore f = ignore' (f ())
let ignore_plain' _ = Lwt.return ()
let ignore_plain f = ignore_plain' (f ())

let enable_colors ?(stdout=true) ?(stderr=false) () =
  let f formatter' = Fmt.set_style_renderer formatter' `Ansi_tty in
  if stdout && Util_io.check_tty_stdout ~die:false () then
      List.iter f [Fmt.stdout; Lwt_fmt.(get_formatter stdout)];
  if stderr && Util_io.check_tty_stderr ~die:false () then
      List.iter f [Fmt.stderr; Lwt_fmt.(get_formatter stderr)]

(** `fprintf` with auto flush. *)
let fprintf ?(flush=true) pf fmt =
  let k _pf p =
    let* () = wanneer flush @@ fun _ -> Lwt_fmt.flush pf
    in p in
  Lwt_fmt.kfprintf k pf fmt

(** `printf` with auto flush. *)
let printf ?flush fmt = fprintf ?flush Lwt_fmt.stdout fmt

let speakf ?(flush=true) pref pf fmt =
  (* --- the 2nd argument is there to give the 'type' (i.e. # and type of placeholders) of the string *)
  let pref' = Scanf.format_from_string ("@[" ^ pref ^ " ") "" in
  let suff' = Scanf.format_from_string "@]" "" in
  let fmt' = pref' ^^ fmt ^^ suff' ^^
    if flush then "@." else "" in
  (** Using the continuation seems to make stderr and stdout messages print in the correct order *)
  let k _ p = p in
  Lwt_fmt.kfprintf k pf fmt'

let warn ?(flush=true) fmt =
  speakf ~flush
  (Util_io.fmt_err "%a Warning:" Util_io.bright_red "~")
  Lwt_fmt.stderr fmt

let warn' fmt = warn ~flush:false fmt

let info ?(flush=true) fmt = speakf ~flush
  (* --- @todo using colors messes up the pretty printer boxes? *)
  (* (Util_io.fmt_out "@[%a@]" Util_io.cyan "~") *)
  "٭"
  Lwt_fmt.stdout fmt

let info' fmt = info ~flush:false fmt

let loop f =
  let rec g () = ignore f >>= g
  in g ()

let loop' f x =
  let rec g x' = f x' >>= g
  in g x

let repeat n f =
  let rec g = function
    | m when m = 0 -> Lwt.return ()
    | m -> f () >>= fun _ -> g (m - 1)
  in g n

(** ------ Note that these work properly even when the promise immediately rejects *)

let map_rejection f g lwt_t =
  try%lwt lwt_t with
  | Failure m -> failwith (f m)
  | e -> failwith (g e)

let decorate_rejection s =
  let f m = Util_io.fmt_err "%s: %s" s m in
  let g e = Util_io.fmt_err "%s: %a" s Fmt.exn e in
  map_rejection f g

let replace_rejection s = map_rejection (Fun.const s) (Fun.const s)

let on_rejection f lwt_t =
  Lwt.catch lwt_t begin function
  | Failure m ->
      let* _ = f () in
      failwith (Fmt.(str "%s" m))
  | e ->
      let* _ = f () in
      failwith (Fmt.(str "%a" exn e))
  end

let on_rejection' f lwt_t =
  Lwt.catch lwt_t begin function
  | Failure m -> f (); failwith (Fmt.(str "%s" m))
  | e -> f (); failwith (Fmt.(str "%a" exn e))
  end

let command_handler ?onerror mapping error_prefix_s =
  let table' = BatMap.of_seq (BatList.to_seq mapping) in
  let lookup' x = BatMap.find_opt x table' in
  let handler' c = match List.find_map lookup' [ `C c; `Default ] with
  | Some cb -> cb c
  | None -> failwith "no handler" in
  let fail' pp e =
    let* () = warn "%s: caught rejection: %a" error_prefix_s pp e in
    match onerror with
    | None -> Lwt.return ()
    | Some f -> f () in
  fun c -> try%lwt handler' c with
  | Failure e -> fail' Fmt.string e
  | e -> fail' Fmt.exn e

let start_key_thread ?onerror mapping' =
  let handle' = command_handler ?onerror mapping' "key thread" in
  let f () = Lwt_io.(read_char stdin) >>= handle'
  in loop f

let single_key_mode' () =
  let open Unix in
  let termio = tcgetattr stdin in
  let restore = fun () ->
    tcsetattr stdin TCSANOW termio in
  let _ = at_exit_plain restore in
  Util_io.stdin_raw ()

let single_key_mode () =
  let _ = Util_io.check_tty_stdin () in
  single_key_mode' ()

let mk_prf ?prepend ?append info_s = fun fmt ->
  let info' = Util_pure.String.join_sp_option [ prepend; Some info_s; append ] in
  let pref_fmt' = Scanf.format_from_string info' "" in
  info (pref_fmt' ^^ " " ^^ fmt)

(** This uses `map` to create promises from values, but it is really an iter
    (hence the name) because the promises have side-effects and we throw away
    the returns. *)

let iteri_join f = Lwt.join % List.mapi f

(** Run promises in sequence and return the return value of the last one if
  it's successful, or else fail on the one which failed.

  Note that the values of xs are functions of the form `unit -> 'a Lwt.t` *)
let sequential' = function
    | [] -> failwith "sequential: must receive non-empty list"
    | x :: xs ->
      let f acc p = acc >>= fun _ -> p () in
      List.fold_left f (x ()) xs

(** Like `sequential'`, but values have the form `unit -> unit Lwt.t` *)
let sequential =
  let f acc p = acc >>= fun _ -> p () in
  List.fold_left f (Lwt.return ())

let parallel' =
  let f x = x () in
  Lwt.all % List.map f

let parallel =
  let f x = x () in
  Lwt.join % List.map f

let timeout ?(step=0.1) ?(type'=`Pick)?(prefix="waiting for ") str total main_t =
  let str' = prefix ^ str in
  let info_t =
    let spin = Util_pure.spinner () in
    (** Note that `remaining'` will drift a bit on long timeouts. *)
    let f remaining' =
      let () = if remaining' <= 0. then failwith "Error: timeout didn't fire" in
      let* () = Lwt_fmt.printf "%s (%.1f) %s\r" str' remaining' (spin ()) in
      (* @todo flushing stdout might be triggering what seems like a bug in
         Lwt which prevents this promise from getting cancelled by `pick`. *)
      (* let* () = Lwt_fmt.(flush stdout) in *)
      let* () = Lwt_unix.sleep step in
      Lwt.return (remaining' -. step) in
    loop' f total in
  let timeout_t =
    let* () = Lwt_unix.sleep total in
    failwith @@ Util_io.fmt_out "Timed out %s" str' in
  let ts = [ Lwt.pick [info_t; timeout_t]; main_t ] in
  match type' with
  | `Pick -> Lwt.pick ts
  | `Choose -> Lwt.choose ts

let timeout' ?step ?type' ?prefix str ototal main_t = match ototal with
| None -> main_t
| Some total -> timeout ?step ?type' ?prefix str total main_t

let aggregator = function
| `Parallel -> parallel
| `Sequential -> sequential

let aggregator_of_yaml = function
| `String "parallel" -> Ok `Parallel
| `String "sequential" -> Ok `Sequential
| _ -> Error (`Msg "aggregator_of_yaml")

let aggregator_to_yaml _ = `String "n/a"

let zombie () = let f () = Lwt_unix.sleep 1000. in loop f

let help_message spec =
  let f i (key, short, (long)) =
    let indent = if i = 0 then "  " else "    " in
    let long_s' = BatString.replace_chars (
      function ' ' -> "@ " | c -> String.make 1 c
    ) long in
    let long' =
      if (String.length long_s') = 0 then Scanf.format_from_string "" ""
      else Scanf.format_from_string ("@[" ^ long_s' ^ "@]") "" in
    let border' = UString.repeat_s (String.length key + 1) " " in
    let* () = printf "%s%s %s" indent key short in
    let* () = wanneer (long' <> "") @@ fun _ -> printf ("@,%s%s" ^^ long') indent border' in
    printf "@," in
  let* () = printf "٭ " in
  let* () = Lwt_list.iteri_s f spec in
  printf "\n\n"

(* --- @todo actually not lwt specific? *)
let cond ~otherwise rules =
  match List.find_opt fst rules with
  | None -> otherwise ()
  | Some (_, rule) -> rule ()
