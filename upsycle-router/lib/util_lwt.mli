type 'a fmt := ('a, Format.formatter, unit, unit Lwt.t) format4 -> 'a
type aggregator = [ `Sequential | `Parallel ]

type result_msg := Rresult.R.msg
type 'a to_yaml := 'a -> Yaml.value
type 'a of_yaml := Yaml.value -> ('a, result_msg) result

module Watch : sig
  type watch_t := unit Lwt.t
  type cond_t := unit Lwt_condition.t
  type watcher := (cond_t * watch_t) list
  type 'a t = ('a, watcher) BatMap.t
  val mk : unit -> 'a t
  val wait : 'a -> 'a t -> watch_t * 'a t
  val notify : 'a -> 'a t -> [
    `No_watchers of 'a | `Ok of int
  ]
end

val fprintf : ?flush:bool -> Lwt_fmt.formatter -> 'a fmt
val printf : ?flush:bool -> 'a fmt
val warn : ?flush:bool -> 'a fmt
val warn' : 'a fmt
val info : ?flush:bool -> 'a fmt
val info' : 'a fmt

val ignore' : 'a Lwt.t -> unit Lwt.t
val ignore : (unit -> 'a Lwt.t) -> unit Lwt.t
val ignore_plain' : 'a -> unit Lwt.t
val ignore_plain : (unit -> 'a) -> unit Lwt.t

val enable_colors : ?stdout:bool -> ?stderr:bool -> unit -> unit
type 'a command := [ `C of 'a | `Default ]
val command_handler : ?onerror:(unit -> unit Lwt.t) -> ('a command * ('a -> unit Lwt.t)) list -> string -> 'a -> unit Lwt.t
val start_key_thread : ?onerror:(unit -> unit Lwt.t) -> (char command * (char -> unit Lwt.t)) list -> 'a Lwt.t

val map_rejection : (string -> string) -> (exn -> string) -> 'a Lwt.t -> 'a Lwt.t
val decorate_rejection : string -> 'a Lwt.t -> 'a Lwt.t
val replace_rejection : string -> 'a Lwt.t -> 'a Lwt.t
(** Note that if `f` itself fails, that failure will be the resultant failure of the original promise. *)
val on_rejection : (unit -> 'b Lwt.t) -> (unit -> 'a Lwt.t) -> 'a Lwt.t
val on_rejection' : (unit -> unit) -> (unit -> 'a Lwt.t) -> 'a Lwt.t
val loop : (unit -> 'a Lwt.t) -> 'a Lwt.t
val repeat : int -> (unit -> 'a Lwt.t) -> unit Lwt.t
val at_exit_plain : (unit -> unit) -> Lwt_main.Exit_hooks.hook
val single_key_mode : unit -> unit
val wanneer : bool -> (unit -> unit Lwt.t) -> unit Lwt.t
val wanneer' : (unit -> bool) -> (unit -> unit Lwt.t) -> unit Lwt.t
val wanneer_flip : (unit -> unit Lwt.t) -> bool -> unit Lwt.t
val wanneer_flip' : (unit -> unit Lwt.t) -> (unit -> bool) -> unit Lwt.t
(** Usage: `let prf fmt = Util_lwt.mk_prf "some string" fmt` *)
val mk_prf : ?prepend:string -> ?append:string -> string -> 'a fmt
val iteri_join : (int -> 'a -> unit Lwt.t) -> 'a list -> unit Lwt.t
val sequential' : (unit -> 'a Lwt.t) list -> 'a Lwt.t
val sequential : (unit -> unit Lwt.t) list -> unit Lwt.t
val parallel' : (unit -> 'a Lwt.t) list -> 'a list Lwt.t
val parallel : (unit -> unit Lwt.t) list -> unit Lwt.t
val timeout :
  ?step:float -> ?type':([ `Pick | `Choose ]) -> ?prefix:string -> string ->
  float -> 'a Lwt.t -> 'a Lwt.t
val timeout' :
  ?step:float -> ?type':([ `Pick | `Choose ]) -> ?prefix:string -> string ->
  float option -> 'a Lwt.t -> 'a Lwt.t
val aggregator : aggregator -> (unit -> unit Lwt.t) list -> unit Lwt.t
val aggregator_of_yaml: aggregator of_yaml
val aggregator_to_yaml: aggregator to_yaml
val zombie : unit -> unit Lwt.t
val help_message : (string * string * string) list -> unit Lwt.t
val cond : otherwise:(unit -> 'a) -> (bool * (unit -> 'a)) list -> 'a
module Infix : sig
  include module type of Lwt.Infix
  type 'a t := 'a Lwt.t
  val (<$>) : ('a -> 'b) -> 'a t -> 'b t
  val (<*>) : ('a -> 'b) t -> 'a t -> 'b t
end
module Letsfix : sig
  include module type of Infix
  type 'a t := 'a Lwt.t
  val (let+) : 'a t -> ('a -> 'b) -> 'b t
  val (and+) : 'a t -> 'b t -> ('a * 'b) t
  val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
  val ( and* ) : 'a t -> 'b t -> ('a * 'b) t
end
