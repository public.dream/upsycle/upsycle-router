module SD = Seaboar.Decode
module SE = Seaboar.Encode

open Util_lwt.Letsfix

(** A serializable timer which resumes on deserialize.

   This is an alternative to simple timeouts which keeps track of how many
   seconds have elapsed and how many are remaining. The advantage is that it can
   be serialized/deserialized and resumed when the message router is restarted,
   which is why it lives in the module util_lwt_seaboar.

   We use integer granularity on the clock because a) that's how Lwt_timeout
   does it (even though we're not using it) and b) ttl is given in seconds and
   expiry in minutes and it's probably overkill to get too precise.

   By making `clock` a ref we can use `Lwt.async`, which is necessary to keep
   exceptions from being swallowed up. The handler should be careful not to
   raise any though because they will crash the entire program.

   By using refs for `elapsed` and `remaining` we can keep the `Alarm_clock`
   structure itself immutable and update the refs from inside the thread.

   `elapsed` is probably not all that useful and might be removed.

   The thread will launch immediately when called like this and will work as long as the main
   program is wrapped in Lwt_main.run. *)

module Timer = struct
  let sleep = 1
  type 'a t = Timer of {
    clock: unit Lwt.t ref;
    cancel: bool ref;
    force_finish: bool ref;
    timer_handler_data: 'a;
    elapsed: int ref;
    remaining: int ref;
  }
  module Acc = struct
    let clock (Timer t) = t.clock
    let cancel (Timer t) = t.cancel
    let force_finish (Timer t) = t.force_finish
    let timer_handler_data (Timer t) = t.timer_handler_data
    let elapsed (Timer t) = t.elapsed
    let remaining (Timer t) = t.remaining
    let clock' = (!) % clock
    let cancel' = (!) % cancel
    let elapsed' = (!) % elapsed
    let remaining' = (!) % remaining
  end

  let mk' ?(elapsed=0) handler handler_data remaining =
    let clock' = ref (Lwt.return ()) in
    let cancel' = ref false in
    let force_finish' = ref false in
    let elapsed' = ref elapsed in
    let remaining' = ref remaining in
    let rec tick () =
      let reloop = function | true -> Lwt.return () | false -> tick () in
      let time_up' = !remaining' <= 0 in
      if time_up' || !force_finish' then
        let () = handler ~expired:time_up' handler_data
        in Lwt.return ()
      else
        let* () = Lwt_unix.sleep (float_of_int sleep) in
        let () = elapsed' := !elapsed' + sleep in
        let () = remaining' := !remaining' - sleep
        in reloop !cancel' in
    let () = Lwt.async (fun _ ->
      let tick' = tick () in
      let () = clock' := tick' in
      Lwt.return ()
    ) in Timer {
      clock = clock';
      timer_handler_data = handler_data;
      cancel = cancel';
      force_finish = force_finish';
      elapsed = elapsed';
      remaining = remaining';
    }

  let mk ?(elapsed=0) handler handler_data remaining =
    if remaining <= 0
      then Error (`Msg "Timer.mk: time is 0 or negative")
      else Ok (mk' ~elapsed handler handler_data remaining)

  let cancel (Timer timer) = timer.cancel := true
  let force_finish (Timer timer) = timer.force_finish := true
  let encode timer_handler_data_encoder (Timer timer) =
    (* --- skip clock.clock, clock.cancel, clock.force_finish *)
    SE.variant "Timer" [
      timer_handler_data_encoder timer.timer_handler_data;
      SE.int !(timer.elapsed); SE.int !(timer.remaining);
    ]
  let decode timer_handler timer_handler_data_decoder =
    let f handler_data elapsed remaining =
      match mk ~elapsed timer_handler handler_data remaining with
      | Ok x -> SD.return x
      | Error (`Msg e) -> SD.fail e in
    SD.variant_bind_3 "Timer" f (timer_handler_data_decoder, SD.int, SD.int)
end
