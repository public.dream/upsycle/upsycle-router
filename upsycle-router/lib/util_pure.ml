module BatMap_real = BatMap
module List_real = List
module Option_real = Option
module Result_real = Result
module String_real = String

module BatMap = struct
  type ('a, 'b) t = ('a, 'b) BatMap.t
  let if_empty yes no t = if BatMap.is_empty t then yes t else no t
  let modify_result ?(not_found=`Err "No such key") k f t =
    try Ok (BatMap.modify k f t) with
    | Not_found -> begin match not_found with
      | `Err e -> Error (`Msg e)
      | `Pp pp -> Error (`Msg (Fmt.str "No such key: %a" pp k))
    end
  (* --- @enhancement avoid double lookup *)
  let modify_result' ?(not_found=`Err "No such key") k f t =
    match BatMap.find_opt k t with
    | None -> begin match not_found with
      | `Err e -> Error (`Msg e)
      | `Pp pp -> Error (`Msg (Fmt.str "No such key: %a" pp k))
    end
    | Some v -> begin match f v with
      | Ok v' -> Ok (BatMap.update k k v' t)
      | Error (`Msg e) -> Error (`Msg e)
    end
  let of_list = BatMap.of_seq % List.to_seq
  let to_list = List.of_seq % BatMap.to_seq
  let has_key k = BatMap.exists (fun k' _ -> k' = k)
  let keys_list = BatList.of_enum % BatMap.keys
  let values_list = BatList.of_enum % BatMap.values
  let remove_result ?(not_found=`Err "No such key") k t =
    try Ok (BatMap.remove k t) with
    | Not_found -> begin match not_found with
      | `Err e -> Error (`Msg e)
      | `Pp pp -> Error (`Msg (Fmt.str "No such key: %a" pp k))
    end
  let extract_result ?(not_found=`Err "No such key") k t =
    try Ok (BatMap.extract k t) with
    | Not_found -> begin match not_found with
      | `Err e -> Error (`Msg e)
      | `Pp pp -> Error (`Msg (Fmt.str "No such key: %a" pp k))
    end
  let extract_opt k t =
    try Some (BatMap.extract k t) with
    | Not_found -> None
end

module List = struct
  type 'a t = 'a List.t
  let sngl x = [x]
  let iter_flip xs f = List.iter f xs
  let map_flip xs f = List.map f xs
  let flatmap f = List.(flatten % map f)
  let of_table = BatMap_real.enum %> BatList.of_enum
  let to_table xs =
    let f acc (a, b) = BatMap_real.add a b acc in
    BatList.fold f BatMap_real.empty xs
  let repeat_f n f =
    let g = function
      | m when m >= n -> None
      | m -> Some (f (), m + 1)
    in BatList.unfold 0 g
  let repeat n x = repeat_f n (Fun.const x)
  let unsnoc = function
    | [] -> failwith "unsnoc: empty list"
    | xs ->
        let l = List_real.length xs
        in BatList.take (l-1) xs, BatList.last xs
  let find_f g =
    let rec h = function
      | [] -> None
      | f :: fs ->
          let res = f () in
          if g res then Some res else h fs
    in h
end

module Option = struct
  type 'a t = 'a Option.t
  let bind a b = Option.bind b a
  let to_bool = function | Some _ -> true | None -> false
  let traverse_list f xs =
    let (>>=) = Option.bind in
    let g x oys =
      oys >>= fun yx ->
      f x >>= fun fx ->
      Some (fx :: yx)
    in List_real.fold_right g xs (Some [])
  let fold none some = Option.fold ~none ~some
  let fold' none some = Option.fold ~none:(none ()) ~some
  module Infix = struct
    include BatOption.Infix
    let (<$>) = Option.map
    let (>>|) a b = Option.map b a
    let (=<<) a b = b >>= a
  end
  module Letsfix = struct
    include Infix
    let (let+) a b = Option.map b a
    let (and+) a b = match a, b with
    | None, _ -> None
    | _, None -> None
    | Some x, Some y -> Some (x, y)
    let ( let* ) = Option.bind
    let ( and* ) = (and+)
  end
end

module Result = struct
  type 'a rresult = ('a, Rresult.R.msg) Rresult.R.t
  type ('a, 'b) t = ('a, 'b) Result.t
  type 'a rt = 'a rresult

  let rec decorate_error_s = function
    | `S s ->
      let f t = s ^ ": " ^ t in
      decorate_error_s (`F f)
    | `F f -> Result.map_error f
  let decorate_error = Result.map_error
  let rdecorate_error decorate t =
    let f (`Msg m) = match decorate with
    | `F g -> `Msg (g m)
    | `S s -> `Msg (s ^ ": " ^ m) in
    Result.map_error f t
  (* --- @todo make private? *)
  let map =
    let decorate_error_real = decorate_error in
    fun ~decorate_error f t ->
      Result.map f t |> decorate_error_real decorate_error
  let map_s ~decorate_error f t =
    Result.map f t |> decorate_error_s decorate_error
  let rmap ?(decorate_error=(`F Fun.id)) f t =
    Result.map f t
    |> rdecorate_error decorate_error
  let (<$>) = Result.map
  let (<*>) mf mx = match mf, mx with
    | Ok f, Ok x -> Ok (f x)
    | Error x, _ -> Error x
    | _, Error y -> Error y
  let lift = Result.map
  let lift2 f r1 r2 = match r1, r2 with
    | (Ok x), (Ok y) -> Ok (f x y)
    | (Error x), _   -> Error x
    | _, (Error y)   -> Error y
  let lift3 f r1 r2 r3 =
    lift2 f r1 r2 <*> r3
  let lift4 f r1 r2 r3 r4 =
    lift3 f r1 r2 r3 <*> r4
  let lift5 f r1 r2 r3 r4 r5 =
    lift4 f r1 r2 r3 r4 <*> r5
  let fold_list_left_m f acc =
    let rec g acc' = function
      | [] -> Ok acc'
      | x::xs -> begin match f acc' x with
        | Ok y -> g y xs
        | Error e -> Error e
      end
    in g acc
  let fold_list_right_m f acc =
    let rec g acc' = function
      | [] -> Ok acc'
      | xs ->
          let ys, y = List.unsnoc xs in
          begin match f y acc' with
          | Ok z -> g z ys
          | Error e -> Error e
          end
    in g acc
  let fold ok error = Result.fold ~ok ~error
  let wanneer' cond' f = if cond' () then f () else Rresult.R.return ()
  let wanneer = wanneer' % Fun.const
  let list_find_f_ok rs =
    let open Option.Infix in
    List.find_f Result_real.is_ok rs >>|
    Result_real.get_ok
  let list_find_f_ok_result rs =
    let f acc x = match acc with
    | Ok y -> Ok y
    | Error es ->
        begin match x () with
        | Ok y -> Ok y
        | Error e -> Error (es @ [e]) end in
    List_real.fold_left f (Error []) rs

  module Infix = struct
    include Rresult.R.Infix
    let (<$>) = (<$>)
    let (<*>) = (<*>)
    let (=<<) a b = b >>= a
  end

  module Letsfix = struct
    include Infix
    let (let+) a b = Rresult.R.map b a
    let (and+) a b = match a, b with
    | Error e, _ -> Error e
    | _, Error e -> Error e
    | Ok x, Ok y -> Ok (x, y)
    let ( let* ) = Rresult.R.bind
    let ( and* ) = (and+)
  end
end

module Set = struct
  let find_opt set elem = BatSet.find_opt elem set
  let find_safe set = Option.to_bool % find_opt set
end

module String = struct
  let lines = BatString.split_on_string ~by:"\n"
  let chomp s = match String.length s with
  | 0 -> s
  | n -> begin match String.get s (n-1) with
    | '\n' -> String.sub s 0 (n-1)
    | _ -> s
  end
  let repeat n f =
    let open Buffer in
    let b = create n in
    let () = for _ = 1 to n do
      add_string b (f ())
    done in
    contents b
  let repeat_s n s = repeat n (Fun.const s)
  let join_sp xs = BatString.join " " xs
  let join_sp_list = join_sp % List_real.flatten
  let join_sp_option =
    let g = Option_real.to_list in
    let f = List_real.map g in
    join_sp_list % f
  let append n m = BatString.join "" [m; n]
  (** Heuristic, may not be totally reliable. Only searches first `take` chars. *)
  let is_binary ?take =
    let take' = Option.fold Fun.id BatSeq.take take in
    let f ch =
      let code = Char.code ch in
      code < 0x20 || code > 0x7e || String.contains "\t\r\n" ch in
    ((<>) None) % BatSeq.find f % take' % String_real.to_seq
end

let table_map_as_pairs f = List.of_table %> List_real.map f
let id x = x
let compose_right_all : ('a -> 'a) list -> ('a -> 'a) =
  fun xs -> let f acc x = acc %> x in List_real.fold_left f id xs

let spinner () =
  let blocks' = [
    "⠄"; "⠆"; "⠇"; "⠋"; "⠙"; "⠸"; "⠰"; "⠠"; "⠰"; "⠸"; "⠙"; "⠋"; "⠇"; "⠆";
  ] in
  let i = ref (-1) in let n = List_real.length blocks' in
  fun _ -> i := (!i + 1) mod n; List_real.nth blocks' !i

let english_plural word n =
  let s = if n = 1 then "" else "s" in
  word ^ s

let english_plural' word n =
  english_plural word n |>
  Fmt.str "%d %s" n

let mk_absolute_path prefix s =
  if Filename.is_relative s then prefix ^ "/" ^ s
  else s
