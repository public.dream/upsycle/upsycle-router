module BatMap_real := BatMap

type 'a rresult := ('a, Rresult.R.msg) Rresult.R.t
(* type result_msg := Rresult.R.msg *)

module BatMap : sig
  type 'a not_found := [ `Err of string | `Pp of (Format.formatter -> 'a -> unit) ]
  type ('a, 'b) t = ('a, 'b) BatMap.t

  val if_empty : (('a, 'b) t -> 'c) -> (('a, 'b) t -> 'c) -> ('a, 'b) t -> 'c
  val modify_result : ?not_found:('a not_found) -> 'a -> ('b -> 'b) -> ('a, 'b) t -> ('a, 'b) t rresult
  (** Like `modify_result`, except that `f` returns a result. *)
  val modify_result' :
    ?not_found:('a not_found) ->
    'a -> ('b -> 'b rresult) -> ('a, 'b) t ->
    ('a, 'b) t rresult
  val of_list : ('a * 'b) list -> ('a, 'b) t
  val to_list : ('a, 'b) t -> ('a * 'b) list
  val has_key : 'a -> ('a, 'b) t -> bool
  val keys_list : ('a, 'b) t -> 'a list
  val values_list : ('a, 'b) t -> 'b list
  val remove_result :
    ?not_found:('a not_found) ->
    'a -> ('a, 'b) t ->
    ('a, 'b) t rresult
  val extract_result :
    ?not_found:('a not_found) ->
    'a -> ('a, 'b) t ->
    ('b * ('a, 'b) t) rresult
  val extract_opt :
    'a -> ('a, 'b) t ->
    ('b * ('a, 'b) t) option
end
module List : sig
  type 'a t = 'a List.t
  val sngl : 'a -> 'a t
  val iter_flip : 'a t -> ('a -> unit) -> unit
  val map_flip : 'a t -> ('a -> 'b) -> 'b t
  val flatmap : ('a -> 'b t) -> 'a t -> 'b t
  val of_table : ('a, 'b) BatMap_real.t -> ('a * 'b) t
  val to_table : ('a * 'b) t -> ('a, 'b) BatMap_real.t
  val repeat_f : int -> (unit -> 'a) -> 'a t
  val repeat : int -> 'a -> 'a t
  (** The opposite of cons, and reversed: `unsnoc [a; b; c]` gives `([a; b], c)`. Throws on empty list. *)
  val unsnoc : 'a t -> 'a t * 'a
  (** Given a function [g] and a list of functions [fs], find the first one for
     which [g (f ())] is true, and return [Some (f ())]. If none of them match,
     return [None]. *)
  val find_f : ('a -> bool) -> (unit -> 'a) t -> 'a option
end
module Option : sig
  type 'a t = 'a Option.t
  val bind : ('a -> 'b option) -> 'a option -> 'b option
  val to_bool : 'a option -> bool
  val traverse_list : ('a -> 'b option) -> 'a list -> 'b list option
  val fold : 'a -> ('b -> 'a) -> 'b option -> 'a
  val fold' : (unit -> 'a) -> ('b -> 'a) -> 'b option -> 'a
  module Infix : sig
    include module type of BatOption.Infix
    val (<$>) : ('a -> 'b) -> 'a option -> 'b option
    val (>>|) : 'a option -> ('a -> 'b) -> 'b option
    val (=<<) : ('a -> 'b t) -> 'a t -> 'b t
  end
  (** A combination of Infix and Let (for let* etc.) *)
  module Letsfix : sig
    include module type of Infix
    val (let+) : 'a t -> ('a -> 'b) -> 'b t
    val (and+) : 'a t -> 'b t -> ('a * 'b) t
    val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
    val ( and* ) : 'a t -> 'b t -> ('a * 'b) t
  end
end
module Result : sig
  type ('a, 'b) t = ('a, 'b) Result.t
  type 'a rt = 'a rresult
  type decorate_s := [ `F of string -> string | `S of string ]
  val decorate_error_s : decorate_s -> ('a, string) t -> ('a, string) t
  val decorate_error : ('b -> 'c) -> ('a, 'b) t -> ('a, 'c) t
  val rdecorate_error : decorate_s -> 'a rresult -> 'a rresult
  val map : decorate_error:('b -> 'c) -> ('a -> 'd) -> ('a, 'b) t -> ('d, 'c) t
  val map_s : decorate_error:decorate_s -> ('a -> 'b) -> ('a, string) t -> ('b, string) t
  val rmap : ?decorate_error:decorate_s -> ('a -> 'b) -> 'a rt -> 'b rt
  val (<$>) : ('a -> 'b) -> ('a, 'c) t -> ('b, 'c) t
  val (<*>) : ('a -> 'b, 'c) t -> ('a, 'c) t -> ('b, 'c) t
  val lift : ('a -> 'b) -> ('a, 'e) t -> ('b, 'e) t
  val lift2 : ('a -> 'b -> 'c) -> ('a, 'e) t -> ('b, 'e) t -> ('c, 'e) t
  val lift3 : ('a -> 'b -> 'c -> 'd) -> ('a, 'e) t -> ('b, 'e) t -> ('c, 'e) t -> ('d, 'e) t
  val lift4 : ('a -> 'b -> 'c -> 'd -> 'f) -> ('a, 'e) t -> ('b, 'e) t -> ('c, 'e) t -> ('d, 'e) t -> ('f, 'e) t
  val lift5 : ('a -> 'b -> 'c -> 'd -> 'f -> 'g) -> ('a, 'e) t -> ('b, 'e) t -> ('c, 'e) t -> ('d, 'e) t -> ('f, 'e) t -> ('g, 'e) t
  val fold_list_left_m : ('a -> 'b -> ('a, 'c) t) -> 'a -> 'b list -> ('a, 'c) t
  val fold_list_right_m : ('b -> 'a -> ('a, 'c) t) -> 'a -> 'b list -> ('a, 'c) t
  val fold : ('a -> 'c) -> ('b -> 'c) -> ('a, 'b) t -> 'c
  val wanneer' : (unit -> bool) -> (unit -> (unit, 'a) t) -> (unit, 'a) t
  val wanneer : bool -> (unit -> (unit, 'a) t) -> (unit, 'a) t
  (** Given a list of functions [rs] where [r : unit -> ('a, 'b) t], find the
      first one which returns [Ok result]. If found, return [Some result],
      otherwise [None]. *)
  val list_find_f_ok : (unit -> ('a, 'b) t) list -> 'a Option.t
  (** Like [list_find_f_ok], but on failure it returns [Error of 'b list], which
      has the same length as [rs] and contains the [Error] values that were
      encountered during the attempt. *)
  val list_find_f_ok_result : (unit -> ('a, 'b) t) list -> ('a, 'b list) t
  module Infix : sig
    include module type of Rresult.R.Infix
    val (<$>) : ('a -> 'b) -> ('a, 'c) t -> ('b, 'c) t
    val (<*>) : ('a -> 'b, 'c) t -> ('a, 'c) t -> ('b, 'c) t
    val (=<<) : ('a -> ('b, 'c) t) -> ('a, 'c) t -> ('b, 'c) t
  end
  (** A combination of Infix and Let (for let* etc.) *)
  module Letsfix : sig
    include module type of Infix
    val (let+) : ('a, 'b) t -> ('a -> 'c) -> ('c, 'b) t
    val (and+) : ('a, 'b) t -> ('c, 'b) t -> (('a * 'c), 'b) t
    val ( let* ) : ('a, 'b) t -> ('a -> ('c, 'b) t) -> ('c, 'b) t
    val ( and* ) : ('a, 'b) t -> ('c, 'b) t -> (('a * 'c), 'b) t
  end
end
module Set : sig
  val find_opt : 'a BatSet.t -> 'a -> 'a option
  val find_safe : 'a BatSet.t -> 'a -> bool
end
module String : sig
  val lines : string -> string list
  val chomp : string -> string
  val repeat : int -> (unit -> string) -> string
  val repeat_s : int -> string -> string
  val join_sp : string list -> string
  val join_sp_list : string list list -> string
  val join_sp_option : string option list -> string
  val append : string -> string -> string
  val is_binary : ?take:int -> string -> bool
end
val table_map_as_pairs : ('a * 'b -> 'c) -> ('a, 'b) BatMap_real.t -> 'c list
val id : 'a -> 'a
val compose_right_all : ('a -> 'a) list -> 'a -> 'a
val spinner : unit -> 'a -> string
val english_plural : string -> int -> string
val english_plural' : string -> int -> string
val mk_absolute_path : string -> string -> string
