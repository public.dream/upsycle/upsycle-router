type result_msg = Rresult.R.msg

type 'a to_yaml = 'a -> Yaml.value
type 'a of_yaml = Yaml.value -> ('a, result_msg) result

let ok x = Ok x

let to_yaml_noop _ = `String "n/a"
let list_parser p err_msg = function
    | `A xs ->
        let open Rresult.R.Infix in
        let f acc x = p x >>= fun mg ->
          let g ys = List.cons mg ys in
          Result.map g acc in
        List.fold_left f (Ok []) xs
    | _ -> Error (`Msg err_msg)

(** Takes a list of Yaml values and returns `Ok [s1, s2, ...]` if they're all
    `\`String s` values, and `Error` otherwise. *)
let strings_result ?(msg="invalid yaml") =
  let open Util_pure.Result in
  let f acc = function
    | `String s -> Ok (acc @ [s])
    | _ -> Error (`Msg msg)
  in fold_list_left_m f []

let string_result ?(msg="invalid yaml") s =
  let open Rresult.R.Infix in
  strings_result ~msg [s] >>= ok % List.hd
