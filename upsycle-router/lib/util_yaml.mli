type result_msg := Rresult.R.msg

type 'a to_yaml = 'a -> Yaml.value
type 'a of_yaml = Yaml.value -> ('a, result_msg) result

val to_yaml_noop : 'a to_yaml
val list_parser : ('a -> ('b, [> `Msg of 'd ] as 'c) result) -> 'd -> [> `A of 'a list ] -> ('b list, 'c) result
val strings_result : ?msg:string -> Yaml.value list -> (string list, result_msg) result
val string_result : ?msg:string -> Yaml.value -> (string, result_msg) result
