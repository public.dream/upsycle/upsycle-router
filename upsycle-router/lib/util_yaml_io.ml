let parse_file x_of_yaml yaml_path =
  let open Rresult in
  let f s = R.reword_error_msg ~replace:true (fun x -> `Msg (Fmt.str
    "Error in config file %s: %s: %s" yaml_path s x)
  ) in
  let p1 = Yaml_unix.of_file in
  let p2 = x_of_yaml in
  match R.ok (Fpath.v yaml_path) >>= f "Couldn't parse file" % p1 >>= f "Invalid config" % p2 with
  | Error (`Msg e) -> failwith e
  | Ok c -> c
